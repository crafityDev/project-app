/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF, DatePipe } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CompanyService } from './pages/company/company.service';
import { ClientService } from './pages/client/client.service';
import { StaffService } from './pages/company/staff/staff.service';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { LoginComponent } from './auth/login/login.component';
import { AuthComponent } from './auth/auth.component';
import { ForgotPasswordComponent } from './auth/forget-password/forget-password.component';
import { RegisterComponent } from './auth/register/register.component';
import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth/auth.guard';
import { BankService } from './pages/shared/bank.service';
import { NbTabsetModule, NbToastrModule } from '@nebular/theme';
import { ProjectService } from './pages/project/project.service';
import { VerifyComponent } from './auth/verify/verify.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AccountService } from './pages/account/account.service';
import { DocumentService } from './pages/document/document.service';
import { TaskService } from './pages/project/project-info/shared/task.service';
import { CompanyAddComponent } from './pages/company/company-add/company-add.component';
import { SetPasswordComponent } from './auth/set-password/set-password.component';
import { DragulaModule } from 'ng2-dragula';
import { UserAddComponent } from './auth/user-add/user-add.component';
import { ProfilewComponent } from './pages/account/profilew/profilew.component';
import { WchangepasswdComponent } from './pages/account/wchangepasswd/wchangepasswd.component';
import { WeditComponent } from './pages/account/wedit/wedit.component';

@NgModule({
  declarations: [AppComponent,
    LoginComponent,
    AuthComponent,
    ForgotPasswordComponent,
    RegisterComponent,
    CompanyAddComponent,
    VerifyComponent,
    SetPasswordComponent,
    UserAddComponent,
    ProfilewComponent,
    WchangepasswdComponent,
    WeditComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbTabsetModule,
    PdfViewerModule,

    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.circleSwish,
    }),
    NbToastrModule.forRoot(),
    DragulaModule.forRoot(),
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    CompanyService,
    BankService,
    ClientService,
    StaffService,
    ProjectService,
    AuthService,
    AuthGuard,
    DatePipe,
    AccountService,
    DocumentService,
    TaskService,
  ],
})
export class AppModule {
}
