import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService {
    constructor(private httpClient: HttpClient) { }

    postlogin(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.httpClient.post('http://206.189.34.171:7777/mt/login', body, {
            headers,
        });
    }

    postregis(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.httpClient.post('http://206.189.34.171:7777/mt/register', body, {
            headers,
        });
    }

    isAuthenticated() {
        return localStorage.getItem('key') != null;
    }

    getUser(mail) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('email', mail);
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getuser', {
            headers: headers,
        });
    }

    requireNewPwd(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.httpClient.put('http://206.189.34.171:7777/mt/sendresetmail', body, {
            headers,
        });
    }

    setNewPwd(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.httpClient.put('http://206.189.34.171:7777/mt/forgotpassword', body, {
            headers,
        });
    }

    changePwd(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.put('http://206.189.34.171:7777/mt/changepassword', body, {
            headers,
        });
    }
}
