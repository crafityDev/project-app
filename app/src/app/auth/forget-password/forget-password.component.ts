import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {

  forgotPass: FormGroup;
  body: any;

  constructor(private authService: AuthService,
    private router: Router,
    private toastrService: NbToastrService) {
    const tempEmail = '';
    this.forgotPass = new FormGroup({
      email: new FormControl(tempEmail, [Validators.required, Validators.email]),
    });
  }

  ngOnInit() {
  }

  requirePasswd() {
    this.body = {
      email: this.forgotPass.get('email').value,
    };
    this.authService.requireNewPwd(this.body).subscribe(
      (data) => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.toastrService.success(
          'We have sent the link for resetting your password within.',
          'Please check your email!',
          { duration: 5000 },
        );
        setTimeout(() => {
          this.router.navigateByUrl('/auth/login');
        }, 400);
      },
      error => {
        // this.toastrService.danger('', error.error.desc_EN, {});
        console.error(error);
      },
    );
  }

}
