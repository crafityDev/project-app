import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  regisForm: FormGroup;
  invitecom: any;
  checked: boolean;
  errorRegis = '';

  constructor(private authService: AuthService,
    private router: Router,
    activeid: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toastrService: NbToastrService) {
    const tempEmail = '';
    const tempPassword = '';
    const tempConfirmPassword = '';
    // const tempAcceptTerms = false;
    this.checked = false;

    activeid.queryParams.subscribe((params: Params) => {
      this.invitecom = params['token'];
    });

    this.regisForm = this.formBuilder.group({
      email: new FormControl(tempEmail, [Validators.required, Validators.email]),
      password: new FormControl(tempPassword, [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl(tempConfirmPassword),
      // acceptTerms: new FormControl(tempAcceptTerms, Validators.requiredTrue),
    }, { validator: this.pwdMatchValidator });
  }

  ngOnInit() {
  }

  regis() {
    if (this.invitecom === undefined) {
      this.authService.postregis(this.regisForm.value).subscribe(
        (data) => {
          // tslint:disable-next-line: no-console
          console.log(data);
          this.toastrService.success(
            'Please check your email for verification.',
            'Register Complete!',
            { duration: 5000 },
          );
          setTimeout(() => {
            this.router.navigateByUrl('/auth/login');
          }, 400);
        },
        error => {
          // this.toastrService.danger('', error.error.desc_EN, {});
          console.error(error);
          this.errorRegis = error.error.desc_EN;
        },
      );
    } else {
      this.authService.postregis(this.regisForm.value).subscribe(
        (data) => {
          // tslint:disable-next-line: no-console
          console.log(data);
          this.toastrService.success(
            'Please check your email for verification.',
            'Register Complete!',
            { duration: 5000 },
          );
          setTimeout(() => {
            this.router.navigate(['/auth/login']
              , { queryParams: { token: this.invitecom } });
          }, 400);
        },
        error => {
          // this.toastrService.danger('', error.error.desc_EN, {});
          console.error(error);
          this.errorRegis = error.error.desc_EN;
        },
      );
    }
  }

  pwdMatchValidator(frm: FormGroup) {
    return frm.get('password').value === frm.get('confirmPassword').value
      ? null : { 'mismatch': true };
  }

  toggleCheck() {
    this.checked = !this.checked;
  }

}
