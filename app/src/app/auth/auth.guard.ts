import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, CanActivateChild } from '@angular/router';

@Injectable()

export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.authService.isAuthenticated();
    }
    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        return this.authService.isAuthenticated();
    }
}
