import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountService } from '../../pages/account/account.service';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { CompanyService } from '../../pages/company/company.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'ngx-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss'],
})
export class UserAddComponent implements OnInit {
  invitecom: any;
  userForm: FormGroup;
  body: any;
  bodypic: any;
  selectedFile = [];
  UserPicturePath: any;

  constructor(activeid: ActivatedRoute,
    private companyservice: CompanyService,
    private accountService: AccountService,
    private authService: AuthService,
    private router: Router,
    private toastrService: NbToastrService) {
    this.userForm = new FormGroup({
      UserName: new FormControl('', Validators.required),
    });

    activeid.queryParams.subscribe((params: Params) => {
      this.invitecom = params['token'];
    });
  }

  ngOnInit() {
    this.UserPicturePath = 'http://206.189.34.171:7777/default/img/gray.png';
    this.getBlob();
  }

  updateProfile() {
    this.body = {
      email: localStorage.getItem('email'),
      UserName: this.userForm.get('UserName').value,
    };
    this.bodypic = {
      type: this.selectedFile[0].name.split('.')[1],
      name: this.selectedFile[0].name.split('.')[0],
    };
    this.accountService.updataUser(this.body).subscribe(
      (response) => {
        this.toastrService.success('', 'Company has been created!', {});
        // tslint:disable-next-line: no-console
        console.log('Save' + response);
        this.accountService.putImgWithAuth(this.bodypic, this.selectedFile[0]).subscribe(
          res => {
            // tslint:disable-next-line: no-console
            console.log('upload' + res);
            if (this.invitecom === undefined) {
              this.router.navigateByUrl('/auth/profile');
            } else {
              this.body = {
                CompanyPair: this.invitecom,
              };
              this.companyservice.updateUserCompanyBypair(this.body).subscribe(
                (data) => {
                  // tslint:disable-next-line: no-console
                  console.log(data);
                  this.authService.getUser(localStorage.getItem('email')).subscribe(
                    ndata => {
                      localStorage.setItem('companyId', ndata.CompanyId);
                      this.router.navigateByUrl('/page/dashboard');
                    });
                }, error => {
                  // this.toastrService.danger(error.message, 'Something went wrong!', {});
                  console.error(error);
                });
            }
          },
          error => {
            this.toastrService.danger('Please try again in a few minutes.', 'Cannot upload this picture.', {});
            console.error(error);
          });
      },
      error => {
        this.toastrService.danger(error.message, 'Please select profile picture', {});
        console.error(error);
      });
  }

  onFileSelected(event) {
    this.selectedFile[0] = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(this.selectedFile[0]);
    reader.onload = (_event) => {
      this.UserPicturePath = reader.result;
    };
  }

  getBlob() {
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[0] = this.blobToFile(xhr.response
          , this.UserPicturePath.split('/')[this.UserPicturePath.split('/').length - 1]);
      } else {
        this.selectedFile = [];
      }
    };
    xhr.open('GET', this.UserPicturePath, true);
    xhr.send();
  }

  blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;
    // A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    // Cast to a File() type
    return <File>theBlob;
  }


}
