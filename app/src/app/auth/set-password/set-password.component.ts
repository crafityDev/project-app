import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../auth.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.scss'],
})
export class SetPasswordComponent implements OnInit {

  newpwdForm: FormGroup;
  errorRegis = '';
  body: any;
  verifyPair: any;
  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastrService: NbToastrService,
    activeid: ActivatedRoute,
  ) {
    activeid.queryParams.subscribe((params: Params) => {
      this.verifyPair = params['Verify'];
    });

    const tempPassword = '';
    const tempConfirmPassword = '';

    this.newpwdForm = this.formBuilder.group({
      password: new FormControl(tempPassword, [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl(tempConfirmPassword),
      // acceptTerms: new FormControl(tempAcceptTerms, Validators.requiredTrue),
    }, { validator: this.pwdMatchValidator });
  }

  ngOnInit() {
  }

  setNew() {
    this.body = {
      password: this.newpwdForm.get('password').value,
      VerifyPair: this.verifyPair,
    };
    this.authService.setNewPwd(this.body).subscribe(
      (data) => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.toastrService.success(
          '',
          'Set New Password Success',
          { duration: 5000 },
        );
        setTimeout(() => {
          this.router.navigateByUrl('/auth/login');
        }, 400);
      },
      error => {
        // this.toastrService.danger('', error.error.desc_EN, {});
        console.error(error);
        this.errorRegis = error.error.desc_EN;
      },
    );
  }

  pwdMatchValidator(frm: FormGroup) {
    return frm.get('password').value === frm.get('confirmPassword').value
      ? null : { 'mismatch': true };
  }

}
