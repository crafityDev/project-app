import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ngx-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss'],
})
export class VerifyComponent implements OnInit {

  verifyPair: any;

  userForm: FormGroup;
  companyForm: FormGroup;
  Done = false;
  id: any;
  body: any;
  CompanyPicturePath: any;
  bodypic: any;
  selectedFile = [];

  constructor(
    activeid: ActivatedRoute,
    private httpClient: HttpClient) {

    activeid.queryParams.subscribe((params: Params) => {
      this.verifyPair = params['Verify'];
    });
  }

  ngOnInit() {
    this.httpClient.get<any>('http://206.189.34.171:7777/mt/verifyMail/' + this.verifyPair).subscribe(
      response => {
        // tslint:disable-next-line: no-console
        console.log(response);
      }, error => {
        console.error(error);
      });
  }

}
