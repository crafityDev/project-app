import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { LayoutService } from '../../@core/utils';
import { CompanyService } from '../../pages/company/company.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  body: any;
  invitecom: any;
  loginForm: FormGroup;
  public loading = false;
  sthWrong = false;
  key: string;

  constructor(private authService: AuthService,
    private companyservice: CompanyService,
    activeid: ActivatedRoute,
    private router: Router,
    private toastrService: NbToastrService,
    protected layoutService: LayoutService) {

    activeid.queryParams.subscribe((params: Params) => {
      this.invitecom = params['token'];
    });

    const email = '';
    const password = '';

    this.loginForm = new FormGroup({
      email: new FormControl(email, [Validators.required, Validators.email]),
      password: new FormControl(password, [Validators.required, Validators.minLength(6)]),
    });
  }

  ngOnInit() {
    this.key = localStorage.getItem('key');
  }

  login() {
    this.loading = true;
    this.authService.postlogin(this.loginForm.value).subscribe(
      (response) => {
        this.loading = false;
        // tslint:disable-next-line: no-console
        console.log(response);
        if (response['success'] === false) {
          if (response['token'] === 'Not Verify') {
            this.toastrService.danger('Please verify your email.', 'Something went wrong!', {});
          } if (response['token'] === 'Wrong E-mail' || response['token'] === 'Wrong Password') {
            this.toastrService.danger('Wrong email or password. Please try again or click Forgot password to reset it.'
              , 'Something went wrong!', {});
            this.sthWrong = true;
          }
        } if (response['success'] === true) {
          this.sthWrong = false;
          this.key = response['token'];
          localStorage.setItem('key', this.key);
          localStorage.setItem('email', this.loginForm.get('email').value);
          this.authService.isAuthenticated();
          this.authService.getUser(this.loginForm.get('email').value).subscribe(
            (res) => {
              this.toastrService.success('', 'Login Complete!', {});
              // tslint:disable-next-line: no-console
              console.log(res);
              if (this.invitecom === undefined) {
                // tslint:disable-next-line: no-console
                console.log('un');
                if (!res.UserName || res.UserName === 'null') {
                  // tslint:disable-next-line: no-console
                  console.log('unnull');
                  this.router.navigateByUrl('/auth/user-add');
                } else {
                  if (!res.CompanyId || res.CompanyId.length === 0) {
                    // tslint:disable-next-line: no-console
                    console.log('unlength');
                    this.router.navigateByUrl('/auth/profile');
                  } else {
                    // tslint:disable-next-line: no-console
                    console.log('undash');
                    localStorage.setItem('companyId', res.CompanyId);
                    this.router.navigateByUrl('/page/dashboard');
                  }
                }
              } else {
                // tslint:disable-next-line: no-console
                console.log('!un');
                if (!res.UserName || res.UserName === 'null') {
                  // tslint:disable-next-line: no-console
                  console.log('!unnull');
                  this.router.navigate(['/auth/user-add'], { queryParams: { token: this.invitecom } });
                } else {
                  // tslint:disable-next-line: no-console
                  console.log('!unver');
                  this.body = {
                    CompanyPair: this.invitecom,
                  };
                  this.companyservice.updateUserCompanyBypair(this.body).subscribe(
                    (data) => {
                      // tslint:disable-next-line: no-console
                      console.log(data);
                      this.authService.getUser(this.loginForm.get('email').value).subscribe(
                        ndata => {
                          localStorage.setItem('companyId', ndata.CompanyId);
                          this.router.navigateByUrl('/page/dashboard');
                        });
                    }, error => {
                      // this.toastrService.danger(error.message, 'Something went wrong!', {});
                      console.error(error);
                    });
                  // this.router.navigate(['/auth/staff-add'], { queryParams: { token: this.invitecom } });
                }
              }
            },
            error => {
              // this.toastrService.danger(error.message, 'Something went wrong!', {});
              console.error(error);
            });
        }
      },
      error => {
        this.loading = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      },
    );
  }

  gotoregis() {
    if (this.invitecom === undefined) {
      this.router.navigateByUrl('/auth/register');
    } else {
      this.router.navigate(['/auth/register']
        , { queryParams: { token: this.invitecom } });
    }
  }

  // testlogin() {
  //   this.loading = true;
  //   this.authService.postlogin(this.loginForm.value).subscribe(
  //     (response) => {
  //       this.loading = false;
  //       // tslint:disable-next-line: no-console
  //       console.log(response);
  //       if (response['token'].length < 60) {
  //         if (response['token'] === 'Not Verify') {
  //         } else {
  //           this.sthWrong = true;
  //         }
  //       } else {
  //         this.sthWrong = false;
  //         this.key = response['token'];
  //         localStorage.setItem('key', this.key);
  //         localStorage.setItem('email', this.loginForm.get('email').value);
  //         this.authService.isAuthenticated();
  //         if (!this.key || this.key === 'Not Verify') {
  //           this.toastrService.danger('Please try again in a few minutes.', 'Something went wrong!', {});
  //           this.router.navigateByUrl('/auth/#');
  //         } else {
  //           this.authService.getUser(this.loginForm.get('email').value).subscribe(
  //             (res) => {
  //               this.toastrService.success('', 'Login Complete!', {});
  //               // tslint:disable-next-line: no-console
  //               console.log(res);
  //               if (!res.CompanyId || res.CompanyId === 'null') {
  //                 this.router.navigateByUrl('/auth/company-add');
  //               } else {
  //                 localStorage.setItem('companyId', res.CompanyId);
  //                 this.router.navigateByUrl('/page/dashboard');
  //               }
  //             },
  //             error => {
  //               // this.toastrService.danger(error.message, 'Something went wrong!', {});
  //               console.error(error);
  //             });
  //         }
  //       }

  //     },
  //     error => {
  //       this.loading = false;
  //       this.toastrService.danger(error.message, 'Something went wrong!', {});
  //       console.error(error);
  //     },
  //   );
  // }

}
