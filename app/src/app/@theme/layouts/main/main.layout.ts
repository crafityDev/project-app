import { Component, OnDestroy, HostListener, OnInit } from '@angular/core';
import { delay, withLatestFrom, takeWhile } from 'rxjs/operators';
import {
    NbMediaBreakpoint,
    NbMediaBreakpointsService,
    NbMenuService,
    NbSidebarService,
    NbThemeService,
} from '@nebular/theme';

import { StateService, LayoutService } from '../../../@core/utils';

// TODO: move layouts into the framework
@Component({
    selector: 'ngx-main-layout',
    styleUrls: ['./main.layout.scss'],
    template: `
    <nb-layout windowMode>
      <nb-layout-header fixed [ngClass]="{'no-submenu': isMobile}">
        <div class="header">
        <ngx-header></ngx-header>
        <ngx-sub-header></ngx-sub-header>
        </div>
      </nb-layout-header>

      <nb-sidebar class="menu-sidebar d-block d-md-none" tag="menu-sidebar" responsive>
        <ng-content select="nb-menu"></ng-content>
      </nb-sidebar>

      <nb-layout-column class="main-content">
        <ng-content select="router-outlet"></ng-content>
      </nb-layout-column>

      <nb-layout-footer fixed>
        <ngx-footer></ngx-footer>
      </nb-layout-footer>

    </nb-layout>
  `,
})

export class MainLayoutComponent implements OnInit, OnDestroy {

    isMobile: boolean = false;

    @HostListener('window:resize', [])
    onResize() {
        const width = window.innerWidth;
        if (width < 768) {
            this.isMobile = true;
        } else {
            this.isMobile = false;
        }
    }

    layout: any = {};
    sidebar: any = {};

    private alive = true;

    currentTheme: string;

    constructor(protected stateService: StateService,
        protected menuService: NbMenuService,
        protected themeService: NbThemeService,
        protected bpService: NbMediaBreakpointsService,
        protected sidebarService: NbSidebarService,
        protected layoutService: LayoutService) {
        this.stateService.onLayoutState()
            .pipe(takeWhile(() => this.alive))
            .subscribe((layout: string) => this.layout = layout);

        this.stateService.onSidebarState()
            .pipe(takeWhile(() => this.alive))
            .subscribe((sidebar: string) => {
                this.sidebar = sidebar;
            });

        const isBp = this.bpService.getByName('is');
        this.menuService.onItemSelect()
            .pipe(
                takeWhile(() => this.alive),
                withLatestFrom(this.themeService.onMediaQueryChange()),
                delay(20),
            )
            .subscribe(([item, [bpFrom, bpTo]]: [any, [NbMediaBreakpoint, NbMediaBreakpoint]]) => {

                if (bpTo.width <= isBp.width) {
                    this.sidebarService.collapse('menu-sidebar');
                }
            });

        this.themeService.getJsTheme()
            .pipe(takeWhile(() => this.alive))
            .subscribe(theme => {
                this.currentTheme = theme.name;
            });
    }

    ngOnInit(): void {

        const width = window.innerWidth;
        if (width < 992) {
            this.isMobile = true;
        } else {
            this.isMobile = false;
        }
    }

    ngOnDestroy() {
        this.alive = false;
    }
}
