import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.scss'],
})
export class SubHeaderComponent implements OnInit {

  @Input() position = 'normal';

  constructor() { }

  ngOnInit() {
  }

}
