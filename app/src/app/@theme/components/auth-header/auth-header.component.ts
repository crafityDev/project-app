import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-auth-header',
  styleUrls: ['./auth-header.component.scss'],
  templateUrl: './auth-header.component.html',
})

export class AuthHeaderComponent implements OnInit {

  @Input() position = 'normal';

  userName: any;
  userPic: any;

  constructor(private router: Router) { }

  ngOnInit() {
  }


  goToHome() {
    this.router.navigateByUrl('/auth/login');
  }
}

