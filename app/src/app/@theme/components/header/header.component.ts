import { Component, Input, OnInit } from '@angular/core';

import { NbSidebarService, NbToastrService } from '@nebular/theme';
import { AnalyticsService } from '../../../@core/utils';
import { LayoutService } from '../../../@core/utils';
import { Router } from '@angular/router';
import { AccountService } from '../../../pages/account/account.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})

export class HeaderComponent implements OnInit {

  @Input() position = 'normal';

  userName: any;
  userPic: any;

  constructor(private sidebarService: NbSidebarService,
    private accountService: AccountService,
    private analyticsService: AnalyticsService,
    private layoutService: LayoutService,
    private router: Router,
    private toastrService: NbToastrService) { }

  ngOnInit() {
    this.accountService.getUser().subscribe(
      (data) => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.userName = data.UserName;
        this.userPic = 'http://206.189.34.171:7777/user/img/' + data.UserPicturePath;
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });

    this.accountService.accountUpdate.subscribe(
      res => {
        this.accountService.getUser().subscribe(
          (data) => {
            // tslint:disable-next-line: no-console
            console.log(data);
            this.userName = data.UserName;
            this.userPic = 'http://206.189.34.171:7777/user/img/' + data.UserPicturePath;
          },
          error => {
            this.toastrService.danger(error.message, 'Something went wrong!', {});
            console.error(error);
          });
      },
    );

  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  goToHome() {
    this.router.navigateByUrl('/page/dashboard');

  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }

  logout() {
    localStorage.removeItem('key');
    localStorage.clear();
    this.router.navigateByUrl('/auth/login');
  }
}

