export class ClientData {
    ClientName: string;
    ClientEmail: string;
    ClientTel: string;
    ClientAddress: string;
    ClientTaxid: string;
    ContactName: string;
    ContactEmail: string;
    ContactTel: string;
}
