import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { LocalDataSource } from 'ng2-smart-table';
// import { SmartTableData } from '../../../@core/data/smart-table';
import { ActivatedRoute, Params, Router } from '@angular/router';


@Component({
  selector: 'ngx-client-info',
  templateUrl: './client-info.component.html',
  styleUrls: ['./client-info.component.scss'],
})

export class ClientInfoComponent implements OnInit {

  Done = false;
  id: any;

  ClientContact = [];
  ClientId = '';
  ClientName = '';
  ClientNameEn = '';
  ClientEmail = '';
  ClientTel = '';
  ClientAddress = '';
  ClientTaxId = '';
  ClientPicturePath: any;

  setting = {
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      ContactNameEn: {
        title: 'Name(EN)',
        type: 'string',
        filter: false,
      },
      ContactName: {
        title: 'Name(TH)',
        type: 'string',
        filter: false,
      },
      ContactEmail: {
        title: 'Email',
        type: 'string',
        filter: false,
      },
      ContactTel: {
        title: 'Telephone Number',
        type: 'number',
        filter: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private clientService: ClientService,
    activeid: ActivatedRoute,
    private router: Router) {

    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });
  }

  ngOnInit() {
    this.Done = true;
    this.clientService.getClientWithAuth(this.id).subscribe(
      data => {
        this.Done = false;
        // tslint:disable-next-line: no-console
        console.log(data);
        this.ClientId = data.ClientId;
        this.ClientName = data.ClientName;
        this.ClientNameEn = data.ClientNameEn;
        this.ClientEmail = data.ClientEmail;
        this.ClientTel = data.ClientTel;
        this.ClientAddress = data.ClientAddress;
        this.ClientTaxId = data.ClientTaxId;
        this.source = data.ClientContact;
        this.ClientPicturePath = 'http://206.189.34.171:7777/client/img/' + data.ClientPicturePath;
      },
      error => {
        this.Done = false;
        console.error(error);
      });
  }

  onEdit(Byid) {
    this.router.navigateByUrl('/page/client/edit/' + Byid);
  }
}
