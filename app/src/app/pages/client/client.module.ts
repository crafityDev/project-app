import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// modules
import { ThemeModule } from '../../@theme/theme.module';
import { ClientRoutingModule, clientPages } from './client-routing.module';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';

@NgModule({
    imports: [
        ThemeModule,
        ClientRoutingModule,
        Ng2SmartTableModule,
        NgxLoadingModule.forRoot({
            animationType: ngxLoadingAnimationTypes.circleSwish,
        }),
    ],
    declarations: [clientPages],
})
export class ClientModule { }
