import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.scss'],
})
export class ClientEditComponent implements OnInit {

  clientForm: FormGroup;
  Done = false;
  id: string;
  ContactData: any;

  ClientPicturePath: any;
  bodypic: any;
  selectedFile = [];

  setting = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      ContactNameEn: {
        title: 'Name (EN)',
        type: 'string',
      },
      ContactName: {
        title: 'Name (TH)',
        type: 'string',
      },
      ContactEmail: {
        title: 'Email',
        type: 'string',
      },
      ContactTel: {
        title: 'Telephone Number',
        type: 'number',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private clientService: ClientService,
    private router: Router,
    activeid: ActivatedRoute,
    private toastrService: NbToastrService) {

    this.clientForm = new FormGroup({
      ClientName: new FormControl('', Validators.required),
      ClientNameEn: new FormControl('', Validators.required),
      ClientEmail: new FormControl('', [Validators.required, Validators.email]),
      ClientTel: new FormControl('', Validators.required),
      ClientAddress: new FormControl('', Validators.required),
      ClientTaxId: new FormControl('', Validators.required),
    });

    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });
  }

  ngOnInit() {
    this.Done = true;
    this.getdata();
  }

  onSaveClient() {
    this.Done = true;
    this.clientService.putClientWithAuth(this.clientForm.value).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log('Save' + response);
        this.onUploadPic();
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  onDeleteClient() {
    this.Done = true;
    this.clientService.deleteClientWithAuth(this.id).subscribe(
      (response) => {
        this.Done = false;
        this.toastrService.success('', 'A client has been removed!', {});
        // tslint:disable-next-line: no-console
        console.log(response);
        this.router.navigateByUrl('/page/client/list/A-Z');
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  onCreateConfirm(event) {
    if (window.confirm('Are you sure you want to Add?')) {
      event.confirm.resolve(event.newData);
      this.Done = true;
      this.ContactData = {
        ClientId: this.id,
        ContactNameEn: event.newData.ContactNameEn,
        ContactName: event.newData.ContactName,
        ContactEmail: event.newData.ContactEmail,
        ContactTel: event.newData.ContactTel,
      };
      // tslint:disable-next-line: no-console
      console.log(this.ContactData);
      this.clientService.postContactWithAuth(this.ContactData).subscribe(
        data => {
          this.toastrService.success('', 'Add contact success!', {});
          // tslint:disable-next-line: no-console
          console.log(data);
          this.getdata();
        },
        error => {
          this.Done = false;
          this.getdata();
          this.toastrService.danger(error.message, 'Something went wrong!', {});
          console.error(error);
        });
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event): void {
    if (window.confirm('Are you sure you want to Save?')) {
      event.confirm.resolve(event.newData);
      this.Done = true;
      this.ContactData = {
        ClientId: this.id,
        ContactId: event.newData.ContactId,
        ContactName: event.newData.ContactName,
        ContactNameEn: event.newData.ContactNameEn,
        ContactEmail: event.newData.ContactEmail,
        ContactTel: event.newData.ContactTel,
      };
      this.clientService.putContactWithAuth(this.ContactData).subscribe(
        data => {
          this.Done = false;
          this.toastrService.success('', 'Edit contact success!', {});
          // tslint:disable-next-line: no-console
          console.log(data);
        },
        error => {
          this.Done = false;
          this.getdata();
          this.toastrService.danger(error.message, 'Something went wrong!', {});
          console.error(error);
        });
      // tslint:disable-next-line: no-console
      console.log(this.ContactData);
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.Done = true;
      this.clientService.deleteContactWithAuth(this.id, event.data.ContactId).subscribe(
        data => {
          this.Done = false;
          this.toastrService.success('', 'A contact has been removed!', {});
          // tslint:disable-next-line: no-console
          console.log(data);
        },
        error => {
          this.Done = false;
          this.getdata();
          this.toastrService.danger(error.message, 'Something went wrong!', {});
          console.error(error);
        });
    } else {
      event.confirm.reject();
    }
  }

  onCancel() {
    this.router.navigateByUrl('/page/client/info/' + this.id);
  }

  onFileSelected(event) {
    this.selectedFile = event.target.files[0];
  }

  onUploadPic() {
    this.bodypic = {
      type: this.selectedFile[0].name.split('.')[1],
      name: this.selectedFile[0].name.split('.')[0],
      ClientId: this.id,
    };
    this.clientService.putImgWithAuth(this.bodypic, this.selectedFile[0]).subscribe(
      data => {
        this.Done = false;
        this.toastrService.success('', 'Edit success!', {});
        this.router.navigateByUrl('/page/client/info/' + this.id);
      },
      error => {
        this.Done = false;
        this.toastrService.danger('Please try again in a few minutes.', 'Cannot upload this picture.', {});
        console.error(error);
      });
  }

  getBlob() {
    this.Done = true;
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[0] = this.blobToFile(xhr.response
          , this.ClientPicturePath.split('/')[this.ClientPicturePath.split('/').length - 1]);
        this.Done = false;
      } else {
        this.Done = false;
        this.selectedFile = [];
      }
    };
    xhr.open('GET', this.ClientPicturePath, true);
    xhr.send();
  }

  blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;
    // A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    // Cast to a File() type
    return <File>theBlob;
  }

  getdata() {
    this.clientService.getClientWithAuth(this.id).subscribe(
      data => {
        this.Done = false;
        this.source = data.ClientContact;
        this.ClientPicturePath = 'http://206.189.34.171:7777/client/img/' + data.ClientPicturePath;
        // console.log(this.ClientPicturePath);
        this.getBlob();
        this.clientForm = new FormGroup({
          ClientId: new FormControl(this.id),
          ClientName: new FormControl(data.ClientName, Validators.required),
          ClientNameEn: new FormControl(data.ClientNameEn, Validators.required),
          ClientEmail: new FormControl(data.ClientEmail, [Validators.required, Validators.email]),
          ClientTel: new FormControl(data.ClientTel, Validators.required),
          ClientAddress: new FormControl(data.ClientAddress, Validators.required),
          ClientTaxId: new FormControl(data.ClientTaxId, Validators.required),
        });
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        this.Done = false;
        console.error(error);
      });
  }
}

