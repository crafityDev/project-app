import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { ClientService } from '../client.service';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss'],
})
export class ClientListComponent implements OnInit {
  Done = false;
  isGridView = true;
  Bysort: any;
  ClientId = '';
  Contact = [];
  clients: any[] = [];
  filterclients: any[] = [];

  private _searchTerm: string;

  get searchTerm(): string {
    return this._searchTerm;
  }
  set searchTerm(value: string) {
    this._searchTerm = value;
    this.filterclients = this.filters(value);
  }

  filters(searchString: string) {
    return this.clients.filter(c => c.ClientName.toLowerCase().indexOf(searchString.toLowerCase()) !== -1);
  }

  selectedSort = [
    { value: 'A-Z', Show: 'Clients title A-Z' },
    { value: 'Z-A', Show: 'Clients title Z-A' },
  ];

  setting = {
    mode: 'external',
    actions: {
      add: false,
      edit: true,
      delete: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      ClientName: {
        title: 'Company Name',
        type: 'string',
      },
      ClientEmail: {
        title: 'Company E-mail',
        type: 'string',
      },
      ClientTel: {
        title: 'Company Telephone',
        type: 'string',
      },
      ContactName: {
        title: 'Contact Person',
        type: 'string',
      },
      ContactEmail: {
        title: 'Contact E-mail',
        type: 'string',
      },
      ContactTel: {
        title: 'Contact Telephone',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private router: Router,
    private clientService: ClientService,
    activeid: ActivatedRoute,
    private toastrService: NbToastrService) {
    activeid.params.subscribe((params: Params) => {
      this.Bysort = params['Bysort'];
      this.Contact = [];
      this.Done = true;
      this.getData();
    });
  }

  ngOnInit() {
  }

  goToInfo(Byid) {
    this.router.navigateByUrl('/page/client/info/' + Byid);
  }

  onEdit(Byid) {
    this.router.navigateByUrl('/page/client/edit/' + Byid);
  }

  TableToInfo(event) {
    this.router.navigateByUrl('/page/client/info/' + event.data.ClientId);
  }

  onEditConfirm(event): void {
    this.router.navigateByUrl('/page/client/edit/' + event.data.ClientId);
  }

  onDelete(Byid) {
    this.Done = true;
    this.clientService.deleteClientWithAuth(Byid).subscribe(
      (response) => {
        this.toastrService.success('', 'A client has been removed!', {});
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getData();
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  onDeleteConfirm(event): void {
    this.Done = true;
    this.clientService.deleteClientWithAuth(event.data.ClientId).subscribe(
      (response) => {
        this.toastrService.success('', 'A client has been removed!', {});
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getData();
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  getData() {
    this.clientService.Sort(this.Bysort).subscribe(
      data => {
        this.clients = data;
        this.filterclients = data;
        for (let i = 0; i < this.clients.length; i++) {
          if (this.clients[i].ClientContact.length > 0) {
            this.Contact.push(
              {
                'ClientId': this.clients[i].ClientId,
                'ClientName': this.clients[i].ClientNameEn,
                'ClientEmail': this.clients[i].ClientEmail,
                'ClientTel': this.clients[i].ClientTel,
                'ContactName': this.clients[i].ClientContact[0].ContactName,
                'ContactEmail': this.clients[i].ClientContact[0].ContactEmail,
                'ContactTel': this.clients[i].ClientContact[0].ContactTel,
              });
          } else {
            this.Contact.push(
              {
                'ClientId': this.clients[i].ClientId,
                'ClientName': this.clients[i].ClientNameEn,
                'ClientEmail': this.clients[i].ClientEmail,
                'ClientTel': this.clients[i].ClientTel,
              });
          }
        }
        this.Done = false;
        // tslint:disable-next-line: no-console
        console.log(this.clients);
        this.source.load(this.Contact);
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  selectSort(event) {
    this.router.navigateByUrl('/page/client/list/' + event);
  }

}
