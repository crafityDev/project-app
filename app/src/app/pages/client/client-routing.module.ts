import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientComponent } from './client.component';
import { ClientAddComponent } from './client-add/client-add.component';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientEditComponent } from './client-edit/client-edit.component';
import { ClientInfoComponent } from './client-info/client-info.component';

const routes: Routes = [{
  path: '',
  component: ClientComponent,
  children: [{
    path: 'add',
    component: ClientAddComponent,
  }, {
    path: 'list/:Bysort',
    component: ClientListComponent,
  }, {
    path: 'info/:Byid',
    component: ClientInfoComponent,
  }, {
    path: 'edit/:Byid',
    component: ClientEditComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientRoutingModule {
}

export const clientPages = [
  ClientComponent,
  ClientAddComponent,
  ClientListComponent,
  ClientInfoComponent,
  ClientEditComponent,
];
