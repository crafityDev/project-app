import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ClientService {
    constructor(private httpClient: HttpClient) { }

    // Normal API
    getAllClient() {
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallclientdata');
    }

    getClient(id) {
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getclientdata/' + id);
    }

    putClient(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.httpClient.put('http://206.189.34.171:7777/mt/putclientdata'
            , body, {
                headers,
            });
    }

    postClient(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.httpClient.post('http://206.189.34.171:7777/mt/postclientdata', body, {
            headers,
        });
    }

    deleteClient(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('body', body);
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deleteclientdata', {
            headers: headers,
        });
    }

    // Auth API
    getAllClientWithAuth() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallclientsortbyname', {
            headers: headers,
        });
    }

    Sort(data) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getclientsortSC/' + data, {
            headers: headers,
        });
    }

    getClientWithAuth(id) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getclientdata/' + id, {
            headers: headers,
        });
    }

    putClientWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/putclientdata', body, {
            headers: headers,
        });
    }

    postClientWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/postclientdata', body, {
            headers: headers,
        });
    }

    deleteClientWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('ClientId', body);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deleteclientdata', {
            headers: headers,
        });
    }

    postContactWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/postcontactdata', body, {
            headers: headers,
        });
    }

    putContactWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.put<any>('http://206.189.34.171:7777/mt/putcontactdata', body, {
            headers: headers,
        });
    }

    deleteContactWithAuth(CliId, ConId) {
        const ClientId = JSON.parse(JSON.stringify(CliId));
        const ContactId = JSON.parse(JSON.stringify(ConId));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('ClientId', ClientId);
        headers = headers.append('ContactId', ContactId);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletecontactdata', {
            headers: headers,
        });
    }

    putImgWithAuth(data, path) {
        const body = JSON.stringify(data);
        const formData: FormData = new FormData();
        formData.append('data', body);
        formData.append('path', path, path.name);
        formData.append('page', 'client');
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // headers = headers.append('enctype', 'multipart/form-data');
        return this.httpClient.put('http://206.189.34.171:7777/mt/uploadPic', formData, {
            headers: headers,
        });
    }
}
