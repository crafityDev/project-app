import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-client-add',
  templateUrl: './client-add.component.html',
  styleUrls: ['./client-add.component.scss'],
})
export class ClientAddComponent implements OnInit {
  public loading = false;
  clientForm: FormGroup;

  ClientPicturePath: any;
  bodypic: any;
  selectedFile = [];
  body: any;
  ClientContact = [];

  setting = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
      mode: 'inline',
    },
    columns: {
      ContactNameEn: {
        title: 'Name (EN)',
        type: 'string',
      },
      ContactName: {
        title: 'Name (TH)',
        type: 'string',
      },
      ContactEmail: {
        title: 'Email',
        type: 'string',
      },
      ContactTel: {
        title: 'Telephone Number',
        type: 'number',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(
    private clientService: ClientService,
    private router: Router,
    private toastrService: NbToastrService) {

    this.clientForm = new FormGroup({
      ClientName: new FormControl('', Validators.required),
      ClientNameEn: new FormControl('', Validators.required),
      ClientEmail: new FormControl('', Validators.email),
      ClientTel: new FormControl(''),
      ClientAddress: new FormControl('', Validators.required),
      ClientTaxId: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    this.ClientPicturePath = 'http://206.189.34.171:7777/default/img/dummy.png';
    this.getBlob();
  }

  onAddClient() {
    this.loading = true;
    this.body = {
      CompanyId: localStorage.getItem('companyId'),
      ClientName: this.clientForm.get('ClientName').value,
      ClientNameEn: this.clientForm.get('ClientNameEn').value,
      ClientEmail: this.clientForm.get('ClientEmail').value,
      ClientTel: this.clientForm.get('ClientTel').value,
      ClientAddress: this.clientForm.get('ClientAddress').value,
      ClientTaxId: this.clientForm.get('ClientTaxId').value,
    };
    this.clientService.postClientWithAuth(this.body).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.toastrService.success('', 'A new client has been added!', {});
        this.bodypic = {
          type: this.selectedFile[0].name.split('.')[1],
          name: this.selectedFile[0].name.split('.')[0],
          ClientId: response.ClientId,
        };
        this.onUploadPic();
        for (let i = 0; i < this.ClientContact.length; i++) {
          this.ClientContact[i].ClientId = response.ClientId;
          this.clientService.postContactWithAuth(this.ClientContact[i]).subscribe(
            (res) => {
              // tslint:disable-next-line: no-console
              console.log(res);
            },
            error => {
              this.toastrService.danger(error.message, 'Contact went wrong!', {});
              console.error(error);
            });
        }
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  onCreateConfirm(event) {
    if (window.confirm('Are you sure you want to Add?')) {
      event.confirm.resolve(event.newData);
      this.ClientContact.push(event.newData);
      // tslint:disable-next-line: no-console
      console.log(this.ClientContact);
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event): void {
    if (window.confirm('Are you sure you want to Save?')) {
      event.confirm.resolve(event.newData);
      this.ClientContact[this.ClientContact.indexOf(event.data)] = event.newData;
      // tslint:disable-next-line: no-console
      console.log(this.ClientContact);
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.ClientContact.splice(this.ClientContact.indexOf(event.data), 1);
      // tslint:disable-next-line: no-console
      console.log(this.ClientContact);
    } else {
      event.confirm.reject();
    }
  }

  onFileSelected(event) {
    this.selectedFile[0] = event.target.files[0];
    const reader = new FileReader();
    // this.selectedFile = event;
    reader.readAsDataURL(this.selectedFile[0]);
    reader.onload = (_event) => {
      this.ClientPicturePath = reader.result;
    };
  }

  onUploadPic() {
    this.clientService.putImgWithAuth(this.bodypic, this.selectedFile[0]).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.loading = false;
        this.goToInfo(this.bodypic.ClientId);
      },
      error => {
        this.toastrService.danger('Please try again in a few minutes.', 'Cannot upload this picture.', {});
        console.error(error);
      });
  }

  goToInfo(Byid) {
    this.router.navigateByUrl('/page/client/info/' + Byid);
  }

  getBlob() {
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[0] = this.blobToFile(xhr.response
          , this.ClientPicturePath.split('/')[this.ClientPicturePath.split('/').length - 1]);
      } else {
        this.selectedFile = [];
      }
    };
    xhr.open('GET', this.ClientPicturePath, true);
    xhr.send();
  }

  blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;
    // A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    // Cast to a File() type
    return <File>theBlob;
  }
}

