import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { DocumentRoutingModule, DocroutedComponents } from './document-routing.module';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';


@NgModule({
  imports: [
    ThemeModule,
    DocumentRoutingModule,
    Ng2SmartTableModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.circleSwish,
    }),
  ],
  declarations: [DocroutedComponents],
})
export class DocumentModule { }
