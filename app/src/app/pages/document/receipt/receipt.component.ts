import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { DocumentService } from '../document.service';
import { ProjectService } from '../../project/project.service';
import { NbToastrService } from '@nebular/theme';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.scss'],
})
export class ReceiptComponent implements OnInit {
  Done = false;
  Docs = [];
  setting = {
    actions: {
      add: false,
      edit: false,
      delete: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      ProjectName: {
        title: 'Project name',
        type: 'string',
      },
      ProjectHeadName: {
        title: 'Project manager',
        type: 'string',
      },
      ClientName: {
        title: 'Client',
        type: 'string',
      },
      PDFnum: {
        title: 'No.',
        type: 'string',
      },
      CreateDate: {
        title: 'Date',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private projectService: ProjectService,
    private toastrService: NbToastrService,
    private docService: DocumentService,
    private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.Done = true;
    this.getDoc();
  }

  onDeleteConfirm(event): void {
    this.Done = true;
    this.projectService.deleteRec(event.data.ReceiptPDFId).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.toastrService.success('', 'A document has been removed!', {});
        this.getDoc();
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  TableToInfo(event) {
    window.open('http://206.189.34.171:7777/receipt/pdf/' + event.data.PDFPath, '_blank');
  }

  getDoc() {
    this.docService.getRec().subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.Docs = response;
        for (let i = 0; i < this.Docs.length; i++) {
          this.Docs[i].CreateDate = this.datePipe.transform(new Date(this.Docs[i].CreateDate * 1000), 'MMM d, y');
        }
        this.Done = false;
        this.source.load(this.Docs);
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }
}
