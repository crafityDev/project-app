import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class DocumentService {
    constructor(private httpClient: HttpClient) { }

    getQuo() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallquotation', {
            headers: headers,
        });
    }

    getInv() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallinvoice', {
            headers: headers,
        });
    }

    getRec() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallreceipt', {
            headers: headers,
        });
    }

    getPur() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallpo', {
            headers: headers,
        });
    }
}
