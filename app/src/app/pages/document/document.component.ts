import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'ngx-document',
  styleUrls: ['./document.component.scss'],
  templateUrl: './document.component.html',
})
export class DocumentComponent implements OnInit {

  items: NbMenuItem[] = [
    {
      title: 'Quotation',
      link: '/page/doc/quotation',
    },
    {
      title: 'Purchase Order',
      link: '/page/doc/purchase-order',
    },
    {
      title: 'Invoice',
      link: '/page/doc/invoice',
    },
    {
      title: 'Receipt',
      link: '/page/doc/receipt',
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
