import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentComponent } from './document.component';
import { QuotationComponent } from './quotation/quotation.component';
import { PurchaseOrderComponent } from './purchase-order/purchase-order.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { ReceiptComponent } from './receipt/receipt.component';

const routes: Routes = [{
    path: '',
    component: DocumentComponent,
    children: [{
        path: 'quotation',
        component: QuotationComponent,
    }, {
        path: 'purchase-order',
        component: PurchaseOrderComponent,
    }, {
        path: 'invoice',
        component: InvoiceComponent,
    }, {
        path: 'receipt',
        component: ReceiptComponent,
    }],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DocumentRoutingModule {
}

export const DocroutedComponents = [
    DocumentComponent,
    QuotationComponent,
    PurchaseOrderComponent,
    InvoiceComponent,
    ReceiptComponent,
];
