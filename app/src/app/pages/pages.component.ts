import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { CompanyService } from './company/company.service';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-main-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-main-layout>
  `,
})
export class PagesComponent implements OnInit {

  isCompanyAdd = false;

  constructor(private companyService: CompanyService) {
  }

  ngOnInit(): void {
    this.companyService.isCompanyAdd.subscribe((data) => {
      this.isCompanyAdd = data;
    });
  }

  menu = MENU_ITEMS;
}
