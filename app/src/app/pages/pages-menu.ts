import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/page/dashboard',
    home: true,
  },
  {
    title: 'Project',
    icon: 'nb-star',
    link: '/page/project/list/A-Z',
  },
  {
    title: 'Client',
    icon: 'nb-heart',
    link: '/page/client/list/A-Z',
  },
  {
    title: 'Document',
    icon: 'nb-compose',
    children: [
      {
        title: 'Quotation',
        link: '/page/doc/quotation',
      },
      {
        title: 'Purchase Order',
        link: '/page/doc/purchase-order',
      },
      {
        title: 'Invoice',
        link: '/page/doc/invoice',
      },
      {
        title: 'Receipt',
        link: '/page/doc/receipt',
      },
    ],
  },
  {
    title: 'Company',
    icon: 'nb-edit',
    children: [
      {
        title: 'Company-info',
        link: '/page/company/company-info',
      },
      {
        title: 'Staffs',
        link: '/page/company/staff-list/A-Z',
      },
      {
        title: 'Bank Account',
        link: '/page/company/bank-account',
      },
    ],
  },
];
