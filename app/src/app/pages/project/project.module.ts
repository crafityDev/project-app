import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { NbDialogModule } from '@nebular/theme';
import { ProjectRoutingModule, projectPages } from './project-routing.module';
import { TaskService } from './project-info/shared/task.service';
import { ChartComponent } from './project-info/project-dashboard/chart.component';
import { ChartModule } from 'angular2-chartjs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TaskEditComponent } from './project-info/tasks/task-edit/task-edit.component';
import { TaskAddComponent } from './project-info/tasks/task-add/task-add.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { EditableModule } from './project-info/shared/editable/editable.module';
import { PaymentComponent } from './project-info/payment/payment.component';
import { DragulaModule } from 'ng2-dragula';
import { SprintInfoComponent } from './project-info/sprint/sprint-info/sprint-info.component';
import { PaymentEditComponent } from './project-info/payment-edit/payment-edit.component';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { SprintEditComponent } from './project-info/sprint/sprint-edit/sprint-edit.component';
import { GetBacklogComponent } from './project-info/sprint/get-backlog/get-backlog.component';
import { OtherComponent } from './project-info/document/other/other.component';
// import { PaymentAddComponent } from './project-info/payment-add/payment-add.component';
// import { PaymentEditComponent } from './project-info/payment-edit/payment-edit.component';

@NgModule({
  imports: [
    ThemeModule,
    ChartModule,
    NgxChartsModule,
    ProjectRoutingModule,
    Ng2SmartTableModule,
    PdfViewerModule,
    EditableModule,
    NbDialogModule.forChild(),
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.circleSwish,
    }),
    DragulaModule,
  ],
  declarations: [
    projectPages,
    ChartComponent,
    TaskEditComponent,
    TaskAddComponent,
    PaymentComponent,
    SprintInfoComponent,
    PaymentEditComponent,
    SprintEditComponent,
    GetBacklogComponent,
    OtherComponent,
    // PaymentAddComponent,
    // PaymentEditComponent,
  ],
  providers: [
    TaskService,
  ],
})
export class ProjectModule { }
