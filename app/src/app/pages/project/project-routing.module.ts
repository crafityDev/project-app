import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectComponent } from './project.component';
import { ProjectAddComponent } from './project-add/project-add.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectDashboardComponent } from './project-info/project-dashboard/project-dashboard.component';
import { ProjectInfoComponent } from './project-info/project-info.component';
import { StaffsComponent } from './project-info/staffs/staffs.component';
import { MindmapComponent } from './project-info/mindmap/mindmap.component';
import { TasksComponent } from './project-info/tasks/tasks.component';
import { InvoiceComponent } from './project-info/document/invoice/invoice.component';
import { QuotationComponent } from './project-info/document/quotation/quotation.component';
import { ReceiptComponent } from './project-info/document/receipt/receipt.component';
import { PurchaseOrderComponent } from './project-info/document/purchase-order/purchase-order.component';
import { ProjectDocumentComponent } from './project-info/document/document.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { DetailComponent } from './project-info/detail/detail.component';
import { SprintComponent } from './project-info/sprint/sprint.component';
import { QuoAddComponent } from './project-info/document/quo-add/quo-add.component';
import { PurAddComponent } from './project-info/document/pur-add/pur-add.component';
import { InvAddComponent } from './project-info/document/inv-add/inv-add.component';
import { RecAddComponent } from './project-info/document/rec-add/rec-add.component';
import { SprintAddComponent } from './project-info/sprint-add/sprint-add.component';
import { PaymentComponent } from './project-info/payment/payment.component';
import { SprintInfoComponent } from './project-info/sprint/sprint-info/sprint-info.component';
import { PaymentEditComponent } from './project-info/payment-edit/payment-edit.component';
import { OtherComponent } from './project-info/document/other/other.component';


const routes: Routes = [{
  path: '',
  component: ProjectComponent,
  children: [
    {
      path: 'add',
      component: ProjectAddComponent,
    },
    {
      path: 'list/:Bysort',
      component: ProjectListComponent,
    },
    {
      path: 'info',
      component: ProjectInfoComponent,
      children: [
        { path: 'dashboard/:Byid', component: ProjectDashboardComponent },
        { path: 'staffs/:Byid', component: StaffsComponent },
        { path: 'mindmap/:Byid', component: MindmapComponent },
        { path: 'tasks/:Byid', component: TasksComponent },
        { path: 'sprint/:Byid', component: SprintComponent },
        { path: 'detail/:Byid', component: DetailComponent },
        { path: 'edit/:Byid', component: ProjectEditComponent },
        { path: 'cost/:Byid', component: PaymentComponent },
        { path: 'cost-edit/:Byid', component: PaymentEditComponent },
        { path: 'sprint-add', component: SprintAddComponent },
        { path: 'sprint-info', component: SprintInfoComponent },
        {
          path: 'docs', component: ProjectDocumentComponent,
          children:
            [{ path: 'quotation', component: QuotationComponent },
            { path: 'purchase-order', component: PurchaseOrderComponent },
            { path: 'invoice', component: InvoiceComponent },
            { path: 'receipt', component: ReceiptComponent },
            { path: 'quo-add', component: QuoAddComponent },
            { path: 'pur-add', component: PurAddComponent },
            { path: 'inv-add', component: InvAddComponent },
            { path: 'rec-add', component: RecAddComponent },
            { path: 'other', component: OtherComponent },
            ],
        },
      ],
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectRoutingModule { }

export const projectPages = [
  ProjectComponent,
  ProjectAddComponent,
  ProjectListComponent,
  ProjectDashboardComponent,
  ProjectInfoComponent,
  ProjectEditComponent,
  StaffsComponent,
  MindmapComponent,
  TasksComponent,
  ProjectDocumentComponent,
  ProjectAddComponent,
  InvoiceComponent,
  QuotationComponent,
  ReceiptComponent,
  PurchaseOrderComponent,
  DetailComponent,
  SprintComponent,
  SprintAddComponent,
  SprintInfoComponent,
  QuoAddComponent,
  InvAddComponent,
  PurAddComponent,
  RecAddComponent,
  PaymentComponent,
  // PaymentAddComponent,
  // PaymentEditComponent,
];
