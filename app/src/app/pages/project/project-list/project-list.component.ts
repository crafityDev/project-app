import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProjectService } from '../project.service';
import { NbToastrService, NbDateService } from '@nebular/theme';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'ngx-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss'],
})
export class ProjectListComponent implements OnInit {

  Done = false;
  isGridView = true;
  isFav = false;

  selectedForm: FormGroup;
  body: any;
  Bysort: any;

  project: any[] = [];
  filterproject: any[] = [];
  private _searchTerm: string;

  get searchTerm(): string {
    return this._searchTerm;
  }
  set searchTerm(value: string) {
    this._searchTerm = value;
    this.filterproject = this.filters(value);
  }

  filters(searchString: string) {
    return this.project.filter(p => p.ProjectName.toLowerCase().indexOf(searchString.toLowerCase()) !== -1);
  }

  selectedSort = [
    { value: 'A-Z', Show: 'Project title A-Z' },
    { value: 'Z-A', Show: 'Project title Z-A' },
    { value: 'MProgress', Show: 'Most Progress' },
    { value: 'LProgress', Show: 'Least Progress' },
    { value: 'MDays', Show: 'Most Days left' },
    { value: 'LDays', Show: 'Least Days left' },
    { value: 'pin', Show: 'Pin Project' },
  ];

  setting = {
    mode: 'external',
    actions: {
      add: false,
      edit: true,
      delete: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      ProjectName: {
        title: 'Project Title',
        type: 'string',
      },
      ProjectStatus: {
        title: 'Status',
        type: 'string',
      },
      ProjectLink: {
        title: 'Link',
        type: 'string',
      },
      ProjectClientName: {
        title: 'Client',
        type: 'string',
      },
      ProjectHeadName: {
        title: 'Project Manager',
        type: 'string',
      },
      ProjectHeadTel: {
        title: 'Telephone Number',
        type: 'string',
      },
    },
  };

  staffPics: any[] = [];
  daysLeft: any[] = [];

  source: LocalDataSource = new LocalDataSource();

  constructor(
    private route: Router,
    activeid: ActivatedRoute,
    private projectService: ProjectService,
    private toastrService: NbToastrService,
    protected dateService: NbDateService<Date>) {

    const value = '';
    const Show = '';
    this.selectedForm = new FormGroup({
      value: new FormControl(value),
      Show: new FormControl(Show),
    });

    activeid.params.subscribe((params: Params) => {
      this.Bysort = params['Bysort'];
      this.staffPics = [];
      this.daysLeft = [];
      this.getData();
    });
  }

  ngOnInit() {
  }

  isFavToggle() {
    this.isFav = !this.isFav;
  }

  goToInfo(Byid): void {
    localStorage.setItem('project', Byid);
    this.route.navigateByUrl('/page/project/info/dashboard/' + Byid);
  }

  onEdit(Byid) {
    localStorage.setItem('project', Byid);
    this.route.navigateByUrl('/page/project/info/edit/' + Byid);
  }

  TableToInfo(event) {
    this.route.navigateByUrl('/page/project/info/dashboard/' + event.data.ProjectId);
  }

  onEditConfirm(event): void {
    this.route.navigateByUrl('/page/project/info/edit/' + event.data.ProjectId);
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  onDelete(Byid) {
    this.Done = true;
    this.projectService.deleteProjectWithAuth(Byid).subscribe(
      (response) => {
        this.toastrService.success('', 'A project has been removed!', {});
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getData();
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  onDeleteConfirm(event): void {
    this.Done = true;
    this.projectService.deleteProjectWithAuth(event.data.ProjectId).subscribe(
      (response) => {
        this.toastrService.success('', 'A project has been removed!', {});
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getData();
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  pinProject(id, pin) {
    this.body = {
      ProjectId: id,
      ProjectPinned: !pin,
    };
    // console.log(this.body)
    this.projectService.pinProject(this.body).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getData();
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  getData() {
    this.projectService.Sort(this.Bysort).subscribe(
      data => {
        this.setData(data);
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  setData(data) {
    this.Done = true;
    this.project = data;
    this.filterproject = data;
    for (const temp of this.project) {
      this.staffPics.push(temp.ProjectStaffPic);
      this.daysLeft.push(
        Math.round(
          (temp.ProjectEnd * 1000 - Math.round(this.dateService.today().getTime())) / (1000 * 60 * 60 * 24)));
    }
    this.Done = false;
    // console.log(this.staffPics);
    this.source.load(this.project);
    // tslint:disable-next-line: no-console
    console.log(this.project);
  }

  selectSort(event) {
    this.route.navigateByUrl('/page/project/list/' + event);
  }

}
