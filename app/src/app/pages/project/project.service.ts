import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ProjectService {
    test: any;
    constructor(private httpClient: HttpClient) { }

    // common
    getAllProjectWithAuth() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getprojectsortbyname', {
            headers: headers,
        });
    }

    getProjectWithAuth(id) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getprojectdata/' + id, {
            headers: headers,
        });
    }

    putProjectWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/putprojectdata', body, {
            headers: headers,
        });
    }

    postProjectWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/postprojectdata', body, {
            headers: headers,
        });
    }

    deleteProjectWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        headers = headers.append('ProjectId', body);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deleteprojectdata', {
            headers: headers,
        });
    }

    getAllProjectStaff() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getstaffnameandpic', {
            headers: headers,
        });
    }

    getAllProjectClient() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getclientnameandpic', {
            headers: headers,
        });
    }

    // staff
    postProjectStaffWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/postprojectstaffdata', body, {
            headers: headers,
        });
    }

    putProjectStaffWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/putprojectstaffdata', body, {
            headers: headers,
        });
    }

    deleteProjectStaffWithAuth(stf) {
        const STF = JSON.parse(JSON.stringify(stf));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('ProjectId', localStorage.getItem('project'));
        headers = headers.append('StaffId', STF);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deleteprojectstaffdata', {
            headers: headers,
        });
    }
    // task
    getAlltask(data) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ProjectId', data);
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getalltask', {
            headers: headers,
        });
    }

    createTask(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/posttaskdata', body, {
            headers: headers,
        });
    }

    editTaskdata(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/puttaskdata', body, {
            headers: headers,
        });
    }

    editMindTask(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/puttaskname', body, {
            headers: headers,
        });
    }

    editMindList(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/putlistname', body, {
            headers: headers,
        });
    }

    deleteTask(tid) {
        const TID = JSON.parse(JSON.stringify(tid));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('TaskId', TID);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletetaskdata', {
            headers: headers,
        });
    }

    deleteTasknobc(sid, lid, tid) {
        const SID = JSON.parse(JSON.stringify(sid));
        const LID = JSON.parse(JSON.stringify(lid));
        const TID = JSON.parse(JSON.stringify(tid));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('SprintId', SID);
        headers = headers.append('ListId', LID);
        headers = headers.append('TaskId', TID);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletetasknobc', {
            headers: headers,
        });
    }
    // list
    getList(data) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ListId', data);
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getlist', {
            headers: headers,
        });
    }

    createList(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/postlistdata', body, {
            headers: headers,
        });
    }

    editList(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/putlistdata', body, {
            headers: headers,
        });
    }

    createNewList(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/addnewlist', body, {
            headers: headers,
        });
    }

    deleteList(sid, lid) {
        const SID = JSON.parse(JSON.stringify(sid));
        const LID = JSON.parse(JSON.stringify(lid));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('SprintId', SID);
        headers = headers.append('ListId', LID);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletelistdata', {
            headers: headers,
        });
    }

    // sprint
    getSprint(data) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getsprintdata/' + data, {
            headers: headers,
        });
    }

    createSprint(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/postsprintdata', body, {
            headers: headers,
        });
    }

    editSprint(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/putsprintdata', body, {
            headers: headers,
        });
    }

    deleteSprint(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('SprintId', body);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletesprintdata', {
            headers: headers,
        });
    }

    updateMain(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/mainsprint', body, {
            headers: headers,
        });
    }

    updateStatusProject(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/updateprojectstatus', body, {
            headers: headers,
        });
    }

    // Doc
    genPDFQuo(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ProjectId', body);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/quotation', body, {
            headers: headers,
        });
    }

    genPDFInv(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ProjectId', body);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/invoice', body, {
            headers: headers,
        });
    }

    genPDFRec(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ProjectId', body);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/receipt', body, {
            headers: headers,
        });
    }

    genPDFPur(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ProjectId', body);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/po', body, {
            headers: headers,
        });
    }

    getQuo() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ProjectId', localStorage.getItem('project'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getprojectquotation', {
            headers: headers,
        });
    }

    getInv() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ProjectId', localStorage.getItem('project'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getprojectinvoice', {
            headers: headers,
        });
    }

    getRec() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ProjectId', localStorage.getItem('project'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getprojectreceipt', {
            headers: headers,
        });
    }

    getPur() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ProjectId', localStorage.getItem('project'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getprojectpo', {
            headers: headers,
        });
    }

    deleteQuo(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('QuotationPDFId', body);
        headers = headers.append('ProjectId', localStorage.getItem('project'));
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletequotation', {
            headers: headers,
        });
    }

    deleteInv(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('InvoicePDFId', body);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deleteinvoice', {
            headers: headers,
        });
    }

    deleteRec(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('ReceiptPDFId', body);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletereceipt', {
            headers: headers,
        });
    }

    deletePur(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('POPDFId', body);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletepo', {
            headers: headers,
        });
    }

    putImgWithAuth(data, path) {
        const body = JSON.stringify(data);
        const formData: FormData = new FormData();
        formData.append('data', body);
        formData.append('path', path, path.name);
        formData.append('page', 'project');
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // headers = headers.append('enctype', 'multipart/form-data');
        return this.httpClient.put('http://206.189.34.171:7777/mt/uploadPic', formData, {
            headers: headers,
        });
    }

    pinProject(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/updatepinproject', body, {
            headers: headers,
        });
    }

    getAllPinProject() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallpinproject', {
            headers: headers,
        });
    }

    // Sort
    Sort(data) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getprojectsortSC/' + data, {
            headers: headers,
        });
    }

    getTasklength(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('SprintId', body);
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallsprinttasknum', {
            headers: headers,
        });
    }

    getTaskNotDone(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('SprintId', body);
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallsprinttasknum', {
            headers: headers,
        });
    }

    getTaskHistory(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('SprintId', body);
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getsprinttasknotdone', {
            headers: headers,
        });
    }

    getCostDescription(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ProjectId', body);
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/quotationDetail', {
            headers: headers,
        });
    }

    postCostDescription(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/quotationDetail', body, {
            headers: headers,
        });
    }

    putCostDescription(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/quotationDetail', body, {
            headers: headers,
        });
    }

    deleteCostDescription(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('ID', body);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/quotationDetail', {
            headers: headers,
        });
    }

    UploadFile(data, path) {
        const body = JSON.stringify(data);
        const formData: FormData = new FormData();
        formData.append('data', body);
        formData.append('path', path, path.name);
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/uploadotherFile', formData, {
            headers: headers,
        });
    }

    getFileName(project, company) {
        const pid = JSON.parse(JSON.stringify(project));
        const cid = JSON.parse(JSON.stringify(company));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('ProjectId', pid);
        headers = headers.append('CompanyId', cid);
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getotherFile', {
            headers: headers,
        });
    }

    getFile(nowUrl, token) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', token == null ? '-' : token);
        return this.httpClient.get(nowUrl, {
            responseType: 'blob',
            headers: headers,
        });
    }

    deleteFile(path, company, file) {
        const body = JSON.parse(JSON.stringify(path));
        const cid = JSON.parse(JSON.stringify(company));
        const fid = JSON.parse(JSON.stringify(file));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('FilePath', body);
        headers = headers.append('FileId', fid);
        headers = headers.append('CompanyId', cid);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deleteotherfile', {
            headers: headers,
        });
    }
}
