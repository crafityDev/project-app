import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, NgForm, FormControl } from '@angular/forms';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { Task, Sprint } from '../project-info/shared/Sprint.model';
import { NbDateService, NbToastrService } from '@nebular/theme';
// import { TaskService } from '../project-info/shared/task.service';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';


@Component({
  selector: 'ngx-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.scss'],
})
export class ProjectAddComponent implements OnInit {

  @ViewChild('projectForm') pForm: NgForm;
  @ViewChild('staffForm') staffForm: NgForm;
  @ViewChild('taskForm') taskForm: NgForm;

  min: Date;
  Done = false;
  ProjectName: string;
  ProjectDesc: string;
  ProjectLink: string;
  ProjectStart: any;
  ProjectEnd: any;
  // ProjectPrice: number;
  ProjectPayment: string;
  // ProjectClientName: string;
  ProjectClient: string;
  // ProjectPercent: string;
  ProjectHead: string;

  ProjectPicturePath: any;
  bodypic: any;
  selectedFile = [];

  postStaff = [];

  // TypeAhead
  staffModel: any;
  managerModel: any;
  clientModel: any;

  isManagerValid: boolean = true;
  isStaffValid: boolean = true;
  isClientValid: boolean = true;

  selectedItem: any;
  manager: { StaffId: string, StaffNameEn: string, StaffPicturePath: string, StaffPositionName: string }[] = [];
  tempid: string;
  staffs: { StaffId: string, StaffNameEn: string, StaffPicturePath: string, StaffPositionName: string }[] = [];
  clients: { ClientId: string, ClientNameEn: string, ClientPicturePath: string }[] = [];

  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  search = (text$: Observable<string>) => {
    return text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.staffs.filter(v => v.StaffNameEn.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10)),
    );
  }
  formatter = (x: { StaffNameEn: string }) => x.StaffNameEn;

  clientSearch = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.clients.filter(v => v.ClientNameEn.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10)),
    )
  clientFormatter = (x: { ClientNameEn: string }) => x.ClientNameEn;

  staffSearch = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.staffs.filter(v => v.StaffNameEn.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10)),
    )
  staffFormatter = (x: { StaffNameEn: string }) => x.StaffNameEn;

  // Stepper
  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;

  staff: any[] = [];
  // clients: any[] = [];
  // tasks: any[] = [];

  closeResult: string;
  projectForm: FormGroup;
  ngModelDate = new Date();
  body: any;

  setting = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      firstName: {
        title: 'Staff Name',
        type: 'string',
      },
      lastName: {
        title: 'Position',
        type: 'string',
      },
    },
  };

  tasks: Task[] = [];
  sprintTasks: Task[] = [];
  sprint: Sprint;
  temp: any;
  isSprint = false;

  constructor(
    // private taskService: TaskService,
    private projectService: ProjectService,
    protected dateService: NbDateService<Date>,
    private route: Router,
    private toastrService: NbToastrService) {
    this.min = this.dateService.addDay(this.dateService.today(), 0);
  }

  ngOnInit() {
    this.getDataForPick();
    this.ProjectPicturePath = 'http://206.189.34.171:7777/default/img/gray.png';
    this.getBlob();
    // this.taskService.allTasks.subscribe(temp => {
    //   this.tasks = temp;
    // });
  }

  addStaff() {
    // this.staff.push(this.staffModel);
    // this.postStaff.push(this.staffModel.StaffId);
    // this.staffForm.reset();

    // tslint:disable-next-line: no-console
    console.log(this.staffs.findIndex(x => x.StaffId === this.tempid));
    this.staff.push(this.staffs[this.staffs.findIndex(x => x.StaffId === this.tempid)]);
    this.postStaff.push(this.tempid);
    this.staffs.splice(this.staffs.findIndex(x => x.StaffId === this.tempid), 1);
    this.selectedItem = new FormControl();
  }

  cancelStaff() {
    this.selectedItem = new FormControl();
  }

  deleteStaff(index: number) {
    this.staffs.push(this.staff[index]);
    this.staff.splice(index, 1);
  }

  addmanager() {
    this.staffs.splice(this.staffs.findIndex(x => x.StaffId === this.ProjectHead), 1);
  }

  resetstaffdata() {
    this.staffs = [];
    this.postStaff = [];
    this.staff = [];
    for (let i = 0; i < this.manager.length; i++) {
      this.staffs.push(this.manager[i]);
    }
  }

  // addTask() {
  //   if (this.taskForm.value.TaskPoint === '' || this.taskForm.value.TaskPoint <= 0) {
  //     this.taskForm.value.TaskPoint = 0;
  //   }
  //   this.tasks.push(this.taskForm.value);
  //   this.taskForm.reset();
  // }

  // cancelTask() {
  //   this.taskForm.reset();
  // }

  // deleteTask(index: number) {
  //   this.tasks.splice(index, 1);
  // }

  selectmanager(event) {
    this.ProjectHead = event;
  }

  selectclient(event) {
    this.ProjectClient = event;
  }

  selectstaff(event) {
    this.tempid = event;
  }

  onAddProject() {
    this.Done = true;
    this.body = {
      CompanyId: localStorage.getItem('companyId'),
      ProjectName: this.ProjectName,
      ProjectDesc: this.ProjectDesc,
      ProjectLink: this.ProjectLink,
      // ProjectPrice: this.ProjectPrice,
      ProjectPayment: this.ProjectPayment,
      ProjectStart: Math.round((this.ProjectStart).getTime() / 1000),
      ProjectEnd: Math.round((this.ProjectEnd).getTime() / 1000),
      // ProjectClient: this.clientModel['ClientId'],
      // ProjectHead: this.managerModel['StaffId'],
      ProjectClient: this.ProjectClient,
      ProjectHead: this.ProjectHead,
      ProjectStaffs: this.postStaff,
    };
    this.projectService.postProjectWithAuth(this.body).subscribe(
      (response) => {
        this.toastrService.success('Uploading data. Please wait for a few minutes.',
          'A new project has been added!', {});
        // tslint:disable-next-line: no-console
        console.log(response);
        localStorage.setItem('project', response.ProjectId);
        this.bodypic = {
          type: this.selectedFile[0].name.split('.')[1],
          name: this.selectedFile[0].name.split('.')[0],
          ProjectId: response.ProjectId,
        };
        this.onUploadPic();
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.error.desc_EN, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  getDataForPick() {
    this.Done = true;
    this.projectService.getAllProjectStaff().subscribe(
      data => {
        this.staffs = data;
        for (const item of data) {
          this.manager.push(item);
        }
        this.Done = false;
      },
      error => {
        this.Done = false;
        // tslint:disable-next-line: no-console
        console.error(error);
      });

    this.projectService.getAllProjectClient().subscribe(
      data => {
        this.clients = data;
      },
      error => {
        this.Done = false;
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  onFileSelected(event) {
    this.selectedFile[0] = event.target.files[0];
    const reader = new FileReader();
    // this.selectedFile = event;
    reader.readAsDataURL(this.selectedFile[0]);
    reader.onload = (_event) => {
      this.ProjectPicturePath = reader.result;
    };
  }

  onUploadPic() {
    this.projectService.putImgWithAuth(this.bodypic, this.selectedFile[0]).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.Done = false;
        this.route.navigateByUrl('/page/project/info/dashboard/' + localStorage.getItem('project'));
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  getBlob() {
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[0] = this.blobToFile(xhr.response
          , this.ProjectPicturePath.split('/')[this.ProjectPicturePath.split('/').length - 1]);
      } else {
        this.selectedFile = [];
      }
    };
    xhr.open('GET', this.ProjectPicturePath, true);
    xhr.send();
  }

  blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;
    // A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    // Cast to a File() type
    return <File>theBlob;
  }

  onChangeManager() {
    const temp = this.manager.findIndex(staff => staff.StaffNameEn === this.managerModel.StaffNameEn);
    if (temp >= 0) { this.isManagerValid = true; } else { this.isManagerValid = false; }
  }

  onChangeStaff() {
    const temp = this.staffs.findIndex(staff => staff.StaffNameEn === this.staffModel.StaffNameEn);
    if (temp >= 0) { this.isStaffValid = true; } else { this.isStaffValid = false; }
  }

  onChangeClient() {
    const temp = this.clients.findIndex(client => client.ClientNameEn === this.clientModel.ClientNameEn);
    if (temp >= 0) { this.isClientValid = true; } else { this.isClientValid = false; }
  }

  startDatePick(event) {
    this.min = event;
  }
}
