import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormGroup } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { NgbModal, ModalDismissReasons, NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { ProjectService } from '../project.service';
import { ClientService } from '../../client/client.service';
import { Router } from '@angular/router';
import { NbDateService } from '@nebular/theme';


@Component({
  selector: 'ngx-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.scss'],
})
export class ProjectEditComponent implements OnInit {
  clients: { ClientId: string, ClientName: string, ClientPicturePath: string }[] = [];
  @ViewChild('projectForm') pForm: NgForm;
  id: string;
  Done = false;
  body: any;
  ProjectName: string;
  ProjectDesc: string;
  ProjectLink: string;
  ProjectStart: any;
  ProjectEnd: any;
  ProjectPayment: string;
  ProjectClientName: string;
  ProjectClient: string;
  ProjectPercent: string;
  ProjectHead: string;

  ProjectPicturePath: any;
  bodypic: any;
  selectedFile = [];

  // TypeAhead
  ClientModel: any;
  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  clientSearch = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.clients.filter(v => v.ClientName.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10)),
    )
  clientFormatter = (x: { ClientName: string }) => x.ClientName;

  closeResult: string;
  projectForm: FormGroup;
  ngModelDate = new Date();

  setting = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      firstName: {
        title: 'Staff Name',
        type: 'string',
      },
      lastName: {
        title: 'Position',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  min: any;

  constructor(private service: SmartTableData,
    private modalService: NgbModal,
    private projectService: ProjectService,
    private clientService: ClientService,
    protected dateService: NbDateService<Date>,
    private route: Router) {
    this.id = localStorage.getItem('project');
    const data = this.service.getData();
    this.source.load(data);

    this.min = this.dateService.addDay(this.dateService.today(), 0);
    // console.log(this.pForm.value.ProjectName);
  }

  ngOnInit() {
    this.Done = true;
    this.projectService.getAllProjectClient().subscribe(
      res => {
        this.clients = res;
      },
      error => {
        this.Done = false;
        // tslint:disable-next-line: no-console
        console.error(error);
      });

    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        this.Done = false;
        // tslint:disable-next-line: no-console
        console.log(data);
        this.ProjectName = data.ProjectName;
        this.ProjectDesc = data.ProjectDesc;
        this.ProjectLink = data.ProjectLink;
        this.ProjectPercent = data.ProjectPercent;
        // this.ProjectPrice = data.ProjectPrice;
        this.ProjectPayment = data.ProjectPayment;
        this.ProjectStart = new Date(data.ProjectStart * 1000);
        this.ProjectEnd = new Date(data.ProjectEnd * 1000);
        this.ProjectHead = data.ProjectHead;
        this.ProjectPicturePath = 'http://206.189.34.171:7777/project/img/' + data.ProjectPicturePath;
        this.getBlob();
        this.clientService.getClientWithAuth(data.ProjectClient).subscribe(
          res => {
            this.clients = res;
            this.ProjectClient = data.ProjectClient;
          });
      },
      error => {
        this.Done = false;
        console.error(error);
      });
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  onSaveProjectDetail() {
    this.body = {
      ProjectId: this.id,
      ProjectName: this.ProjectName,
      ProjectDesc: this.ProjectDesc,
      ProjectLink: this.ProjectLink,
      ProjectPayment: this.ProjectPayment,
      ProjectStart: Math.round((this.ProjectStart).getTime() / 1000),
      ProjectEnd: Math.round((this.ProjectEnd).getTime() / 1000),
      ProjectHead: this.ProjectHead,
      ProjectClient: this.ProjectClient,
    };
    // tslint:disable-next-line: no-console
    console.log(this.body);
    this.projectService.putProjectWithAuth(this.body).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.bodypic = {
          type: this.selectedFile[0].name.split('.')[1],
          name: this.selectedFile[0].name.split('.')[0],
          ProjectId: this.id,
        };
        this.onUploadPic();
      },
      error => {
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  onCancel() {
    this.route.navigateByUrl('/page/project/info/detail/' + this.id);
  }

  onFileSelected(event) {
    this.selectedFile[0] = event.target.files[0];
    const reader = new FileReader();
    // this.selectedFile = event;
    reader.readAsDataURL(this.selectedFile[0]);
    reader.onload = (_event) => {
      this.ProjectPicturePath = reader.result;
    };
  }

  onUploadPic() {
    this.projectService.putImgWithAuth(this.bodypic, this.selectedFile[0]).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.route.navigateByUrl('/page/project/info/detail/' + this.id);
      },
      error => {
        // this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  getBlob() {
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[0] = this.blobToFile(xhr.response
          , this.ProjectPicturePath.split('/')[this.ProjectPicturePath.split('/').length - 1]);
      } else {
        this.selectedFile = [];
      }
    };
    xhr.open('GET', this.ProjectPicturePath, true);
    xhr.send();
  }

  blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;
    // A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    // Cast to a File() type
    return <File>theBlob;
  }

  startDatePick(event) {
    this.min = event;
  }

  selectclient(event) {
    this.ProjectClient = event;
  }
}
