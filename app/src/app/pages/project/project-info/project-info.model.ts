export interface ProjectData {
    projectname: string;
    start: Date;
    due: Date;
    requirement: string;
    price: number;
    payment: string;
}
