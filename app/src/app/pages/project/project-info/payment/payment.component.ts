import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { ProjectService } from '../../project.service';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  id: any;
  paymentForm: FormGroup;

  VatPrice: number;
  Total: number;
  checkTotal = true;
  SubTotal: number;
  ProjectPayment: string;
  tabledata = [];

  setting = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      No: {
        title: 'No.',
        type: 'string',
      },
      Description: {
        title: 'Description',
        type: 'string',
      },
      Quantity: {
        title: 'Quantity',
        type: 'number',
      },
      Unit: {
        title: 'Unit',
        type: 'String',
      },
      UnitPrice: {
        title: 'Unit Price/Baht',
        type: 'number',
      },
      Amount: {
        title: 'Amount',
        type: 'number',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource(this.tabledata);

  constructor(activeid: ActivatedRoute,
    private projectService: ProjectService,
    private route: Router) {
    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });
  }

  ngOnInit() {
    this.getDescription();
  }

  getDescription() {
    this.projectService.getCostDescription(this.id).subscribe(
      data => {
        this.tabledata = data.Detail;
        this.VatPrice = data.VAT;
        this.Total = data.Total;
        this.SubTotal = data.SubTotal;
        this.source.load(this.tabledata);
      });
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        this.ProjectPayment = data.ProjectPayment;
      });
  }

  onEdit() {
    this.route.navigateByUrl('/page/project/info/cost-edit/' + this.id);
  }
}
