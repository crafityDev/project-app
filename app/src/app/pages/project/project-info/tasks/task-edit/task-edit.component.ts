import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectService } from '../../../project.service';
import { NbDateService } from '@nebular/theme';

@Component({
  selector: 'ngx-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.scss'],
})
export class TaskEditComponent implements OnInit {
  editTask = false;
  duedatecheck = false;
  inSprint = false;
  taskEditForm: FormGroup;
  TaskName: string;
  TaskDesc: string;
  TaskDuedate: any;
  body: any;
  min: Date;
  @Input() Taskdata;
  @Input() Listdata;
  @Input() Sprintdata;
  @Output() percentEvent = new EventEmitter<string>();
  @Output() edittaskEvent = new EventEmitter
    <{ taskindex: any, listindex: any, taskname: any, taskdesc: any, duedate: any }>();
  @Output() deletetaskEvent = new EventEmitter<{ taskindex: any, listindex: any }>();
  constructor(private modalService: NgbModal,
    private projectService: ProjectService,
    protected dateService: NbDateService<Date>) {

    this.min = this.dateService.addDay(this.dateService.today(), 0);
  }

  ngOnInit() {
    // tslint:disable-next-line: no-console
    console.log(this.Taskdata);
    if (this.Sprintdata !== '') {
      this.inSprint = true;
    }
    if (this.Taskdata.DueDate !== 0) {
      this.duedatecheck = true;
      this.TaskDuedate = this.Taskdata.DueDate;
    } else {
      this.TaskDuedate = this.dateService.addDay(this.dateService.today(), 0).getTime() / 1000;
    }
    // tslint:disable-next-line: no-console
    console.log(this.Listdata);
  }

  onEditTask(taskid) {
    if (!this.duedatecheck) {
      this.body = {
        // ProjectId: this.id,
        SprintId: this.Taskdata.SprintId,
        // SprintDe: this.SprintDId,
        // ListId: listid,
        TaskId: taskid,
        TaskName: this.TaskName,
        TaskDesc: this.TaskDesc,
        SelectedType: 'Task',
      };
    } else {
      this.body = {
        // ProjectId: this.id,
        SprintId: this.Taskdata.SprintId,
        // SprintDe: this.SprintDId,
        // ListId: listid,
        TaskId: taskid,
        TaskName: this.TaskName,
        TaskDesc: this.TaskDesc,
        DueDate: Math.round((this.TaskDuedate).getTime() / 1000),
        SelectedType: 'Task',
      };
    }
    // tslint:disable-next-line: no-console
    console.log(this.body);
    // this.lists[listindex].ListTask[taskindex].TaskName = this.taskEditForm.get('TaskName').value;
    // this.lists[listindex].ListTask[taskindex].TaskDesc = this.taskEditForm.get('TaskDesc').value;
    this.projectService.editTaskdata(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.outputedittask();
        this.outputpercent(data['ProjectPercent']);
        this.modalService.dismissAll();
        this.editTask = !this.editTask;
        // this.getPage();
      }, error => {
        console.error(error);
      });
  }

  editTaskClick(taskName, taskDesc) {
    this.editTask = !this.editTask;
    this.TaskName = taskName;
    this.TaskDesc = taskDesc;
    this.TaskDuedate = new Date(this.TaskDuedate * 1000);
  }

  onDeleteTask(taskid) {
    // this.lists[listindex].ListTask.splice(taskindex, 1);
    this.projectService.deleteTask(taskid).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        this.outputdeletetask();
        this.outputpercent(res['ProjectPercent']);
        this.modalService.dismissAll();
        this.editTask = !this.editTask;
        // this.getPage();
      }, error => {
        console.error(error);
      });
  }

  DeleteTaskSprintid(taskid) {
    if (!this.duedatecheck) {
      this.body = {
        TaskId: taskid,
        TaskName: this.TaskName,
        TaskDesc: this.TaskDesc,
        SelectedType: 'Task',
      };
    } else {
      this.body = {
        TaskId: taskid,
        TaskName: this.TaskName,
        TaskDesc: this.TaskDesc,
        DueDate: Math.round((this.TaskDuedate).getTime() / 1000),
        SelectedType: 'Task',
      };
    }
    this.projectService.editTaskdata(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.outputdeletetask();
        this.outputpercent(data['ProjectPercent']);
        this.modalService.dismissAll();
        this.editTask = !this.editTask;
        // this.getPage();
      }, error => {
        console.error(error);
      });
  }

  cancelTaskEdit() {
    this.modalService.dismissAll();
    this.editTask = !this.editTask;
  }

  outputpercent(value) {
    this.percentEvent.emit(value);
  }

  outputedittask() {
    if (!this.duedatecheck) {
      this.edittaskEvent.emit({
        taskindex: this.Taskdata.indextemp,
        listindex: this.Listdata.index,
        taskname: this.TaskName,
        taskdesc: this.TaskDesc,
        duedate: 0,
      });
    } else {
      this.edittaskEvent.emit({
        taskindex: this.Taskdata.indextemp,
        listindex: this.Listdata.index,
        taskname: this.TaskName,
        taskdesc: this.TaskDesc,
        duedate: Math.round((this.TaskDuedate).getTime() / 1000),
      });
    }
  }

  outputdeletetask() {
    this.deletetaskEvent.emit({ taskindex: this.Taskdata.indextemp, listindex: this.Listdata.index });
  }

  startDatePick(event) {
    this.min = event;
  }

  toggleduedate() {
    this.duedatecheck = !this.duedatecheck;
  }
}
