import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Taskboard } from '../shared/Sprint.model';
// import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { NbDateService } from '@nebular/theme';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { TaskService } from '../shared/task.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectService } from '../../project.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { DragulaService } from 'ng2-dragula';
// import { Subscription, interval } from 'rxjs';

@Component({
  selector: 'ngx-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
})
export class TasksComponent implements OnInit, OnDestroy {
  @Output() percentEvent = new EventEmitter<string>();

  id: string;
  SprintId: string;
  SprintDId: string;
  SprintName = '';
  body: any;
  body1: any;
  temp: any[] = [];
  sprintidtemp: string;
  editTask = false;

  // allTasks: Task[] = [];
  // sprintTasks: Task[] = [];

  controls: FormArray;
  listForm: FormGroup;
  sprintForm: FormGroup;
  taskAddForm: FormGroup;
  taskEditForm: FormGroup;

  subscription: Subscription;

  taskboard: Taskboard[] = [];
  lists = [];
  lists1 = [];
  temptask: any;

  constructor(private modalService: NgbModal,
    private taskService: TaskService,
    private projectService: ProjectService,
    protected dateService: NbDateService<Date>,
    private dragula: DragulaService,
    activeid: ActivatedRoute) {
    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });
    // dragula.createGroup("task-group", {
    //   revertOnSpill:true
    // });

    this.sprintForm = new FormGroup({
      SprintName: new FormControl('', Validators.required),
      SprintStart: new FormControl('', Validators.required),
      SprintEnd: new FormControl('', Validators.required),
    });

    this.listForm = new FormGroup({
      ListName: new FormControl('', Validators.required),
    });

    this.taskEditForm = new FormGroup({
      TaskName: new FormControl('', Validators.required),
      TaskPoint: new FormControl(''),
      TaskDesc: new FormControl(''),
    });

    this.taskAddForm = new FormGroup({
      TaskName1: new FormControl('', Validators.required),
      TaskPoint1: new FormControl(''),
      TaskDesc1: new FormControl(''),
    });
  }

  ngOnInit() {
    this.getPage();
    // this.dragula.drag('task-group').subscribe(value => {
    // });
    this.dragula.drop('task-group').subscribe(value => {
      // console.log(value.el.id);
      // console.log(value.source.id)
      // console.log(value.target.id);
      this.lists = [];
      this.lists1 = [];
      this.temptask = this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
        .Task[this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
          .Task.findIndex(y => y.TaskId === value.el.id)];

      this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
        .Task.splice(this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
          .Task.findIndex(y => y.TaskId === value.el.id), 1);

      this.taskboard[this.taskboard.findIndex(x => x.ListId === value.target.id)].Task.push(this.temptask);

      for (let i = 0; i < this.taskboard[
        this.taskboard.findIndex(x => x.ListId === value.target.id)].Task.length; i++) {
        this.lists.push(this.taskboard[
          this.taskboard.findIndex(x => x.ListId === value.target.id)].Task[i].TaskId);
        this.sprintidtemp = this.taskboard[
          this.taskboard.findIndex(x => x.ListId === value.target.id)].Task[i]['SprintId'];
      }
      for (let i = 0; i < this.taskboard[
        this.taskboard.findIndex(x => x.ListId === value.source.id)].Task.length; i++) {
        this.lists1.push(this.taskboard[
          this.taskboard.findIndex(x => x.ListId === value.source.id)].Task[i].TaskId);
      }
      this.body = {
        SprintTemp: this.sprintidtemp,
        ListName: this.taskboard[this.taskboard.findIndex(x => x.ListId === value.target.id)].ListName,
        ListId: this.taskboard[this.taskboard.findIndex(x => x.ListId === value.target.id)].ListId,
        ListTask: this.lists,
      };
      this.body1 = {
        SprintTemp: this.sprintidtemp,
        ListName: this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)].ListName,
        ListId: this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)].ListId,
        ListTask: this.lists1,
      };
      this.temp = [];
      this.temp.push(this.body);
      this.temp.push(this.body1);
      this.projectService.editList(this.temp).subscribe(
        data => {
          // tslint:disable-next-line: no-console
          console.log(data);
          this.outputpercent(data['ProjectPercent']);
          // this.getPage();
        },
        error => {
          console.error(error);
        });
    });
  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }

  getControl(index: number, field: string): FormControl {
    return this.controls.at(index).get(field) as FormControl;
  }

  updateField(index: number, field: string) {
    const control = this.getControl(index, field);

    if (control.valid) {
      this.lists = this.lists.map((e, i) => {
        if (index === i) {
          return {
            ...e,
            [field]: control.value,
          };
        }
        return e;
      });
    }
    this.taskService.updateListName(control.value, index);
    // this.UpdateDrop();
  }

  get listIds(): string[] {
    return this.lists.map(list => list.ListId);
  }

  onAddTask(listid) {
    this.body = {
      ProjectId: this.id,
      // SprintId: this.SprintId,
      SprintDe: this.SprintDId,
      ListDe: listid,
      TaskName: this.taskAddForm.get('TaskName1').value,
    };
    this.body1 = {
      CompanyId: localStorage.getItem('companyId'),
      ProjectId: this.id,
      // SprintId: this.SprintId,
      SprintDe: this.SprintDId,
      // ListDe: listid,
      TaskName: this.taskAddForm.get('TaskName1').value,
      DueDate: 0,
    };
    if (this.body.TaskName === '') {
      this.taskboard[this.taskboard.findIndex(x => x.ListId === listid)]['add'] = false;
    } if (this.body.TaskName === null) {
      this.taskboard[this.taskboard.findIndex(x => x.ListId === listid)]['add'] = false;
    } if (this.body.TaskName !== '' && this.body.TaskName !== null) {
      this.projectService.createTask(this.body).subscribe(
        data => {
          // tslint:disable-next-line: no-console
          console.log(data);
          this.outputpercent(data['ProjectPercent']);
          this.body.TaskId = data.TaskId;
          this.taskboard[this.taskboard.findIndex(x => x.ListId === listid)]['add'] = false;
          this.taskboard[this.taskboard.findIndex(x => x.ListId === listid)].Task.push(this.body);
          // this.getPage();
        }, error => {
          console.error(error);
        });
    }
    this.taskAddForm.reset();
  }

  onEditTaskName(taskid, listindex, taskindex) {
    this.body = {
      TaskId: taskid,
      TaskName: this.taskEditForm.get('TaskName').value,
      SelectedType: 'Task',
    };
    this.taskboard[listindex].Task[taskindex]['TaskName'] = this.taskEditForm.get('TaskName').value;
    this.taskboard[listindex].Task[taskindex]['edit'] = false;
    this.projectService.editMindTask(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        // this.outputpercent(data['ProjectPercent']);
        // this.getPage();
      }, error => {
        console.error(error);
      });
    this.taskEditForm.reset();
  }

  onDeleteList(listid, index) {
    this.lists.splice(index, 1);
    this.projectService.deleteList(this.SprintId, listid).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        this.getPage();
      }, error => {
        console.error(error);
      });
  }

  spliceTask(value) {
    this.taskboard[value.listindex].Task.splice(value.taskindex, 1);
    // tslint:disable-next-line: no-console
    console.log(value);
  }

  // this.ClientContact.splice(this.ClientContact.indexOf(event.data), 1);
  // this.taskboard[listindex].Task.splice(taskindex, 1);

  editTaskValue(value) {
    this.taskboard[value.listindex].Task[value.taskindex]['TaskName'] = value.taskname;
    this.taskboard[value.listindex].Task[value.taskindex]['TaskDesc'] = value.taskdesc;
    this.taskboard[value.listindex].Task[value.taskindex]['DueDate'] = value.duedate;
    // tslint:disable-next-line: no-console
    console.log(value);
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  openTask(content, listindex, taskindex) {
    this.taskboard[listindex].Task[taskindex]['indextemp'] = taskindex;
    this.modalService.open(content, { centered: true });
  }

  getPage() {
    this.taskboard = [];
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        this.SprintDId = data.ProjectSprintDe;
        this.projectService.getSprint(data.ProjectSprintDe).subscribe(
          res => {
            // tslint:disable-next-line: no-console
            console.log(res);
            this.SprintId = res.SprintId;
            for (let i = 0; i < res.SprintList.length; i++) {
              this.projectService.getList(res.SprintList[i]).subscribe(
                response => {
                  response.add = false;
                  response.index = i;
                  for (let j = 0; j < response.Task.length; j++) {
                    response.Task[j].edit = false;
                  }
                  this.taskboard.push(response);
                  this.taskboard.sort((a, b) => a.index - b.index);
                  // tslint:disable-next-line: no-console
                  console.log(this.taskboard);
                });
            }
          });
      }, error => {
        console.error(error);
      });
  }

  UpdateDrop() {
    this.body = {
    };
    this.projectService.editList(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        // this.outputpercent(data['ProjectPercent']);
        // this.getPage();
      },
      error => {
        console.error(error);
      });
  }

  addTaskClick(listindex) {
    this.taskboard[listindex]['add'] = true;
  }

  editTaskNameClick(listindex, taskindex, taskname) {
    this.taskboard[listindex].Task[taskindex]['edit'] = true;
    this.taskEditForm = new FormGroup({
      TaskName: new FormControl(taskname, Validators.required),
    });
  }

  cancelTaskAdd() {
    this.modalService.dismissAll();
    this.taskAddForm.reset();
  }

  cancelList() {
    this.modalService.dismissAll();
    this.listForm.reset();
  }

  outputpercent(value) {
    this.percentEvent.emit(value);
  }
}
