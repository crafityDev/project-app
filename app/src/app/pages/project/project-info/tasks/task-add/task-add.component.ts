import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.scss'],
})
export class TaskAddComponent implements OnInit {

  taskAddForm: FormGroup;

  constructor(private modalService: NgbModal) {

    const TaskName = '';
    const TaskPoint = '';
    const TaskDesc = '';

    this.taskAddForm = new FormGroup({
      TaskName: new FormControl(TaskName, Validators.required),
      TaskPoint: new FormControl(TaskPoint),
      TaskDesc: new FormControl(TaskDesc),
    });
  }

  ngOnInit() {
  }

  save() {
    this.modalService.dismissAll();
  }

  cancel() {
    this.modalService.dismissAll();
  }
}
