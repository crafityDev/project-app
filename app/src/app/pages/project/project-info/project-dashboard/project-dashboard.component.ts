import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { TaskService } from '../shared/task.service';
import { FormControl, FormArray, FormGroup, Validators } from '@angular/forms';
import { List, Task, Sprint } from '../shared/Sprint.model';
import { moveItemInArray, CdkDragDrop, transferArrayItem } from '@angular/cdk/drag-drop';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Params } from '@angular/router';
import { ProjectService } from '../../project.service';
import { Subscription, interval } from 'rxjs';

@Component({
  selector: 'ngx-project-dashboard',
  templateUrl: './project-dashboard.component.html',
  styleUrls: ['./project-dashboard.component.scss'],
})
export class ProjectDashboardComponent implements OnInit, OnDestroy {

  @Output() percentEvent = new EventEmitter<string>();
  id: string;
  body: any;
  editTask = false;

  lists: List[] = [];
  tasks: Task[] = [];
  sprintTasks: Task[] = [];
  sprint: Sprint;
  temp: any;
  SprintName: string;
  SprintStart: any;
  SprintEnd: any;
  SprintId: any;
  getsprint = [];
  subscription: Subscription;
  controls: FormArray;
  sprintForm: FormGroup;
  listForm: FormGroup;
  taskAddForm: FormGroup;
  taskEditForm: FormGroup;

  hasSprint = false;
  datelegth: any;
  taskperdate: number;

  constructor(private modalService: NgbModal,
    private taskService: TaskService,
    private projectService: ProjectService,
    activeid: ActivatedRoute) {
    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });

    this.sprintForm = new FormGroup({
      SprintName: new FormControl('', Validators.required),
      SprintStart: new FormControl('', Validators.required),
      SprintEnd: new FormControl('', Validators.required),
    });

    this.listForm = new FormGroup({
      ListName: new FormControl('', Validators.required),
    });

    this.taskEditForm = new FormGroup({
      TaskName: new FormControl('', Validators.required),
      TaskPoint: new FormControl(''),
      TaskDesc: new FormControl(''),
    });

    this.taskAddForm = new FormGroup({
      TaskName1: new FormControl('', Validators.required),
      TaskPoint1: new FormControl(''),
      TaskDesc1: new FormControl(''),
    });
  }

  ngOnInit(): void {
    const put = interval(50000);
    this.subscription = put.subscribe(val => this.UpdateDrop());
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        for (let i = 0; i < data.ProjectSprints.length; i++) {
          this.projectService.getSprint(data.ProjectSprints[i]).subscribe(
            res => {
              if (res.MainSprint === 'Main') {
                // tslint:disable-next-line: no-console
                console.log(res);
                this.lists = res.SprintList;
                this.SprintName = res.SprintName;
                this.SprintStart = res.SprintStart;
                this.SprintEnd = res.SprintEnd;
                this.SprintId = res.SprintId;

                this.sprintForm = new FormGroup({
                  SprintName: new FormControl(res.SprintName, Validators.required),
                  SprintStart: new FormControl(new Date(res.SprintStart * 1000), Validators.required),
                  SprintEnd: new FormControl(new Date(res.SprintEnd * 1000), Validators.required),
                });

                this.datelegth = Math.round(
                  (res.SprintEnd * 1000 - res.SprintStart * 1000) / (1000 * 60 * 60 * 24));
                this.projectService.getTasklength(res.SprintId).subscribe(
                  response => {
                    // tslint:disable-next-line: no-console
                    console.log(response);
                    this.taskperdate = Math.ceil(response.Task / this.datelegth);
                  });
              }

              this.getsprint.push(res);
              const toGroups = this.lists.map(list => {
                // const a = new FormControl(list.isAdmin);
                return new FormGroup({
                  ListName: new FormControl(list.ListName, Validators.required),
                });
              });
              this.controls = new FormArray(toGroups);
            });
        }
      }, error => {
        console.error(error);
      });
    this.taskService.editTask.subscribe(x => {
      this.editTask = x;
    });
    this.taskService.hasSprint.subscribe(isTrue => {
      this.hasSprint = isTrue;
    });

    // this.taskService.getAllList().subscribe((temp) => {
    //   this.lists = temp;
    // });
    // this.taskService.allTasks.subscribe(temp => {
    //   this.tasks = temp;
    // });

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getControl(index: number, field: string): FormControl {
    return this.controls.at(index).get(field) as FormControl;
  }

  updateField(index: number, field: string) {
    const control = this.getControl(index, field);

    if (control.valid) {
      this.lists = this.lists.map((e, i) => {
        if (index === i) {
          return {
            ...e,
            [field]: control.value,
          };

        }
        return e;
      });
    }

    this.taskService.updateListName(control.value, index);
    this.UpdateDrop();
  }

  get listIds(): string[] {
    return this.lists.map(list => list.ListId);
  }

  onTaskDrop(event: CdkDragDrop<Task[]>) {
    // In case the destination container is different from the previous container, we
    // need to transfer the given task to the target data array. This happens if
    // a task has been dropped on a different list.
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    this.UpdateDrop();
  }

  onListDrop(event: CdkDragDrop<List[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    const toGroups = this.lists.map(list => {
      // const a = new FormControl(list.isAdmin);
      return new FormGroup({
        ListName: new FormControl(list.ListName, Validators.required),
      });
    });
    this.controls = new FormArray(toGroups);
    this.UpdateDrop();
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  onAddList() {
    this.body = {
      SprintId: this.SprintId,
      ListName: this.listForm.get('ListName').value,
    };
    this.projectService.createList(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.getPage();
      }, error => {
        console.error(error);
      });
    this.modalService.dismissAll();
    this.listForm.reset();
  }

  onAddTask(listid) {
    if (this.taskAddForm.value.TaskPoint === '' || this.taskAddForm.value.TaskPoint < 1) {
      this.body = {
        SprintId: this.SprintId,
        ListId: listid,
        TaskName: this.taskAddForm.get('TaskName1').value,
        TaskPoint: 0,
        TaskDesc: this.taskAddForm.get('TaskDesc1').value,
      };
    } else {
      this.body = {
        SprintId: this.SprintId,
        ListId: listid,
        TaskName: this.taskAddForm.get('TaskName1').value,
        TaskPoint: this.taskAddForm.get('TaskPoint1').value,
        TaskDesc: this.taskAddForm.get('TaskDesc1').value,
      };
    }
    // tslint:disable-next-line: no-console
    console.log(this.body);
    this.projectService.createTask(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log('addtask' + data);
        this.outputpercent(data['ProjectPercent']);
        this.getPage();
      }, error => {
        console.error(error);
      });
    this.modalService.dismissAll();
    this.taskAddForm.reset();
  }

  onEditTask(listid, taskid, listindex, taskindex) {
    this.editTask = !this.editTask;
    if (this.taskEditForm.value.TaskPoint === '' || this.taskEditForm.value.TaskPoint < 1) {
      this.body = {
        SprintId: this.SprintId,
        ListId: listid,
        TaskId: taskid,
        TaskName: this.taskEditForm.get('TaskName').value,
        TaskPoint: 0,
        TaskDesc: this.taskEditForm.get('TaskDesc').value,
      };
      this.lists[listindex].ListTask[taskindex].TaskName = this.taskEditForm.get('TaskName').value;
      this.lists[listindex].ListTask[taskindex].TaskPoint = 0;
      this.lists[listindex].ListTask[taskindex].TaskDesc = this.taskEditForm.get('TaskDesc').value;
    } else {
      this.body = {
        SprintId: this.SprintId,
        ListId: listid,
        TaskId: taskid,
        TaskName: this.taskEditForm.get('TaskName').value,
        TaskPoint: this.taskEditForm.get('TaskPoint').value,
        TaskDesc: this.taskEditForm.get('TaskDesc').value,
      };
      this.lists[listindex].ListTask[taskindex].TaskName = this.taskEditForm.get('TaskName').value;
      this.lists[listindex].ListTask[taskindex].TaskPoint = this.taskEditForm.get('TaskPoint').value;
      this.lists[listindex].ListTask[taskindex].TaskDesc = this.taskEditForm.get('TaskDesc').value;
    }
    this.projectService.editTaskdata(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.outputpercent(data['ProjectPercent']);
        this.getPage();
      }, error => {
        console.error(error);
      });
    this.modalService.dismissAll();
    this.taskEditForm.reset();
  }

  onDeleteTask(taskid) {
    // this.lists[listindex].ListTask.splice(taskindex, 1);
    this.projectService.deleteTask(taskid).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        this.outputpercent(res['ProjectPercent']);
        this.getPage();
      }, error => {
        console.error(error);
      });
  }

  onDeleteList(listid, index) {
    this.lists.splice(index, 1);
    this.projectService.deleteList(this.SprintId, listid).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        this.getPage();
      }, error => {
        console.error(error);
      });
  }

  updateMainSprint(sprintId) {
    this.body = {
      SprintId: sprintId,
      ProjectId: this.id,
      MainSprint: 'Main',
    };
    this.projectService.updateMain(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.getPage();
        this.modalService.dismissAll();
      },
      error => {
        console.error(error);
      });
  }

  editSprint(sprintId) {
    this.body = {
      SprintId: sprintId,
      ProjectId: this.id,
      SprintList: this.lists,
      SprintName: this.sprintForm.get('SprintName').value,
      SprintStart: Math.round((this.sprintForm.get('SprintStart').value).getTime() / 1000),
      SprintEnd: Math.round((this.sprintForm.get('SprintEnd').value).getTime() / 1000),
    };
    this.projectService.editSprint(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.modalService.dismissAll();
        this.getPage();
      },
      error => {
        console.error(error);
      });
  }

  deleteSprint(sprintId, content) {
    for (let i = 0; i < this.getsprint.length; i++) {
      if (this.getsprint[i].SprintId === sprintId) {
        this.getsprint.splice(i, 1);
      }
    }
    this.projectService.deleteSprint(sprintId).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.modalService.open(content, { centered: true });
      },
      error => {
        console.error(error);
      });
  }

  getPage() {
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        for (let i = 0; i < data.ProjectSprints.length; i++) {
          this.projectService.getSprint(data.ProjectSprints[i]).subscribe(
            res => {
              if (res.MainSprint === 'Main') {
                // tslint:disable-next-line: no-console
                console.log(res);
                this.lists = res.SprintList;
                this.SprintId = res.SprintId;
                this.SprintStart = res.SprintStart;
                this.SprintEnd = res.SprintEnd;
                this.SprintId = res.SprintId;

                this.sprintForm = new FormGroup({
                  SprintName: new FormControl(res.SprintName, Validators.required),
                  SprintStart: new FormControl(new Date(res.SprintStart * 1000), Validators.required),
                  SprintEnd: new FormControl(new Date(res.SprintEnd * 1000), Validators.required),
                });
              }
            });
        }
      }, error => {
        console.error(error);
      });
  }

  UpdateDrop() {
    this.body = {
      SprintId: this.SprintId,
      ProjectId: this.id,
      SprintList: this.lists,
      SprintName: this.SprintName,
      SprintStart: this.SprintStart,
      SprintEnd: this.SprintEnd,
    };
    this.projectService.editSprint(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log('update' + data);
        this.outputpercent(data['ProjectPercent']);
        this.getPage();
      },
      error => {
        console.error(error);
      });
  }

  editTaskClick(TaskPoint, TaskName, TaskDesc) {
    this.editTask = !this.editTask;
    this.taskEditForm = new FormGroup({
      TaskName: new FormControl(TaskName, Validators.required),
      TaskPoint: new FormControl(TaskPoint),
      TaskDesc: new FormControl(TaskDesc),
    });
  }

  cancelTaskAdd() {
    this.modalService.dismissAll();
    this.taskAddForm.reset();
  }

  cancelTaskEdit() {
    this.modalService.dismissAll();
    this.editTask = !this.editTask;
  }

  cancelList() {
    this.modalService.dismissAll();
    this.listForm.reset();
  }

  outputpercent(value) {
    this.percentEvent.emit(value);
  }

}
