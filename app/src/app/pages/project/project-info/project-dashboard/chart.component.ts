import { Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { ProjectService } from '../../project.service';
import { ActivatedRoute, Params } from '@angular/router';
// import { TaskService } from '../shared/task.service';

@Component({
  selector: 'ngx-chart-dash',
  template: `
  <chart [type]="typeChart" [data]="data" [options]="options" *ngIf="setdataDone"></chart>
`,
})
export class ChartComponent implements OnDestroy {

  data: {};
  options: any;
  typeChart: any;
  themeSubscription: any;
  temp = [];
  sprint: any;
  id: string;
  datelegth: any;
  setdataDone = false;

  constructor(private theme: NbThemeService,
    private projectService: ProjectService,
    activeid: ActivatedRoute) {
    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });

    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;
      // const temp: string[] = ['A', 'B', 'C', 'D', 'E', 'F'];
      // this.taskService.getSprintDate();

      this.projectService.getProjectWithAuth(this.id).subscribe(
        response => {
          for (let i = 0; i < response.ProjectSprints.length; i++) {
            this.projectService.getSprint(response.ProjectSprints[i]).subscribe(
              data => {
                if (data.MainSprint === 'Main') {
                  // tslint:disable-next-line: no-console
                  console.log(data);
                  this.sprint = data;
                  this.datelegth = Math.round(
                    (data.SprintEnd - data.SprintStart) / (60 * 60 * 24));
                  this.projectService.getTasklength(data.SprintId).subscribe(
                    res => {
                      // tslint:disable-next-line: no-console
                      console.log(res);
                      this.data = {
                        labels: this.temp,
                        datasets: [{
                          label: 'Ideal Burn',
                          data: [],
                          borderColor: colors.info,
                          backgroundColor: colors.info,
                          fill: false,
                          pointRadius: 8,
                          pointHoverRadius: 10,
                        }, {
                          label: 'Actual Burn',
                          data: [],
                          borderColor: colors.dangerLight,
                          backgroundColor: colors.dangerLight,
                          fill: false,
                          pointRadius: 8,
                          pointHoverRadius: 10,
                        }],
                      };
                      for (let j = 0; j < this.datelegth + 1; j++) {
                        if (j === this.datelegth) {
                          this.temp.push(j);
                          this.data['datasets'][0]['data'].push(0);
                        } else {
                          this.temp.push(j);
                          this.data['datasets'][0]['data'].push(res.Task / (j + 1));
                        }
                      }
                      this.projectService.getTaskHistory(data.SprintId).subscribe(
                        res1 => {
                          // tslint:disable-next-line: no-console
                          console.log(res1);
                          for (let k = 0; k < res1.length; k++) {
                            this.data['datasets'][1]['data'].push(res1[k].TaskNotDone);
                            // this.data['datasets'][2]['data'].push(this.tasklegth[k].TaskNotDone);
                          }
                          this.setdataDone = true;
                        });
                    });
                }
              });
          }
        }, error => {
          console.error(error);
        });
      this.typeChart = 'line';
      // 'line','bar','radar','pie','doughnut','polarArea','bubble','scatter'
      this.options = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          position: 'bottom',
          labels: {
            fontColor: chartjs.textColor,
          },
        },
        hover: {
          mode: 'index',
        },
        scales: {
          xAxes: [
            {
              display: true,
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Tasks',
              },
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
        },
      };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

  // private random() {
  //   return Math.round(Math.random() * 100);
  // }
}
