import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Router } from '@angular/router';
import { ProjectService } from '../../project.service';
import { Task, Taskboard } from '../shared/Sprint.model';
import { NbDateService } from '@nebular/theme';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'ngx-sprint-add',
  templateUrl: './sprint-add.component.html',
  styleUrls: ['./sprint-add.component.scss'],
})
export class SprintAddComponent implements OnInit {
  id: string;
  body: any;
  body1: any;
  SprintDId: string;
  SprintId: string;
  sprintForm: FormGroup;
  allTasks: Task[] = [];
  // sprintTasks: Task[] = [];

  min: any;
  max: Date;

  taskboard: Taskboard[] = [];
  sprintTasks: Taskboard[] = [{
    ListName: 'Backlog for New Sprint',
    ListId: 'tempId',
    SelectedType: 'Task',
    index: 1,
    Task: [],
  }];
  lists = [];
  lists1 = [];
  temptask: any;

  constructor(private router: Router,
    private projectService: ProjectService,
    private dragula: DragulaService,
    protected dateService: NbDateService<Date>) {
    this.id = localStorage.getItem('project');

    const SprintName = '';
    const SprintStart = '';
    const SprintEnd = '';

    this.sprintForm = new FormGroup({
      SprintName: new FormControl(SprintName, Validators.required),
      SprintStart: new FormControl(SprintStart, Validators.required),
      SprintEnd: new FormControl(SprintEnd, Validators.required),
    });
  }

  ngOnInit() {
    // this.projectService.getProjectWithAuth(this.id).subscribe(
    //   data => {
    //     for (let i = 0; i < data.ProjectSprints.length; i++) {
    //       this.projectService.getSprint(data.ProjectSprints[i]).subscribe(
    //         res => {
    //           if (res.MainSprint === 'Default') {
    //             // tslint:disable-next-line: no-console
    //             console.log(res);
    //             this.SprintId = res.SprintId;
    //           }
    //         });
    //     }
    //     this.projectService.getAlltask(this.id).subscribe(
    //       response => {
    //         this.allTasks = response;
    //       });
    //   }, error => {
    //     console.error(error);
    //   });
    this.getPage();

    this.dragula.drop('task-group1').subscribe(value => {
      // // TaskId
      // console.log(value.el.id);
      // // ListId
      // console.log(value.source.id);
      // // newList
      // console.log(value.target.id);
      // this.sprintTasks[0].Task.push(this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
      //   .Task[this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
      //     .Task.findIndex(y => y.TaskId === value.el.id)]);
      // this.lists = this.sprintTasks[0].Task;
      this.lists.push(this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
        .Task[this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
          .Task.findIndex(y => y.TaskId === value.el.id)]);

      // tslint:disable-next-line: no-console
      console.log(this.lists);
    });
  }

  getPage() {
    this.taskboard = [];
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        this.max = new Date(data.ProjectEnd * 1000);
        this.SprintDId = data.ProjectSprintDe;
        this.projectService.getSprint(data.ProjectSprintDe).subscribe(
          res => {
            // tslint:disable-next-line: no-console
            console.log(res);
            for (let i = 0; i < res.SprintList.length; i++) {
              this.projectService.getList(res.SprintList[i]).subscribe(
                response => {
                  if (response.ListName === 'backlog') {
                    this.taskboard.push(response);
                    // tslint:disable-next-line: no-console
                    console.log(this.taskboard);
                    for (let j = this.taskboard[i].Task.length - 1; j >= 0; j--) {
                      if (response.Task[j].SprintId !== '') {
                        this.taskboard[i].Task.splice(j, 1);
                      }
                    }
                  }
                });
            }
          });
      }, error => {
        console.error(error);
      });
  }

  startDatePick(event) {
    this.min = event;
  }

  onTaskDrop1(event: CdkDragDrop<Task[]>) {
    // In case the destination container is different from the previous container, we
    // need to transfer the given task to the target data array. This happens if
    // a task has been dropped on a different list.
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  onAddSprint() {
    this.body = {
      ProjectId: this.id,
      SprintName: this.sprintForm.get('SprintName').value,
      SprintStart: Math.round((this.sprintForm.get('SprintStart').value).getTime() / 1000),
      SprintEnd: Math.round((this.sprintForm.get('SprintEnd').value).getTime() / 1000),
      // ListTask: this.lists,
      CompanyId: localStorage.getItem('companyId'),
    };
    this.projectService.createSprint(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        for (let i = 0; i < this.lists.length; i++) {
          this.body = {
            SprintId: data.SprintId,
            TaskId: this.lists[i].TaskId,
            TaskName: this.lists[i].TaskName,
            TaskDesc: this.lists[i].TaskDesc,
            DueDate: Math.round(this.lists[i].TaskDuedate / 1000),
            SelectedType: 'Task',
          };
          // tslint:disable-next-line: no-console
          console.log(this.body);
          this.projectService.editTaskdata(this.body).subscribe(
            res => {
              // tslint:disable-next-line: no-console
              console.log(res);
            }, error => {
              console.error(error);
            });
        }
        this.body1 = {
          SprintId: data.SprintId,
          ProjectId: this.id,
          MainSprint: 'Main',
        };
        this.projectService.updateMain(this.body1).subscribe(
          res => {
            // tslint:disable-next-line: no-console
            console.log(res);
            this.router.navigateByUrl('/page/project/info/sprint/' + this.id);
            this.sprintForm.reset();
          },
          error => {
            console.error(error);
          });
      }, error => {
        console.error(error);
      });
  }

  cancelSprint() {
    this.sprintForm.reset();
    this.sprintTasks = [];
  }

}
