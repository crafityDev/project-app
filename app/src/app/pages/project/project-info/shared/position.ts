import { Observable } from 'rxjs';

export interface Position {
    position: string;
    startDate: Date;
    endDate: Date;
    salary: number;
}

export abstract class PositionData {
    abstract getPositionData(): Observable<PositionData>;
}
