import { NgModule } from '@angular/core';
import { ThemeModule } from '../../../../../@theme/theme.module';
import { EditableComponent } from './editable.component';
import { EditModeDirective } from './edit-mode.directive';
import { EditableOnEnterDirective } from './editable-on-enter.directive';
import { FocusableDirective } from './focusable.directive';
import { ViewModeDirective } from './view-mode.directive';

@NgModule({
  declarations: [
    EditableComponent,
    EditModeDirective,
    EditableOnEnterDirective,
    FocusableDirective,
    ViewModeDirective,
  ],
  imports: [
    ThemeModule,
  ],
  exports: [
    EditableComponent,
    EditModeDirective,
    EditableOnEnterDirective,
    FocusableDirective,
    ViewModeDirective,
  ],
})
export class EditableModule { }
