import { Injectable } from '@angular/core';
import { of as observableOf, Observable, Subject } from 'rxjs';
import { List, Task, Sprint } from './Sprint.model';

@Injectable()
export class TaskService {

    private allList = [{
        'ListName': 'Todo',
        'ListId': 'todo',
        'ListTask': [{
            'TaskId': 'first-task',
            'TaskName': '1 Task',
            'TaskDesc': 'This is my first task',
            'TaskPoint': 8,
            'TaskStatus': false,
        }],
    },
    {
        'ListName': 'In Progress',
        'ListId': 'inprogress',
        'ListTask': [{
            'TaskId': 'second-task',
            'TaskName': '2 Task',
            'TaskDesc': 'This is my first task',
            'TaskPoint': 2,
            'TaskStatus': false,
        }],
    },
    {
        'ListName': 'D-Done',
        'ListId': 'ddone',
        'ListTask': [{
            'TaskId': 'third-task',
            'TaskName': '3 Task',
            'TaskDesc': 'This is my first task',
            'TaskPoint': 1,
            'TaskStatus': true,
        }],
    },
    ];

    private allTask: Task[] = [];

    private sprint: Sprint;

    allTasks = new Subject<Task[]>();

    hasSprint = new Subject<boolean>();

    editTask = new Subject<boolean>();

    getAllList(): Observable<List[]> {
        return observableOf(this.allList);
    }

    getAllTask(): Observable<Task[]> {
        this.allTask = [];
        for (let i = 0; i < this.allList.length; i++) {
            for (let j = 0; j < this.allList[i].ListTask.length; j++) {
                this.allTask.push(this.allList[i].ListTask[j]);
            }
        }
        // this.allTasks.next(this.allTask);
        return observableOf(this.allTask);
    }

    getAllSprint(): Observable<Sprint> {
        return observableOf(this.sprint);
    }

    getNumberOfTask(): Observable<number> {
        return observableOf(this.allTask.length);
    }

    getTask(listId, taskId): Task {
        let temp: any;
        for (let i = 0; i < this.allList.length; i++) {
            if (this.allList[i].ListId === listId) {
                for (let j = 0; j < this.allList[i].ListTask.length; j++) {
                    if (this.allList[i].ListTask[j].TaskId === taskId) {
                        temp = {
                            TaskName: this.allList[i].ListTask[j].TaskName,
                            TaskId: this.allList[i].ListTask[j].TaskId,
                            TaskPoint: this.allList[i].ListTask[j].TaskPoint,
                            TaskDesc: this.allList[i].ListTask[j].TaskDesc,
                            TaskStatus: this.allList[i].ListTask[j].TaskStatus,
                        };
                        break;
                    }
                }
            }
        }
        return temp;
    }

    getSprintDate() {
        return this.sprint.date;
    }

    setSprint(temp: Sprint) {
        this.sprint = temp;
    }

    updateListName(tempName, index) {
        this.allList = this.allList.map((list, i) => {
            if (index === i) {
                list.ListName = tempName;
            }
            return list;
        });

    }

    addTask(tempTask, listId) {
        for (let i = 0; i < this.allList.length; i++) {
            if (this.allList[i].ListId === listId) {
                this.allList[i].ListTask.push(tempTask);
                this.getAllTask();
                break;
            }
        }
    }

}
