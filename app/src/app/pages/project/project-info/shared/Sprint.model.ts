export interface List {
  ListName: string;
  ListId: string;
  ListTask: Task[];
}

export interface Task {
  TaskName: string;
  TaskDesc: string;
  TaskId: string;
  TaskPoint: number;
  TaskStatus: boolean;
}

export interface Sprint {
  title: string;
  id: string;
  startDate: Date;
  endDate: Date;
  duration: number;
  date: string[];
  tasks: Task[];
}

export interface Tasks {
  // title: string;
  // description: string;
  // class?: string;
  CompanyId: string;
  ProjectId: string;
  SelectedType: string;
  SprintName: string;
  DueDate: Date;
  TaskName: string;
  TaskDesc: string;
  TaskId: string;
  TaskPoint: number;
  TaskStatus: boolean;
}

export interface Taskboard {
  // title: string;
  // tasks: Tasks[];
  ListName: string;
  ListId: string;
  SelectedType: string;
  index: number;
  Task: Tasks[];
}
