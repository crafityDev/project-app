import { Observable } from 'rxjs';
import { Position } from './position';

export interface Staff {
    name: string;
    email: string;
    tel: string;
    address: string;
    startDate: Date;
    resignDate: Date;
    position: Position;
}

export abstract class StaffData {
    abstract getStaffData(): Observable<Staff>;
}
