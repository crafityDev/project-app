import { Observable } from 'rxjs';
import { List } from './Sprint.model';

export interface ProjectData {
    title: string;
    lists: List[];
    pic: string;
    describe: string;
    duration: number;
    startDate: Date;
    endDate: Date;
    pinned: boolean;
}

export abstract class ProjectData {
    abstract getProjectData(): Observable<ProjectData>;
}
