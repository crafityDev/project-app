import { Component, OnInit, OnDestroy } from '@angular/core';
import { MindMapMain } from 'angular-mindmap';
import { ProjectService } from '../../project.service';
import { Subscription, interval } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';

// declare var jm: any;
// import '../../../../../assets/js/jsmind.draggable.js'

const HIERARCHY_RULES = {
  ROOT: {
    name: 'Root',
    backgroundColor: '#7EC6E1',
    getChildren: () => [
      // HIERARCHY_RULES.Sprint,
      HIERARCHY_RULES.List,
      HIERARCHY_RULES.Task,
    ],
  },
  Sprint: {
    name: 'Sprint',
    color: '#fff',
    backgroundColor: '#616161',
    getChildren: () => [
      HIERARCHY_RULES.List,
      HIERARCHY_RULES.Task,
    ],
  },
  List: {
    name: 'List',
    color: '#fff',
    backgroundColor: '#989898',
    getChildren: () => [
      HIERARCHY_RULES.Task,
    ],
  },
  Task: {
    name: 'Task',
    color: '#fff',
    backgroundColor: '#C6C6C6',
    getChildren: () => [

    ],
  },
};

const option = {
  container: 'jsmind_container',
  theme: 'normal',
  editable: true,
  hasInteraction: true,
  enableDraggable: true,
  depth: 4,
  hierarchyRule: HIERARCHY_RULES,
};

@Component({
  selector: 'ngx-mindmap',
  templateUrl: './mindmap.component.html',
  styleUrls: ['./mindmap.component.scss'],
})

export class MindmapComponent implements OnInit, OnDestroy {
  lists: any;
  liststemp: string;
  SprintId: string;
  projectdata: any;
  listtemp: any;
  tasktemp: any;
  body: any;
  body1: any;
  id: any;
  mindMap;

  subscription: Subscription;

  mind = {};
  constructor(private projectService: ProjectService,
    private route: Router,
    activeid: ActivatedRoute) {
    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });
  }

  ngOnInit(): void {
    const put = interval(50000);
    this.subscription = put.subscribe(val => this.updateNode());
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        this.projectdata = data;
        if (data.ProjectSprints.length <= 1) {
          this.route.navigateByUrl('/page/project/info/sprint-add');
          this.body = data.ProjectSprints.length;
        } else {
          for (let i = 0; i < data.ProjectSprints.length; i++) {
            this.projectService.getSprint(data.ProjectSprints[i]).subscribe(
              res => {
                if (res.MainSprint === 'Main') {
                  // tslint:disable-next-line: no-console
                  console.log(res);
                  this.lists = res.SprintList;
                  this.SprintId = res.SprintId;
                  this.mind = {
                    format: 'nodeTree',
                    data: {
                      id: res.SprintId,
                      topic: res.SprintName,
                      selectedType: 'Sprint',
                      backgroundColor: '#7EC6E1',
                      children: [
                      ],
                    },
                  };
                  for (let j = 0; j < this.lists.length; j++) {
                    this.listtemp = {
                      id: this.lists[j].ListId,
                      color: '#fff',
                      topic: this.lists[j].ListName,
                      direction: 'right',
                      selectedType: 'List',
                      backgroundColor: '#616161',
                      children: [],
                    };
                    for (let k = 0; k < this.lists[j].ListTask.length; k++) {
                      this.tasktemp = {
                        id: this.lists[j].ListTask[k].TaskId + j,
                        color: '#fff',
                        topic: this.lists[j].ListTask[k].TaskName,
                        direction: 'right',
                        selectedType: 'Task',
                        backgroundColor: '#989898',
                      };
                      this.listtemp.children.push(this.tasktemp);
                    }
                    this.mind['data'].children.push(this.listtemp);
                  }
                  this.mindMap = MindMapMain.show(option, this.mind);
                  this.mindMap.draggable;
                }
              });
          }
        }
      }, error => {
        console.error(error);
      });
  }

  ngOnDestroy() {
    if (this.body <= 1) {
      this.subscription.unsubscribe();
    } else {
      this.updateNode();
      this.subscription.unsubscribe();
    }
  }

  removeNode() {
    const selectedNode = this.mindMap.getSelectedNode();
    const selectedId = selectedNode && selectedNode.id;
    // const i = selectedNode.parent.id;
    // tslint:disable-next-line: no-console
    console.log(selectedNode.parent.id);
    // console.log((selectedId).substr(0,6));
    if (!selectedId) {
      return;
    }
    this.mindMap.removeNode(selectedId);
    this.getMindMapData();
    if ((selectedId).substr(0, 2) === 'L0') {
      this.projectService.deleteList(this.SprintId, selectedId).subscribe(
        res => {
          // tslint:disable-next-line: no-console
          console.log(res);
          // this.getPage();
        }, error => {
          console.error(error);
        });
    }
    if ((selectedId).substr(0, 2) === 'T0') {
      this.projectService.deleteTask((selectedId).substr(0, 6)).subscribe(
        res => {
          // tslint:disable-next-line: no-console
          console.log(res);
          // this.getPage();
        }, error => {
          console.error(error);
        });
    }
  }

  updateNode() {
    // const selectedNode = this.mindMap.getSelectedNode();
    // const selectedId = selectedNode && selectedNode.id;

    // this.mindMap.beginEdit(selectedNode);
    const datamindmap = this.mindMap.getData().data;
    for (let i = 0; i < datamindmap.children.length; i++) {
      this.body = {
        SprintId: datamindmap.id,
        ListId: datamindmap.children[i].id,
        ListName: datamindmap.children[i].topic,
        SelectedType: datamindmap.children[i].selectedType,
      };
      // console.log(this.body);
      this.projectService.editMindList(this.body).subscribe(
        res => {
          // tslint:disable-next-line: no-console
          console.log(res);
        }, error => {
          console.error(error);
        });
      // console.log('spn' + datamindmap.id);
      // console.log('list' + datamindmap.children[i].id);
      for (let j = 0; j < datamindmap.children[i].children.length; j++) {
        this.body1 = {
          SprintId: datamindmap.id,
          ListId: datamindmap.children[i].id,
          TaskId: (datamindmap.children[i].children[j].id).substr(0, 6),
          TaskName: datamindmap.children[i].children[j].topic,
          SelectedType: datamindmap.children[i].children[j].selectedType,
        };
        // console.log(this.body1);
        this.projectService.editMindTask(this.body1).subscribe(
          response => {
            // tslint:disable-next-line: no-console
            console.log(response);
          }, error => {
            console.error(error);
          });
        // console.log('task' + datamindmap.children[i].children[j].id);
      }
    }
    // this.getPage();
  }

  addNode() {
    const selectedNode = this.mindMap.getSelectedNode();
    // const selectedId = selectedNode && selectedNode.id;
    if (!selectedNode) {
      return;
    }
    // const nodeId = customizeUtil.uuid.newid();
    // tslint:disable-next-line: no-console
    console.log(selectedNode);
    // console.log(nodeId);
    // this.mindMap.addNode(selectedNode, nodeId, 'New List');
    // tslint:disable-next-line: no-console
    console.log(selectedNode.children.length - 1);

    if ((selectedNode.id).substr(0, 2) === 'SP') {
      this.body = {
        SprintId: selectedNode.id,
      };
      this.projectService.createNewList(this.body).subscribe(
        data => {
          // tslint:disable-next-line: no-console
          console.log(data);
          // this.getPage();
          this.mindMap.addNode(selectedNode, data.ListId, 'New List');
        }, error => {
          console.error(error);
        });
    }
    if ((selectedNode.id).substr(0, 2) === 'L0') {
      this.body = {
        SprintId: this.SprintId,
        ListId: selectedNode.id,
        TaskName: 'New Task',
      };
      this.projectService.createTask(this.body).subscribe(
        data => {
          // tslint:disable-next-line: no-console
          console.log(data);
          this.mindMap.addNode(selectedNode, data.TaskId, 'New Task');
          // this.getPage();
        }, error => {
          console.error(error);
        });
    }
  }

  getMindMapData() {
    const data = this.mindMap.getData().data;
    // tslint:disable-next-line: no-console
    console.log('data: ', data);
  }

  getPage() {
    // tslint:disable-next-line: no-console
    console.log('get');
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        this.projectdata = data;
        for (let i = 0; i < data.ProjectSprints.length; i++) {
          this.projectService.getSprint(data.ProjectSprints[i]).subscribe(
            res => {
              if (res.MainSprint === 'Main') {
                // tslint:disable-next-line: no-console
                console.log(res);
                this.lists = res.SprintList;
                this.SprintId = res.SprintId;
                this.mind = {
                  format: 'nodeTree',
                  data: {
                    id: res.SprintId,
                    topic: res.SprintName,
                    selectedType: 'Sprint',
                    backgroundColor: '#7EC6E1',
                    children: [
                    ],
                  },
                };
                for (let j = 0; j < this.lists.length; j++) {
                  this.listtemp = {
                    id: this.lists[j].ListId,
                    color: '#fff',
                    topic: this.lists[j].ListName,
                    direction: 'right',
                    selectedType: 'List',
                    backgroundColor: '#616161',
                    children: [],
                  };
                  for (let k = 0; k < this.lists[j].ListTask.length; k++) {
                    this.tasktemp = {
                      id: this.lists[j].ListTask[k].TaskId + j,
                      color: '#fff',
                      topic: this.lists[j].ListTask[k].TaskName,
                      direction: 'right',
                      selectedType: 'Task',
                      backgroundColor: '#989898',
                    };
                    this.listtemp.children.push(this.tasktemp);
                  }
                  this.mind['data'].children.push(this.listtemp);
                }
                this.mindMap = MindMapMain.show(option, this.mind);
                this.mindMap.draggable;
              }
            });
        }
      }, error => {
        console.error(error);
      });
  }
}
