import { Component, OnInit } from '@angular/core';
import { ProjectData } from '../project-info.model';
import { ProjectService } from '../../project.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ClientService } from '../../../client/client.service';
import { NbDateService } from '@nebular/theme';

@Component({
  selector: 'ngx-info',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  projectinfo: ProjectData;
  ngModelDate = new Date();

  // Project
  ProjectName: string;
  ProjectDesc: string;
  ProjectLink: string;
  ProjectPercent = '';
  ProjectPrice = '';
  ProjectPayment: string;
  ProjectStart: any;
  ProjectEnd: any;
  ProjectPicturePath: any;
  ProjectStatus: string;

  ProjectClient = '';
  ProjectHead = '';
  ProjectStaffs = [];

  // Client
  ClientId = '';
  ClientName: string;
  ClientEmail: string;
  ClientTel: string;
  ClientAddress: string;
  ClientTaxId: string;
  ClientPicturePath: any;
  id: any;
  body: any;

  daysLeft = 0;

  constructor(private projectService: ProjectService,
    private clientService: ClientService,
    private route: Router,
    activeid: ActivatedRoute,
    protected dateService: NbDateService<Date>) {
    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });
  }

  ngOnInit() {
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.ProjectName = data.ProjectName;
        this.ProjectDesc = data.ProjectDesc;
        this.ProjectLink = data.ProjectLink;
        this.ProjectPercent = data.ProjectPercent;
        this.ProjectStart = data.ProjectStart;
        this.ProjectEnd = data.ProjectEnd;
        this.ProjectPicturePath = 'http://206.189.34.171:7777/project/img/' + data.ProjectPicturePath;
        this.ProjectClient = data.ProjectClient;
        this.ProjectStatus = data.ProjectStatus;
        this.daysLeft =
          Math.round(
            (data.ProjectEnd * 1000 - Math.round(this.dateService.today().getTime())) / (1000 * 60 * 60 * 24));

        this.clientService.getClientWithAuth(this.ProjectClient).subscribe(
          res => {
            // tslint:disable-next-line: no-console
            console.log(res);
            this.ClientId = res.ClientId;
            this.ClientName = res.ClientNameEn;
            this.ClientEmail = res.ClientEmail;
            this.ClientTel = res.ClientTel;
            this.ClientAddress = res.ClientAddress;
            this.ClientTaxId = res.ClientTaxId;
            this.ClientPicturePath = 'http://206.189.34.171:7777/client/img/' + res.ClientPicturePath;
          }, error => {
            console.error(error);
          });
      }, error => {
        console.error(error);
      });
  }

  onEdit() {
    this.route.navigateByUrl('/page/project/info/edit/' + this.id);
  }

  updateStatus() {
    this.body = {
      ProjectId: this.id,
      ProjectStatus: 'done',
    };
    this.projectService.updateStatusProject(this.body).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
      }, error => {
        console.error(error);
      });
  }
}
