import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Taskboard } from '../../shared/Sprint.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectService } from '../../../project.service';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'ngx-get-backlog',
  templateUrl: './get-backlog.component.html',
  styleUrls: ['./get-backlog.component.scss'],
})
export class GetBacklogComponent implements OnInit {
  body: any;
  taskboard: Taskboard[] = [];
  sprintTasks: Taskboard[] = [{
    ListName: 'Backlog for this Sprint',
    ListId: 'tempId',
    SelectedType: 'Task',
    index: 1,
    Task: [],
  }];
  lists = [];
  @Input() Sprintid;
  @Input() SprintDe;
  @Output() getdataEvent = new EventEmitter<string>();
  constructor(private modalService: NgbModal,
    private dragula: DragulaService,
    private projectService: ProjectService) { }

  ngOnInit() {
    this.getbacklog();

    this.dragula.drop('task-group2').subscribe(value => {
      this.lists.push(this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
        .Task[this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
          .Task.findIndex(y => y.TaskId === value.el.id)]);
      // tslint:disable-next-line: no-console
      console.log(this.lists);
    });
  }

  moveBacklog() {
    for (let i = 0; i < this.lists.length; i++) {
      this.body = {
        SprintId: this.Sprintid,
        TaskId: this.lists[i].TaskId,
        TaskName: this.lists[i].TaskName,
        TaskDesc: this.lists[i].TaskDesc,
        DueDate: Math.round(this.lists[i].TaskDuedate / 1000),
        SelectedType: 'Task',
      };
      // tslint:disable-next-line: no-console
      console.log(this.body);
      this.projectService.editTaskdata(this.body).subscribe(
        res => {
          // tslint:disable-next-line: no-console
          console.log(res);
          this.outputgetpage();
          this.modalService.dismissAll();
        }, error => {
          console.error(error);
        });
    }
  }

  getbacklog() {
    this.taskboard = [];
    // this.SprintDId = data.ProjectSprintDe;
    this.projectService.getSprint(this.SprintDe).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        for (let i = 0; i < res.SprintList.length; i++) {
          this.projectService.getList(res.SprintList[i]).subscribe(
            response => {
              if (response.ListName === 'backlog') {
                this.taskboard.push(response);
                // tslint:disable-next-line: no-console
                console.log(i, this.taskboard);
                for (let j = this.taskboard[i].Task.length - 1; j >= 0; j--) {
                  if (response.Task[j].SprintId !== '') {
                    this.taskboard[i].Task.splice(j, 1);
                  }
                }
              }
            });
        }
      });
  }

  cancelmove() {
    this.modalService.dismissAll();
  }

  outputgetpage() {
    this.getdataEvent.emit();
  }
}
