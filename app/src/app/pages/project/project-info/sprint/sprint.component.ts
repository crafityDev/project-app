import { Component, OnInit, OnDestroy, EventEmitter, Output, ViewChild } from '@angular/core';
// import { TaskService } from '../shared/task.service';
import { NbDateService, NbPopoverDirective } from '@nebular/theme';
import { List, Task, Sprint, Taskboard } from '../shared/Sprint.model';
import { FormArray, FormControl, Validators, FormGroup } from '@angular/forms';
// import { moveItemInArray, CdkDragDrop, transferArrayItem } from '@angular/cdk/drag-drop';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectService } from '../../project.service';
// import { interval, Subscription } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'ngx-sprint',
  templateUrl: './sprint.component.html',
  styleUrls: ['./sprint.component.scss'],
})
export class SprintComponent implements OnInit, OnDestroy {

  @Output() percentEvent = new EventEmitter<string>();
  id: string;
  editTask = false;

  update: any;
  body: any;
  sprint: Sprint;
  SprintDId: string;
  SprintId: string;
  SprintName: string;
  SprintStart: any;
  SprintEnd: any;
  SprintList: any;
  min: any;
  // getsprint = [];
  // subscription: Subscription;
  controls: FormArray;
  listForm: FormGroup;

  taskboard: Taskboard[] = [];
  lists: List[] = [];
  tasks: Task[] = [];
  sprintTasks: Task[] = [];
  @ViewChild(NbPopoverDirective) popover: NbPopoverDirective;

  constructor(private modalService: NgbModal,
    // private taskService: TaskService,
    private projectService: ProjectService,
    activeid: ActivatedRoute,
    private route: Router,
    protected dateService: NbDateService<Date>) {
    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });

    this.min = this.dateService.addDay(this.dateService.today(), 0);

    this.listForm = new FormGroup({
      ListName: new FormControl('', Validators.required),
    });

  }

  ngOnInit() {
    // const put = interval(50000);
    // this.subscription = put.subscribe(val => this.UpdateDrop());
    this.getPage();
  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }

  startDatePick(event) {
    this.min = event;
  }

  getControl(index: number, field: string): FormControl {
    return this.controls.at(index).get(field) as FormControl;
  }

  openVerticallyCentered(content) {
    this.popover.hide();
    this.modalService.open(content, { centered: true });
  }

  updateMainSprint(sprintId) {
    this.body = {
      SprintId: sprintId,
      ProjectId: this.id,
      MainSprint: 'Main',
    };
    this.projectService.updateMain(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.getPage();
        this.modalService.dismissAll();
      },
      error => {
        console.error(error);
      });
  }

  editSprintValue(value) {
    this.taskboard[value.indexsprint]['SprintName'] = value.sprintName;
    this.taskboard[value.indexsprint]['SprintStart'] = value.sprintStart;
    this.taskboard[value.indexsprint]['SprintEnd'] = value.sprintEnd;
  }

  deleteSprint(sprintId, i) {
    this.taskboard.splice(i, 1);
    this.projectService.deleteSprint(sprintId).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        // this.getPage();
      },
      error => {
        console.error(error);
      });

  }

  getPage() {
    this.taskboard = [];
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        this.SprintDId = data.ProjectSprintDe;
        for (let i = 0; i < data.ProjectSprints.length; i++) {
          this.projectService.getSprint(data.ProjectSprints[i]).subscribe(
            res => {
              // tslint:disable-next-line: no-console
              console.log(res);
              this.SprintId = res.SprintId;
              for (let j = 0; j < res.SprintList.length; j++) {
                this.projectService.getList(res.SprintList[j]).subscribe(
                  response => {
                    response.SprintId = res.SprintId;
                    response.SprintName = res.SprintName;
                    response.SprintEnd = res.SprintEnd;
                    response.index = i;
                    if (response.ListName === 'backlog') {
                      this.taskboard.push(response);
                      this.taskboard.sort((a, b) => a.index - b.index);
                      // tslint:disable-next-line: no-console
                      console.log(this.taskboard);
                    }
                  });
              }
            });
        }
        if (data.ProjectSprints.length <= 0) {
          this.gotoaddpage();
        }
      }, error => {
        console.error(error);
      });
  }

  gotoaddpage() {
    this.route.navigateByUrl('/page/project/info/sprint-add');
  }



  outputpercent(value) {
    this.percentEvent.emit(value);
  }

  gotoinfo(sprintId) {
    // this.route.navigateByUrl('/page/project/info/sprint-info');
    this.route.navigate(['/page/project/info/sprint-info']
      , { queryParams: { Sid: sprintId, Did: this.SprintDId, id: this.id } });
  }
}
