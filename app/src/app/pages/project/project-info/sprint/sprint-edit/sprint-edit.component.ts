import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectService } from '../../../project.service';

@Component({
  selector: 'ngx-sprint-edit',
  templateUrl: './sprint-edit.component.html',
  styleUrls: ['./sprint-edit.component.scss'],
})
export class SprintEditComponent implements OnInit {
  body: any;
  SprintName: string;
  SprintStart: any;
  SprintEnd: any;
  SprintList: any;
  min: any;

  sprintForm: FormGroup;
  @Input() Sprintdata;
  @Input() projectid;
  @Input() indexdata;
  @Output() editsprintEvent = new EventEmitter
    <{ sprintName: any, sprintStart: any, sprintEnd: any, indexsprint: any }>();
  constructor(private modalService: NgbModal,
    private projectService: ProjectService) {
    this.sprintForm = new FormGroup({
      SprintName: new FormControl('', Validators.required),
      SprintStart: new FormControl('', Validators.required),
      SprintEnd: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    this.projectService.getSprint(this.Sprintdata).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        this.sprintForm = new FormGroup({
          SprintName: new FormControl(res.SprintName, Validators.required),
          SprintStart: new FormControl(new Date(res.SprintStart * 1000), Validators.required),
          SprintEnd: new FormControl(new Date(res.SprintEnd * 1000), Validators.required),
        });
        this.SprintList = res.SprintList;
      },
      error => {
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  editSprint() {
    this.body = {
      SprintId: this.Sprintdata,
      ProjectId: this.projectid,
      SprintList: this.SprintList,
      SprintName: this.sprintForm.get('SprintName').value,
      SprintStart: Math.round((this.sprintForm.get('SprintStart').value).getTime() / 1000),
      SprintEnd: Math.round((this.sprintForm.get('SprintEnd').value).getTime() / 1000),
    };
    this.projectService.editSprint(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.outputeditSprint();
        this.modalService.dismissAll();
        // this.getPage();
      },
      error => {
        console.error(error);
      });
  }

  canceleditsprint() {
    this.modalService.dismissAll();
    this.sprintForm.reset();
  }

  outputeditSprint() {
    this.editsprintEvent.emit({
      sprintName: this.sprintForm.get('SprintName').value,
      sprintStart: Math.round((this.sprintForm.get('SprintStart').value).getTime() / 1000),
      sprintEnd: Math.round((this.sprintForm.get('SprintEnd').value).getTime() / 1000),
      indexsprint: this.indexdata,
    });
  }

  startDatePick(event) {
    this.min = event;
  }

}
