import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from '../../../project.service';
import { Taskboard } from '../../shared/Sprint.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'ngx-sprint-info',
  templateUrl: './sprint-info.component.html',
  styleUrls: ['./sprint-info.component.scss'],
})
export class SprintInfoComponent implements OnInit {
  @Output() percentEvent = new EventEmitter<string>();

  id: string;
  SprintId: string;
  SprintDId: string;
  body: any;
  body1: any;
  temp: any[] = [];
  SprintName: string;
  SprintStart: any;
  SprintEnd: any;
  taskAddForm: FormGroup;
  taskEditForm: FormGroup;
  listForm: FormGroup;
  listEditForm: FormGroup;

  taskboard: Taskboard[] = [];
  lists = [];
  lists1 = [];
  listde = [];
  temptask: any;
  listadd = false;

  constructor(active: ActivatedRoute,
    private modalService: NgbModal,
    private projectService: ProjectService,
    private dragula: DragulaService) {
    active.queryParams.subscribe(
      queryParams => {
        this.SprintId = queryParams['Sid'];
        this.SprintDId = queryParams['Did'];
        this.id = queryParams['id'];
      });

    this.taskEditForm = new FormGroup({
      TaskName: new FormControl('', Validators.required),
      TaskPoint: new FormControl(''),
      TaskDesc: new FormControl(''),
    });

    this.taskAddForm = new FormGroup({
      TaskName1: new FormControl('', Validators.required),
      TaskPoint1: new FormControl(''),
      TaskDesc1: new FormControl(''),
    });

    this.listForm = new FormGroup({
      ListName: new FormControl('', Validators.required),
    });

    this.listEditForm = new FormGroup({
      ListName1: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    this.getPage();

    this.dragula.drop('task-group-sprint').subscribe(value => {
      // console.log(value.el.id);
      // console.log(value.source.id)
      // console.log(value.target.id);
      this.lists = [];
      this.lists1 = [];
      this.temptask = this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
        .Task[this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
          .Task.findIndex(y => y.TaskId === value.el.id)];

      this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
        .Task.splice(this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)]
          .Task.findIndex(y => y.TaskId === value.el.id), 1);

      this.taskboard[this.taskboard.findIndex(x => x.ListId === value.target.id)].Task.push(this.temptask);

      for (let i = 0; i < this.taskboard[
        this.taskboard.findIndex(x => x.ListId === value.target.id)].Task.length; i++) {
        this.lists.push(this.taskboard[
          this.taskboard.findIndex(x => x.ListId === value.target.id)].Task[i].TaskId);
      }
      for (let i = 0; i < this.taskboard[
        this.taskboard.findIndex(x => x.ListId === value.source.id)].Task.length; i++) {
        this.lists1.push(this.taskboard[
          this.taskboard.findIndex(x => x.ListId === value.source.id)].Task[i].TaskId);
      }
      this.body = {
        SprintTemp: this.SprintDId,
        ListName: this.taskboard[this.taskboard.findIndex(x => x.ListId === value.target.id)].ListName,
        ListId: this.taskboard[this.taskboard.findIndex(x => x.ListId === value.target.id)].ListId,
        ListTask: this.lists,
      };
      this.body1 = {
        SprintTemp: this.SprintDId,
        ListName: this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)].ListName,
        ListId: this.taskboard[this.taskboard.findIndex(x => x.ListId === value.source.id)].ListId,
        ListTask: this.lists1,
      };
      this.temp = [];
      this.temp.push(this.body);
      this.temp.push(this.body1);
      this.projectService.editList(this.temp).subscribe(
        data => {
          // tslint:disable-next-line: no-console
          console.log(data);
          this.outputpercent(data['ProjectPercent']);
          // this.getPage();
        },
        error => {
          console.error(error);
        });
    });
  }

  getPage() {
    this.taskboard = [];
    this.projectService.getSprint(this.SprintDId).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        for (let j = 0; j < res.SprintList.length; j++) {
          this.projectService.getList(res.SprintList[j]).subscribe(
            response => {
              // tslint:disable-next-line: no-console
              this.listde.push(response);
            });
        }
      },
      error => {
        // tslint:disable-next-line: no-console
        console.error(error);
      });
    this.projectService.getSprint(this.SprintId).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        this.SprintName = res.SprintName;
        this.SprintStart = res.SprintStart;
        this.SprintEnd = res.SprintEnd;
        // this.SprintId = res.SprintId;
        for (let i = 0; i < res.SprintList.length; i++) {
          this.projectService.getList(res.SprintList[i]).subscribe(
            response => {
              response.add = false;
              response.listedit = false;
              response.index = i;
              for (let j = 0; j < response.Task.length; j++) {
                response.Task[j].edit = false;
              }
              this.taskboard.push(response);
              this.taskboard.sort((a, b) => a.index - b.index);
              // tslint:disable-next-line: no-console
              console.log(this.taskboard);
            });
        }
      },
      error => {
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  onAddTask(listid, listname) {
    this.body1 = {
      CompanyId: localStorage.getItem('companyId'),
      ProjectId: this.id,
      SprintId: this.SprintId,
      SprintDe: this.SprintDId,
      // ListDe: listid,
      TaskName: this.taskAddForm.get('TaskName1').value,
      DueDate: 0,
    };
    if (listname === 'complete') {
      this.body = {
        ProjectId: this.id,
        SprintId: this.SprintId,
        SprintDe: this.SprintDId,
        ListId: listid,
        ListDe: this.listde[this.listde.findIndex(x => x.ListName === 'complete')].ListId,
        TaskName: this.taskAddForm.get('TaskName1').value,
      };
    } else {
      this.body = {
        ProjectId: this.id,
        SprintId: this.SprintId,
        SprintDe: this.SprintDId,
        ListId: listid,
        ListDe: this.listde[this.listde.findIndex(x => x.ListName === 'backlog')].ListId,
        TaskName: this.taskAddForm.get('TaskName1').value,
      };
    }
    if (this.body.TaskName === '') {
      this.taskboard[this.taskboard.findIndex(x => x.ListId === listid)]['add'] = false;
    }
    if (this.body.TaskName === null) {
      this.taskboard[this.taskboard.findIndex(x => x.ListId === listid)]['add'] = false;
    }
    if (this.body.TaskName !== '' && this.body.TaskName !== null) {
      this.projectService.createTask(this.body).subscribe(
        data => {
          // tslint:disable-next-line: no-console
          console.log(data);
          this.outputpercent(data['ProjectPercent']);
          this.body1.TaskId = data.TaskId;
          this.taskboard[this.taskboard.findIndex(x => x.ListId === listid)]['add'] = false;
          this.taskboard[this.taskboard.findIndex(x => x.ListId === listid)].Task.push(this.body1);
        }, error => {
          console.error(error);
        });
    }
    this.taskAddForm.reset();
  }

  onEditTaskName(taskid, listindex, taskindex) {
    this.body = {
      TaskId: taskid,
      TaskName: this.taskEditForm.get('TaskName').value,
      SelectedType: 'Task',
    };
    if (this.body.TaskName !== '') {
      this.taskboard[listindex].Task[taskindex]['TaskName'] = this.taskEditForm.get('TaskName').value;
      this.taskboard[listindex].Task[taskindex]['edit'] = false;
      this.projectService.editMindTask(this.body).subscribe(
        data => {
          // tslint:disable-next-line: no-console
          console.log(data);
        }, error => {
          console.error(error);
        });
      this.taskEditForm.reset();
    }
  }

  onAddList() {
    this.body = {
      SprintId: this.SprintId,
      ListName: this.listForm.get('ListName').value,
    };
    this.projectService.createList(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.body1 = {
          ListName: this.listForm.get('ListName').value,
          ListId: data.ListId,
          Task: [],
        };
        this.taskboard.push(this.body1);
        this.listForm.reset();
      }, error => {
        console.error(error);
      });
    this.listadd = false;
  }

  onEditList(listid, i) {
    this.taskboard[i].ListName = this.listEditForm.get('ListName1').value;
    this.taskboard[i]['listedit'] = false;
    this.body = {
      SprintId: this.SprintId,
      ListId: listid,
      ListName: this.listEditForm.get('ListName1').value,
    };
    // console.log(this.body);
    this.projectService.editMindList(this.body).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
      }, error => {
        console.error(error);
      });
  }

  onDeleteList(listid, listindex) {
    this.taskboard.splice(listindex, 1);
    // this.lists.splice(listindex, 1);
    this.projectService.deleteList(this.SprintId, listid).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        // this.getPage();
      }, error => {
        console.error(error);
      });
  }

  addTaskClick(listindex) {
    this.taskboard[listindex]['add'] = true;
  }

  editTaskNameClick(listindex, taskindex, taskname) {
    this.taskboard[listindex].Task[taskindex]['edit'] = true;
    this.taskEditForm = new FormGroup({
      TaskName: new FormControl(taskname, Validators.required),
    });
  }

  addlistClick() {
    this.listadd = !this.listadd;
  }

  editlistClick(listname, i) {
    this.listEditForm = new FormGroup({
      ListName1: new FormControl(listname, Validators.required),
    });
    this.taskboard[i]['listedit'] = true;
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  spliceTask(value) {
    this.taskboard[value.listindex].Task.splice(value.taskindex, 1);
    // tslint:disable-next-line: no-console
    console.log(value);
  }

  editTaskValue(value) {
    this.taskboard[value.listindex].Task[value.taskindex]['TaskName'] = value.taskname;
    this.taskboard[value.listindex].Task[value.taskindex]['TaskDesc'] = value.taskdesc;
    this.taskboard[value.listindex].Task[value.taskindex]['DueDate'] = value.duedate;
    // tslint:disable-next-line: no-console
    console.log(value);
  }

  editSprintValue(value) {
    this.SprintName = value.sprintName;
    this.SprintStart = value.sprintStart;
    this.SprintEnd = value.sprintEnd;
  }

  openXl(content) {
    this.modalService.open(content, { centered: true, windowClass: 'xlModal' });
  }

  openTask(content, listindex, taskindex) {
    this.taskboard[listindex].Task[taskindex]['indextemp'] = taskindex;
    this.modalService.open(content, { centered: true });
  }

  outputpercent(value) {
    this.percentEvent.emit(value);
  }

}
