import { Component, OnInit, HostListener } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ProjectData } from './project-info.model';
import { NbMenuItem, NbDateService } from '@nebular/theme';
import { ProjectService } from '../project.service';


@Component({
  selector: 'ngx-project-info',
  templateUrl: './project-info.component.html',
  styleUrls: ['./project-info.component.scss'],
})
export class ProjectInfoComponent implements OnInit {
  projectinfo: ProjectData;
  formControl = new FormControl(new Date());
  ngModelDate = new Date();
  id: any;
  items: NbMenuItem[] = [];

  ProjectName: string;
  ProjectLink: string;
  ProjectPercent = '';
  ProjectStart: any;
  ProjectEnd: any;
  ProjectPicturePath: any;
  ProjectStatus = '';

  daysLeft: number;
  body: any;

  isMobile = false;
  isSmall = false;
  showMenu = false;

  @HostListener('window:resize', [])
  onResize() {
    const width = window.innerWidth;
    if (width < 767) {
      this.isMobile = true;
      if (width < 500) {
        this.isSmall = true;
      } else {
        this.isSmall = false;
      }
    } else {
      this.isMobile = false;
      this.isSmall = false;
    }
  }

  constructor(private projectService: ProjectService,
    protected dateService: NbDateService<Date>) {
    this.id = localStorage.getItem('project');
    this.items = [{
      title: 'Dashboard',
      link: '/page/project/info/dashboard/' + this.id,
    },
    {
      title: 'Backlog',
      link: '/page/project/info/tasks/' + this.id,
    },
    {
      title: 'Sprint',
      link: '/page/project/info/sprint/' + this.id,
    },
    // {
    //   title: 'Mind Map',
    //   link: '/page/project/info/mindmap/' + this.id,
    // },
    {
      title: 'Documents',
      children: [
        {
          title: 'Quotation',
          link: '/page/project/info/docs/quotation',
        },
        {
          title: 'Purchase Order',
          link: '/page/project/info/docs/purchase-order',
        },
        {
          title: 'Invoice',
          link: '/page/project/info/docs/invoice',
        },
        {
          title: 'Receipt',
          link: '/page/project/info/docs/receipt',
        },
        {
          title: 'Other',
          link: '/page/project/info/docs/other',
        },
      ],
    },
    {
      title: 'Team',
      link: '/page/project/info/staffs/' + this.id,
    },
    {
      title: 'Cost',
      link: '/page/project/info/cost/' + this.id,
    },
    {
      title: 'Summary',
      link: '/page/project/info/detail/' + this.id,
    }];
  }

  ngOnInit() {
    const width = window.innerWidth;
    if (width < 767) {
      this.isMobile = true;
      if (width < 500) {
        this.isSmall = true;
      } else {
        this.isSmall = false;
      }
    } else {
      this.isMobile = false;
      this.isSmall = false;
    }
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.ProjectName = data.ProjectName;
        // this.ProjectDesc = data.ProjectDesc;
        this.ProjectLink = data.ProjectLink;
        this.ProjectPercent = data.ProjectPercent;
        // this.ProjectPrice = data.ProjectPrice;
        // this.ProjectPayment = data.ProjectPayment;
        this.ProjectStart = data.ProjectStart;
        this.ProjectEnd = data.ProjectEnd;
        this.ProjectPicturePath = 'http://206.189.34.171:7777/project/img/' + data.ProjectPicturePath;

        this.ProjectStatus = data.ProjectStatus;
        this.daysLeft =
          Math.round(
            (data.ProjectEnd * 1000 - Math.round(this.dateService.today().getTime())) / (1000 * 60 * 60 * 24));
        // this.ProjectClient = data.ProjectClient;
        // this.ProjectHead = '';
        // this.ProjectStaffs = [];
      }, error => {
        console.error(error);
      });
    // this.projectinfo = {
    //   projectname: 'test',
    //   start: this.ngModelDate,
    //   due: this.ngModelDate,
    //   requirement: '10%fjdsifsj',
    //   price: 197565,
    //   payment: 'asdas',
    // };
  }

  closeMenu() {
    setTimeout(() => {
      this.showMenu = false;
    }, 250);
  }

  updateStatus(data) {
    this.body = {
      ProjectId: this.id,
      ProjectStatus: data,
    };
    this.projectService.updateStatusProject(this.body).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        this.ProjectStatus = data;
      }, error => {
        console.error(error);
      });
  }

  show(event) {
    event.percentEvent.subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.ProjectPercent = data;
      });
  }
}
