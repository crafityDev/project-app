import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { NbToastrService } from '@nebular/theme';
import { ProjectService } from '../../../project.service';

@Component({
  selector: 'ngx-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss'],
})
export class PurchaseOrderComponent implements OnInit {

  Docs = [];
  setting = {
    actions: {
      add: false,
      edit: false,
      delete: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      PDFnum: {
        title: 'No.',
        type: 'string',
      },
      CreateDate: {
        title: 'Date',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private projectService: ProjectService,
    private route: Router,
    private toastrService: NbToastrService,
    private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.getDoc();
  }

  onDeleteConfirm(event): void {
    this.projectService.deletePur(event.data.POPDFId).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getDoc();
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  onAdd() {
    this.route.navigateByUrl('/page/project/info/docs/pur-add');
  }

  TableToInfo(event) {
    window.open('http://206.189.34.171:7777/po/pdf/' + event.data.PDFPath, '_blank');
  }

  getDoc() {
    this.projectService.getPur().subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.Docs = response;
        for (let i = 0; i < this.Docs.length; i++) {
          this.Docs[i].CreateDate = this.datePipe.transform(new Date(this.Docs[i].CreateDate * 1000), 'MMM d, y');
        }
        this.source.load(this.Docs);
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }
}
