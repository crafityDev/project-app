import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../../../client/client.service';
import { ProjectService } from '../../../project.service';
import { StaffService } from '../../../../company/staff/staff.service';
import { CompanyService } from '../../../../company/company.service';
import { FormGroup } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-inv-add',
  templateUrl: './inv-add.component.html',
  styleUrls: ['./inv-add.component.scss'],
})
export class InvAddComponent implements OnInit {
  id: string;
  body: any;
  ProjectName: string;
  ProjectDesc: string;
  ProjectLink: string;
  ProjectStart: any;
  ProjectEnd: any;
  ProjectClient: string;
  ProjectHead: string;
  ProjectPayment: string;
  VatPrice: number;
  Total: number;
  SubTotal: number;
  logo: any;

  ClientName: string;
  ClientTel: string;
  ClientAddress: string;
  ClientTaxId: string;

  CompanyName: string;
  CompanyTel: string;
  CompanyAddress: string;
  CompanyTaxId: string;

  HeadName: String;
  HeadSig: any;

  projectForm: FormGroup;
  tabledata = [];
  setting = {
    actions: {
      add: true,
      edit: true,
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
      mode: 'inline',
    },
    columns: {
      No: {
        title: 'No.',
        type: 'string',
      },
      Description: {
        title: 'Description',
        type: 'string',
      },
      Quantity: {
        title: 'Quantity',
        type: 'number',
      },
      Unit: {
        title: 'Unit',
        type: 'String',
        addable: false,
        editable: false,
      },
      UnitPrice: {
        title: 'Unit Price/Baht',
        type: 'number',
      },
      Amount: {
        title: 'Amount',
        type: 'number',
        addable: false,
        editable: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource(this.tabledata);

  constructor(private clientService: ClientService,
    private projectService: ProjectService,
    private staffService: StaffService,
    private companyService: CompanyService) {
    this.id = localStorage.getItem('project');
  }

  ngOnInit() {
    this.getDescription();
    this.companyService.getCompanyWithAuth().subscribe(
      res1 => {
        this.logo = 'http://206.189.34.171:7777/company/img/' + res1.CompanyPicturePath;
        this.CompanyName = res1.CompanyName;
        this.CompanyAddress = res1.CompanyAddress;
        this.CompanyTel = res1.CompanyTel;
        this.CompanyTaxId = res1.CompanyTaxId;
      }, error => {
        console.error(error);
      });
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        this.ProjectName = data.ProjectName;
        this.ProjectDesc = data.ProjectDesc;
        this.ProjectLink = data.ProjectLink;
        this.ProjectPayment = data.ProjectPayment;
        this.ProjectStart = new Date(data.ProjectStart * 1000);
        this.ProjectEnd = new Date(data.ProjectEnd * 1000);
        this.ProjectHead = data.ProjectHead;
        this.ProjectClient = data.ProjectClient;
        this.clientService.getClientWithAuth(data.ProjectClient).subscribe(
          res => {
            this.ClientName = res.ClientName;
            this.ClientTel = res.ClientTel;
            this.ClientAddress = res.ClientAddress;
            this.ClientTaxId = res.ClientTaxId;
          });
        this.staffService.getStaffWithAuth(data.ProjectHead, localStorage.getItem('companyId')).subscribe(
          response => {
            this.HeadName = response.StaffName;
            this.HeadSig = 'http://206.189.34.171:7777/staff/sig/' + response.StaffSignature;
          });
      },
      error => {
        console.error(error);
      });
  }

  onCreateConfirm(event) {
    if (window.confirm('Are you sure you want to Add?')) {
      event.confirm.resolve(event.newData);
      this.body = {
        ProjectId: this.id,
        No: event.newData.No,
        Description: event.newData.Description,
        Quantity: Number(event.newData.Quantity),
        Unit: 'PCS',
        UnitPrice: Number(event.newData.UnitPrice),
      };
      this.projectService.postCostDescription(this.body).subscribe(
        res => {
          // tslint:disable-next-line: no-console
          console.log(res);
          this.getDescription();
        });
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event): void {
    if (window.confirm('Are you sure you want to Save?')) {
      event.confirm.resolve(event.newData);
      this.body = {
        ID: event.newData.ID,
        ProjectId: event.newData.Projectid,
        No: event.newData.No,
        Description: event.newData.Description,
        Quantity: Number(event.newData.Quantity),
        Unit: 'PCS',
        UnitPrice: Number(event.newData.UnitPrice),
      };
      this.projectService.putCostDescription(this.body).subscribe(
        res => {
          // tslint:disable-next-line: no-console
          console.log(res);
          this.getDescription();
        });
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.projectService.deleteCostDescription(event.data.ID).subscribe(
        res => {
          // tslint:disable-next-line: no-console
          console.log(res);
          this.getDescription();
        });
    } else {
      event.confirm.reject();
    }
  }

  genPDF() {
    this.projectService.genPDFInv(this.id).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        window.open('http://206.189.34.171:7777/invoice/pdf/' + data.Path, '_blank');
      },
      error => {
        console.error(error);
      });
  }

  getDescription() {
    this.projectService.getCostDescription(this.id).subscribe(
      data => {
        this.tabledata = data.Detail;
        this.VatPrice = data.VAT;
        this.Total = data.Total;
        this.SubTotal = data.SubTotal;
        this.source.load(this.tabledata);
      });
  }

  onEditPayment() {
    this.body = {
      ProjectId: this.id,
      ProjectName: this.ProjectName,
      ProjectDesc: this.ProjectDesc,
      ProjectLink: this.ProjectLink,
      ProjectPayment: this.ProjectPayment,
      ProjectStart: Math.round((this.ProjectStart).getTime() / 1000),
      ProjectEnd: Math.round((this.ProjectEnd).getTime() / 1000),
      ProjectHead: this.ProjectHead,
      ProjectClient: this.ProjectClient,
    };
    // tslint:disable-next-line: no-console
    console.log(this.body);
    this.projectService.putProjectWithAuth(this.body).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
      },
      error => {
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }
}
