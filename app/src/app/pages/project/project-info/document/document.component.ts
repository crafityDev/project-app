import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../../@core/data/smart-table';

@Component({
  selector: 'ngx-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss'],
})
export class ProjectDocumentComponent implements OnInit {

  setting = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },

    columns: {
      id: {
        title: 'No.',
        type: 'number',
      },
      firstName: {
        title: 'Document Type',
        type: 'string',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'quotation', title: 'Quotation' },
              { value: 'purchase order', title: 'Purchase Order' },
              { value: 'invoice', title: 'Invoice' },
              { value: 'receipt', title: 'Receipt' },
            ],
          },
        },
      },
      email: {
        title: 'Date',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData) {
    const data = this.service.getData();
    this.source.load(data);
  }

  ngOnInit() {
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

}
