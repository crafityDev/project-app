import { Component, OnInit, ViewChild } from '@angular/core';
import { ProjectService } from '../../../project.service';
import { NbToastrService } from '@nebular/theme';
import { LocalDataSource } from 'ng2-smart-table';
import { saveAs } from 'file-saver';

@Component({
  selector: 'ngx-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.scss'],
})
export class OtherComponent implements OnInit {
  Deletepath: string;
  Done = false;
  SelectedCat: any;
  bodyfile: any;
  fileToUpload: File = null;
  checkfile = true;
  @ViewChild('form') form;

  selected = [
    { value: 'Etc.', Show: 'Etc.' },
    { value: 'Design', Show: 'Design' },
    { value: 'Dev', Show: 'Dev' },
    { value: 'Accounting', Show: 'Accounting' },
    { value: 'Contract', Show: 'Contract' },
  ];

  setting = {
    mode: 'external',
    actions: {
      add: false,
      edit: true,
      delete: true,
    },
    delete: {
      // deleteButtonContent: '<i class="nb-trash"></i>',
      deleteButtonContent: '<i class="fas fa-trash" icon-2x"></i>',
      confirmDelete: true,
    },
    edit: {
      editButtonContent: '<i class="fas fa-file-download" icon-2x></i>',
      confirmEdit: true,
    },
    columns: {
      filename: {
        title: 'File',
        type: 'string',
      },
      type: {
        title: 'Type',
        type: 'string',
      },
      catagory: {
        title: 'Catagory',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(
    private projectService: ProjectService,
    private toastrService: NbToastrService) {
  }

  ngOnInit() {
    this.GetFileName();
    this.SelectedCat = 'Etc.';
  }

  handleFileInput(files: FileList) {
    // tslint:disable-next-line: no-console
    this.fileToUpload = files.item(0);
    // tslint:disable-next-line: no-console
    console.log(this.fileToUpload);
    this.checkfile = false;
  }

  GetFileName() {
    this.Done = true;
    this.projectService.getFileName(localStorage.getItem('project'), localStorage.getItem('companyId')).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        if (data != null) {
          this.source.load(data);
        }
        this.Done = false;
      },
      error => {
        this.toastrService.danger('Please try again in a few minutes.', {});
        console.error(error);
        this.Done = false;
      });
  }

  UploadFile() {
    this.bodyfile = {
      type: this.fileToUpload.name.split('.')[1],
      filename: this.fileToUpload.name.split('.')[0],
      catagory: this.SelectedCat,
      CompanyId: localStorage.getItem('companyId'),
      ProjectId: localStorage.getItem('project'),
    };
    this.Done = true;
    this.projectService.UploadFile(this.bodyfile, this.fileToUpload).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.GetFileName();
        this.form.nativeElement.reset();
        this.Done = false;
      },
      error => {
        this.toastrService.danger('Please try again in a few minutes.', 'Cannot upload this file.', {});
        console.error(error);
        this.Done = false;
      });
  }

  DeleteFile(event) {
    this.Done = true;
    this.Deletepath = '/var/asset/mindtask/project/file/'
      + localStorage.getItem('companyId') + '/' + localStorage.getItem('project') + '/' + event.data.id + '/';
    this.projectService.deleteFile(this.Deletepath, localStorage.getItem('companyId'), event.data.id).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.GetFileName();
        this.Done = false;
      },
      error => {
        this.toastrService.danger('Please try again in a few minutes.', {});
        console.error(error);
        this.Done = false;
      });
  }

  download(event) {
    this.Done = true;
    this.projectService.getFile(event.data.path + '.' + event.data.type, localStorage.getItem('key')).subscribe(
      res => {
        // this.submitted = false;
        // tslint:disable-next-line: no-console
        console.log(res);
        const blob = new Blob([res], {
          type: event.data.type,
        });
        saveAs(blob, event.data.filename + '.' + event.data.type);
        this.Done = false;
      },
      error => {
        // this.submitted = false;
        console.error(error);
        this.Done = false;
        // this.showToast('error', 'Download File' + item.name, JSON.stringify(error.error));
      },
    );
  }

  selectedCat(event) {
    this.SelectedCat = event;
  }

}
