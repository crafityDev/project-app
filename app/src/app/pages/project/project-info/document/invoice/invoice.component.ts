import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { ProjectService } from '../../../project.service';
import { NbToastrService } from '@nebular/theme';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss'],
})
export class InvoiceComponent implements OnInit {

  Docs = [];
  setting = {
    actions: {
      add: false,
      edit: false,
      delete: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      PDFnum: {
        title: 'No.',
        type: 'string',
      },
      CreateDate: {
        title: 'Date',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private projectService: ProjectService,
    private route: Router,
    private toastrService: NbToastrService,
    private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.getDoc();
  }

  onDeleteConfirm(event): void {
    this.projectService.deleteInv(event.data.InvoicePDFId).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getDoc();
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  onAdd() {
    this.route.navigateByUrl('/page/project/info/docs/inv-add');
  }

  TableToInfo(event) {
    window.open('http://206.189.34.171:7777/invoice/pdf/' + event.data.PDFPath, '_blank');
  }

  getDoc() {
    this.projectService.getInv().subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.Docs = response;
        for (let i = 0; i < this.Docs.length; i++) {
          this.Docs[i].CreateDate = this.datePipe.transform(new Date(this.Docs[i].CreateDate * 1000), 'MMM d, y');
        }
        this.source.load(this.Docs);
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }
}
