import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { ActivatedRoute, Params } from '@angular/router';
import { ProjectService } from '../../project.service';
import { StaffService } from '../../../company/staff/staff.service';


@Component({
  selector: 'ngx-member',
  templateUrl: './staffs.component.html',
  styleUrls: ['./staffs.component.scss'],
})
export class StaffsComponent implements OnInit {

  @ViewChild('staffForm') staffForm: NgForm;

  temp: any;
  tempid: string;
  staff: any[] = [];
  stemp: any;
  postStaff: any;
  postStaff1: any;
  selectedItem: any;
  // TypeAhead
  staffModel: any;
  staffs: { StaffId: string, StaffNameEn: string, StaffPicturePath: string, StaffPositionName: string }[] = [];
  id: any;
  HeadId: String;
  HeadName: String;
  HeadEmail: String;
  HeadTel: String;
  HeadPic: any;
  HeadSig: any;

  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  staffSearch = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.staffs.filter(v => v.StaffNameEn.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10)),
    )
  staffFormatter = (x: { StaffNameEn: string }) => x.StaffNameEn;


  constructor(private projectService: ProjectService,
    private staffService: StaffService,
    activeid: ActivatedRoute) {
    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });
    // this.id = localStorage.getItem('project');
  }


  ngOnInit() {
    this.getStaffDataFunc();
  }

  items = [
    { title: 'Edit', link: '/page/client/edit' },
    { title: 'Delete', link: '/page/client/edit' },
  ];

  arrayOne(n: number): any[] {
    return Array(n);
  }

  addStaff() {
    this.postStaff = {
      ProjectStaffs: this.tempid,
      ProjectId: this.id,
    };
    this.projectService.postProjectStaffWithAuth(this.postStaff).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getStaffDataFunc();
      },
      error => {
        // tslint:disable-next-line: no-console
        console.error(error);
      });
    this.staffForm.reset();
  }

  cancelStaff() {
    this.staffForm.reset();
  }

  setManager(index: number) {
    this.postStaff1 = this.temp;
    this.postStaff1['ProjectHead'] = this.staff[index].StaffId;
    this.postStaff = {
      ProjectStaffs: this.HeadId,
      ProjectId: this.id,
    };
    this.projectService.putProjectWithAuth(this.postStaff1).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.projectService.deleteProjectStaffWithAuth(this.staff[index].StaffId).subscribe(
          (data) => {
            // tslint:disable-next-line: no-console
            console.log(data);
            this.projectService.postProjectStaffWithAuth(this.postStaff).subscribe(
              (res) => {
                // tslint:disable-next-line: no-console
                console.log(res);
                this.getStaffDataFunc();
              },
              error => {
                // tslint:disable-next-line: no-console
                console.error(error);
              });
          },
          error => {
            // tslint:disable-next-line: no-console
            console.error(error);
          });
      },
      error => {
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  getStaffDataFunc() {
    this.selectedItem = new FormControl();
    this.stemp = [];
    this.projectService.getAllProjectStaff().subscribe(
      data1 => {
        this.staffs = data1;
        this.projectService.getProjectWithAuth(this.id).subscribe(
          data => {
            // tslint:disable-next-line: no-console
            console.log(data);
            this.temp = data;
            this.staffs.splice(this.staffs.findIndex(x => x.StaffId === data.ProjectHead), 1);
            for (let i = 0; i < data.ProjectStaffs.length; i++) {
              this.staffs.splice(this.staffs.findIndex(x => x.StaffId === data.ProjectStaffs[i]), 1);
              this.staffService.getStaffWithAuth(data.ProjectStaffs[i], localStorage.getItem('companyId')).subscribe(
                res => {
                  this.stemp.push(res);
                  this.staff = this.stemp;
                });
            }
            this.staffService.getStaffWithAuth(data.ProjectHead, localStorage.getItem('companyId')).subscribe(
              response => {
                this.HeadId = response.StaffId;
                this.HeadName = response.StaffNameEn;
                this.HeadEmail = response.StaffEmail;
                this.HeadTel = response.StaffTel;
                this.HeadPic = 'http://206.189.34.171:7777/staff/img/' + response.StaffPicturePath;
                this.HeadSig = 'http://206.189.34.171:7777/staff/sig/' + response.StaffSignature;
              });
          }, error => {
            console.error(error);
          });
      },
      error => {
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  selectstaff(event) {
    this.tempid = event;
  }

  deleteStaff(index: number) {
    this.projectService.deleteProjectStaffWithAuth(this.staff[index].StaffId).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getStaffDataFunc();
      },
      error => {
        // tslint:disable-next-line: no-console
        console.error(error);
      });
    this.staff.splice(index, 1);
  }

}
