import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
import { ProjectService } from '../../project.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'ngx-payment-edit',
  templateUrl: './payment-edit.component.html',
  styleUrls: ['./payment-edit.component.scss'],
})
export class PaymentEditComponent implements OnInit {
  id: string;
  body: any;
  ProjectName: string;
  ProjectDesc: string;
  ProjectLink: string;
  ProjectStart: any;
  ProjectEnd: any;
  ProjectClient: string;
  ProjectHead: string;
  ProjectPayment: string;
  VatPrice: number;
  Total: number;
  SubTotal: number;

  projectForm: FormGroup;
  tabledata = [];

  setting = {
    actions: {
      add: true,
      edit: true,
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      No: {
        title: 'No.',
        type: 'string',
      },
      Description: {
        title: 'Description',
        type: 'string',
      },
      Quantity: {
        title: 'Quantity',
        type: 'number',
      },
      Unit: {
        title: 'Unit',
        type: 'String',
        addable: false,
        editable: false,
      },
      UnitPrice: {
        title: 'Unit Price/Baht',
        type: 'number',
      },
      Amount: {
        title: 'Amount',
        type: 'number',
        addable: false,
        editable: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(activeid: ActivatedRoute,
    private projectService: ProjectService,
    private route: Router) {
    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });
  }

  ngOnInit() {
    this.getDescription();
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        this.ProjectName = data.ProjectName;
        this.ProjectDesc = data.ProjectDesc;
        this.ProjectLink = data.ProjectLink;
        this.ProjectPayment = data.ProjectPayment;
        this.ProjectStart = new Date(data.ProjectStart * 1000);
        this.ProjectEnd = new Date(data.ProjectEnd * 1000);
        this.ProjectHead = data.ProjectHead;
        this.ProjectClient = data.ProjectClient;
      });
  }

  onCreateConfirm(event) {
    if (window.confirm('Are you sure you want to Add?')) {
      event.confirm.resolve(event.newData);
      this.body = {
        ProjectId: this.id,
        No: event.newData.No,
        Description: event.newData.Description,
        Quantity: Number(event.newData.Quantity),
        Unit: 'PCS',
        UnitPrice: Number(event.newData.UnitPrice),
      };
      this.projectService.postCostDescription(this.body).subscribe(
        res => {
          // tslint:disable-next-line: no-console
          console.log(res);
          this.getDescription();
        },
        error => {
          // tslint:disable-next-line: no-console
          console.error(error);
          this.getDescription();
        });
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event): void {
    if (window.confirm('Are you sure you want to Save?')) {
      event.confirm.resolve(event.newData);
      this.body = {
        ID: event.newData.ID,
        ProjectId: event.newData.Projectid,
        No: event.newData.No,
        Description: event.newData.Description,
        Quantity: Number(event.newData.Quantity),
        Unit: 'PCS',
        UnitPrice: Number(event.newData.UnitPrice),
      };
      this.projectService.putCostDescription(this.body).subscribe(
        res => {
          // tslint:disable-next-line: no-console
          console.log(res);
          this.getDescription();
        },
        error => {
          // tslint:disable-next-line: no-console
          console.error(error);
          this.getDescription();
        });
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.projectService.deleteCostDescription(event.data.ID).subscribe(
        res => {
          // tslint:disable-next-line: no-console
          console.log(res);
          this.getDescription();
        },
        error => {
          // tslint:disable-next-line: no-console
          console.error(error);
          this.getDescription();
        });
    } else {
      event.confirm.reject();
    }
  }

  getDescription() {
    this.projectService.getCostDescription(this.id).subscribe(
      data => {
        this.tabledata = data.Detail;
        this.VatPrice = data.VAT;
        this.Total = data.Total;
        this.SubTotal = data.SubTotal;
        this.source.load(this.tabledata);
      });
  }

  onEditPayment() {
    this.body = {
      ProjectId: this.id,
      ProjectName: this.ProjectName,
      ProjectDesc: this.ProjectDesc,
      ProjectLink: this.ProjectLink,
      ProjectPayment: this.ProjectPayment,
      ProjectStart: Math.round((this.ProjectStart).getTime() / 1000),
      ProjectEnd: Math.round((this.ProjectEnd).getTime() / 1000),
      ProjectHead: this.ProjectHead,
      ProjectClient: this.ProjectClient,
    };
    // tslint:disable-next-line: no-console
    console.log(this.body);
    this.projectService.putProjectWithAuth(this.body).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.route.navigateByUrl('/page/project/info/cost/' + this.id);
      },
      error => {
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  onCancel() {
    this.route.navigateByUrl('/page/project/info/cost/' + this.id);
  }
}
