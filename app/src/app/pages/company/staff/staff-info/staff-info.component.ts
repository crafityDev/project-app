import { Component, OnInit } from '@angular/core';
import { StaffService } from '../staff.service';
import { ActivatedRoute, Router, Params } from '@angular/router';


@Component({
  selector: 'ngx-staff-info',
  templateUrl: './staff-info.component.html',
  styleUrls: ['./staff-info.component.scss'],
})


export class StaffInfoComponent implements OnInit {

  Done = false;
  id: string;

  StaffId = '';
  StaffName = '';
  StaffNameEn = '';
  StaffEmail = '';
  StaffTel = '';
  StaffAddress = '';
  StaffStart: any;
  StaffResignation: any;
  PositionHistory = [];
  StaffPicturePath: any;
  StaffSignature: any;
  StaffDate = false;
  StaffPositionName = '';
  StaffPositionSalary = '';
  ShowPosition = true;

  constructor(
    private staffService: StaffService,
    activeid: ActivatedRoute,
    private router: Router) {

    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });
  }

  ngOnInit() {
    this.Done = true;
    this.staffService.getStaffWithAuth(this.id, localStorage.getItem('companyId')).subscribe(
      data => {
        this.Done = false;
        // tslint:disable-next-line: no-console
        console.log(data);
        this.StaffId = data.StaffId;
        this.StaffName = data.StaffName;
        this.StaffNameEn = data.StaffNameEn;
        this.StaffEmail = data.StaffEmail;
        this.StaffTel = data.StaffTel;
        this.StaffAddress = data.StaffAddress;
        this.StaffStart = data.StaffStart;
        this.StaffResignation = data.StaffResignation;
        this.PositionHistory = data.StaffPositionHistory;
        this.StaffPicturePath = 'http://206.189.34.171:7777/staff/img/' + data.StaffPicturePath;
        this.StaffSignature = 'http://206.189.34.171:7777/staff/sig/' + data.StaffSignature;
        this.StaffPositionName = data.StaffPositionName;
        this.StaffPositionSalary = data.StaffPositionSalary;
        if (this.PositionHistory.length === 0) {
          this.ShowPosition = false;
        }
        if (this.StaffResignation === 0) {
          this.StaffDate = true;
        } else {
          this.StaffDate = false;
        }
      },
      error => {
        this.Done = false;
        console.error(error);
      });
  }

  onEdit(Byid) {
    this.router.navigateByUrl('/page/company/staff-edit/' + Byid);
  }
}
