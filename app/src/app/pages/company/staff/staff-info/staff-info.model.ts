export class StaffData {
    StaffName: string;
    StaffEmail: string;
    StaffTel: string;
    StaffAddress: string;
    StaffStart: Date;
    StaffResignation: Date;
}
