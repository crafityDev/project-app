import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class StaffService {

    ngModelDate = new Date();

    constructor(private httpClient: HttpClient) { }

    // Normal API
    getAllStaff() {
        return this.httpClient.get<any>
            ('http://206.189.34.171:7777/mt/getallstaffdata');
    }

    getStaff(id) {
        return this.httpClient.get<any>
            ('http://206.189.34.171:7777/mt/getstaffdata/' + id);
    }

    putStaff(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.httpClient.put('http://206.189.34.171:7777/mt/putstaffdata'
            , body, {
            headers,
        });
    }

    postStaff(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.httpClient.post('http://206.189.34.171:7777/mt/poststaffdata', body, {
            headers,
        });
    }

    deleteStaff(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('body', body);
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletestaffdata', {
            headers: headers,
        });
    }

    // Auth API
    // getAllStaffWithAuth() {
    //     let headers = new HttpHeaders();
    //     headers = headers.append('Authorization', localStorage.getItem('key'));
    //     headers = headers.append('CompanyId', localStorage.getItem('companyId'));
    //     return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallstaffsortbyname', {
    //         headers: headers,
    //     });
    // }

    Sort(data) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getstaffsortSC/' + data, {
            headers: headers,
        });
    }

    // SortByNameZA() {
    //     let headers = new HttpHeaders();
    //     headers = headers.append('Authorization', localStorage.getItem('key'));
    //     headers = headers.append('CompanyId', localStorage.getItem('companyId'));
    //     return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallstaffsortbyza', {
    //         headers: headers,
    //     });
    // }

    // SortByStart() {
    //     let headers = new HttpHeaders();
    //     headers = headers.append('Authorization', localStorage.getItem('key'));
    //     headers = headers.append('CompanyId', localStorage.getItem('companyId'));
    //     return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallstaffsortbystartdate', {
    //         headers: headers,
    //     });
    // }

    getStaffWithAuth(id, Cid) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', Cid);
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getstaffdata/' + id, {
            headers: headers,
        });
    }

    putStaffWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/putstaffdata', body, {
            headers: headers,
        });
    }

    postStaffWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/poststaffdata', body, {
            headers: headers,
        });
    }

    deleteStaffWithAuth(data, Cid) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('StaffId', body);
        headers = headers.append('CompanyId', Cid);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletestaffdata', {
            headers: headers,
        });
    }

    postPositionWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/postpositiondata', body, {
            headers: headers,
        });
    }

    putPositionWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.put<any>('http://206.189.34.171:7777/mt/putpositiondata', body, {
            headers: headers,
        });
    }

    deletePositionWithAuth(StfId, PosId, Cid) {
        const StaffId = JSON.parse(JSON.stringify(StfId));
        const PositionId = JSON.parse(JSON.stringify(PosId));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('StaffId', StaffId);
        headers = headers.append('PositionId', PositionId);
        headers = headers.append('CompanyId', Cid);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletepositiondata', {
            headers: headers,
        });
    }

    putImgWithAuth(data, path) {
        const body = JSON.stringify(data);
        const formData: FormData = new FormData();
        formData.append('data', body);
        formData.append('path', path, path.name);
        formData.append('page', 'staff');
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // headers = headers.append('enctype', 'multipart/form-data');
        return this.httpClient.put('http://206.189.34.171:7777/mt/uploadPic', formData, {
            headers: headers,
        });
    }

    putSigWithAuth(data, path) {
        const body = JSON.stringify(data);
        const formData: FormData = new FormData();
        formData.append('data', body);
        formData.append('path', path, path.name);
        formData.append('page', 'staffsignature');
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // headers = headers.append('enctype', 'multipart/form-data');
        return this.httpClient.put('http://206.189.34.171:7777/mt/uploadPic', formData, {
            headers: headers,
        });
    }
}
