import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
// import { SmartTableData } from '../../../../@core/data/smart-table';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { StaffService } from '../staff.service';
import { NbToastrService } from '@nebular/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'ngx-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.scss'],
})
export class StaffListComponent implements OnInit {
  Done = false;
  isGridView = true;
  body: any;
  Bysort: any;
  inviteForm: FormGroup;
  staffs: any[] = [];
  filterstaffs: any[] = [];
  private _searchTerm: string;

  get searchTerm(): string {
    return this._searchTerm;
  }
  set searchTerm(value: string) {
    this._searchTerm = value;
    this.filterstaffs = this.filters(value);
  }

  filters(searchString: string) {
    return this.staffs.filter(s => s.StaffName.toLowerCase().indexOf(searchString.toLowerCase()) !== -1);
  }


  selectedSort = [
    { value: 'A-Z', Show: 'Staffs title A-Z' },
    { value: 'Z-A', Show: 'Staffs title Z-A' },
    { value: 'MStartDate', Show: 'Starting Date' },
    { value: 'LStartDate', Show: 'Starting Date' },
  ];

  setting = {
    mode: 'external',
    actions: {
      add: false,
      edit: true,
      delete: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      StaffNameEn: {
        title: 'Name',
        type: 'string',
      },
      StaffEmail: {
        title: 'E-mail',
        type: 'string',
      },
      StaffTel: {
        title: 'Telephone',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private staffService: StaffService,
    activeid: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private toastrService: NbToastrService) {
    activeid.params.subscribe((params: Params) => {
      this.Bysort = params['Bysort'];
      this.Done = true;
      this.getData();
    });

    this.inviteForm = new FormGroup({
      Email: new FormControl('', Validators.required),
    });
  }

  ngOnInit() { }

  goToInfo(Byid) {
    this.router.navigateByUrl('/page/company/staff-info/' + Byid);
  }

  TableToInfo(event) {
    this.router.navigateByUrl('/page/company/staff-info/' + event.data.StaffId);
  }

  onEdit(Byid) {
    this.router.navigateByUrl('/page/company/staff-edit/' + Byid);
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  onDelete(Byid) {
    this.Done = true;
    this.staffService.deleteStaffWithAuth(Byid, localStorage.getItem('companyId')).subscribe(
      (response) => {
        this.toastrService.success('', 'A staff has been removed!', {});
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getData();
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  onEditConfirm(event): void {
    this.router.navigateByUrl('/page/company/staff-edit/' + event.data.StaffId);
  }

  onDeleteConfirm(event): void {
    this.Done = true;
    this.staffService.deleteStaffWithAuth(event.data.StaffId, localStorage.getItem('companyId')).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.toastrService.success('', 'A staff has been removed!', {});
        this.getData();
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  getData() {
    this.staffService.Sort(this.Bysort).subscribe(
      data => {
        this.setData(data);
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  setData(data) {
    this.Done = false;
    this.staffs = data;
    this.filterstaffs = data;
    this.source.load(this.staffs);
    // tslint:disable-next-line: no-console
    console.log(this.staffs);
  }

  selectSort(event) {
    this.router.navigateByUrl('/page/company/staff-list/' + event);
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  inviteStaff() {
    this.body = {
      Email: this.inviteForm.get('Email').value,
    };
    this.staffService.postStaffWithAuth(this.body).subscribe(
      (response) => {
        this.toastrService.success('', 'A new staff has been added!', {});
        // tslint:disable-next-line: no-console
        console.log(response);
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  onCancelInvite() {
    this.modalService.dismissAll();
    this.inviteForm.reset();
  }

}
