import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NbDateService } from '@nebular/theme';
import { DatePipe } from '@angular/common';
import { LocalDataSource } from 'ng2-smart-table';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'ngx-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss'],
})
export class PositionComponent implements OnInit {

  @Output() change = new EventEmitter<any>();
  positionFormAdd: FormGroup;
  positionFormEdit: FormGroup;
  PositionData = [];
  Positiontemp = [];
  index: any;
  enddatecheck = false;

  setting = {
    mode: 'external',
    actions: {
      add: true,
      edit: true,
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      PositionName: {
        title: 'Position',
        type: 'string',
      },
      PositionSalary: {
        title: 'Salary',
        type: 'string',
      },
      PositionStart: {
        title: 'Start Date',
        type: 'string',
      },
      PositionEnd: {
        title: 'End Date',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  min: any;
  max: Date;

  constructor(
    private modalService: NgbModal,
    protected dateService: NbDateService<Date>,
    private datePipe: DatePipe,
  ) {
    // this.min = this.dateService.addDay(this.dateService.today(), 0);
    this.max = this.dateService.addDay(this.dateService.today(), 0);

    this.positionFormAdd = new FormGroup({
      PositionName: new FormControl('', Validators.required),
      PositionSalary: new FormControl('', Validators.required),
      PositionStart: new FormControl('', Validators.required),
      PositionEnd: new FormControl(''),
    });

    this.positionFormEdit = new FormGroup({
      PositionName: new FormControl('', Validators.required),
      PositionSalary: new FormControl('', Validators.required),
      PositionStart: new FormControl('', Validators.required),
      PositionEnd: new FormControl(''),
    });
  }

  ngOnInit() {
  }

  testlog() {
    // tslint:disable-next-line: no-console
    console.log(this.positionFormAdd);
  }

  addPosition() {
    if (this.positionFormAdd.get('PositionEnd').value === ''
      || this.positionFormAdd.get('PositionEnd').value === null) {
      this.PositionData.push({
        PositionName: this.positionFormAdd.value.PositionName,
        PositionStart: Math.round((this.positionFormAdd.get('PositionStart').value).getTime() / 1000),
        PositionEnd: null,
        PositionSalary: this.positionFormAdd.value.PositionSalary,
        StaffId: '',
      });
      this.Positiontemp.push({
        PositionName: this.positionFormAdd.value.PositionName,
        PositionStart: this.datePipe.transform(new Date((this.positionFormAdd.get('PositionStart').value)), 'MMM d, y'),
        PositionEnd: 'now',
        PositionSalary: this.positionFormAdd.value.PositionSalary,
      });
    } else {
      this.PositionData.push({
        PositionName: this.positionFormAdd.value.PositionName,
        PositionStart: Math.round((this.positionFormAdd.get('PositionStart').value).getTime() / 1000),
        PositionEnd: Math.round((this.positionFormAdd.get('PositionEnd').value).getTime() / 1000),
        PositionSalary: this.positionFormAdd.value.PositionSalary,
        StaffId: '',
      });
      this.Positiontemp.push({
        PositionName: this.positionFormAdd.value.PositionName,
        PositionStart: this.datePipe.transform(new Date((this.positionFormAdd.get('PositionStart').value)), 'MMM d, y'),
        PositionEnd: this.datePipe.transform(new Date((this.positionFormAdd.get('PositionEnd').value)), 'MMM d, y'),
        PositionSalary: this.positionFormAdd.value.PositionSalary,
      });
    }
    this.positionevent(this.PositionData);
    this.positionFormAdd.reset();
    this.modalService.dismissAll('');
    this.source.load(this.Positiontemp);
  }

  openVerticallyCentered(content) {
    this.positionFormAdd.reset();
    this.enddatecheck = false;
    this.modalService.open(content, { centered: true });
  }

  cancelPosition() {
    this.positionFormAdd.reset();
    this.positionFormEdit.reset();
    this.modalService.dismissAll('');
  }

  onEditConfirm(event, content): void {
    this.index = event.index;
    this.min = new Date(this.PositionData[event.index].PositionStart * 1000);
    if (this.PositionData[event.index].PositionEnd === 0 || this.PositionData[event.index].PositionEnd == null
      || this.enddatecheck === false) {
      this.positionFormEdit = new FormGroup({
        PositionName: new FormControl(this.PositionData[event.index].PositionName, Validators.required),
        PositionSalary: new FormControl(this.PositionData[event.index].PositionSalary, Validators.required),
        PositionStart: new FormControl(new Date(this.PositionData[event.index].PositionStart * 1000)
          , Validators.required),
        PositionEnd: new FormControl(new Date(this.dateService.addDay(this.dateService.today(), 0))),
      });
    } else {
      this.enddatecheck = true;
      this.positionFormEdit = new FormGroup({
        PositionName: new FormControl(this.PositionData[event.index].PositionName, Validators.required),
        PositionSalary: new FormControl(this.PositionData[event.index].PositionSalary, Validators.required),
        PositionStart: new FormControl(new Date(this.PositionData[event.index].PositionStart * 1000)
          , Validators.required),
        PositionEnd: new FormControl(new Date(this.PositionData[event.index].PositionEnd * 1000)),
      });
    }
    this.modalService.open(content, { centered: true });
    // if (window.confirm('Are you sure you want to Save?')) {
    //   event.confirm.resolve(event.newData);
    //   this.PositionData = {
    //     StaffId: this.id,
    //     PositionId: event.newData.PositionId,
    //     PositionName: event.newData.PositionName,
    //     PositionStart: event.newData.PositionStart,
    //     PositionEnd: event.newData.PositionEnd,
    //     PositionSalary: event.newData.PositionSalary,
    //   };
    //   this.staffService.putPositionWithAuth(this.PositionData).subscribe(
    //     data => {
    //       // tslint:disable-next-line: no-console
    //       console.log(data);
    //     },
    //     error => {
    //       console.error(error);
    //     });
    //   // tslint:disable-next-line: no-console
    //   console.log(this.PositionData);
    // } else {
    //   event.confirm.reject();
    // }
  }

  editPosition() {
    if (this.positionFormEdit.get('PositionEnd').value === '' ||
      this.positionFormEdit.get('PositionEnd').value === null || this.enddatecheck === false) {
      this.PositionData[this.index].PositionName = this.positionFormEdit.value.PositionName;
      this.PositionData[this.index].PositionStart =
        Math.round((this.positionFormEdit.get('PositionStart').value).getTime() / 1000);
      this.PositionData[this.index].PositionEnd = null,
        this.PositionData[this.index].PositionSalary = this.positionFormEdit.value.PositionSalary,
        this.PositionData[this.index].StaffId = '',
        this.Positiontemp[this.index].PositionName = this.positionFormEdit.value.PositionName;
      this.Positiontemp[this.index].PositionStart =
        this.datePipe.transform(new Date((this.positionFormEdit.get('PositionStart').value)), 'MMM d, y');
      this.Positiontemp[this.index].PositionEnd = 'now';
      this.Positiontemp[this.index].PositionSalary = this.positionFormEdit.value.PositionSalary;
    } else {
      this.PositionData[this.index].PositionName = this.positionFormEdit.value.PositionName;
      this.PositionData[this.index].PositionStart =
        Math.round((this.positionFormEdit.get('PositionStart').value).getTime() / 1000);
      this.PositionData[this.index].PositionEnd =
        Math.round((this.positionFormEdit.get('PositionEnd').value).getTime() / 1000);
      this.PositionData[this.index].PositionSalary = this.positionFormEdit.value.PositionSalary,
        this.PositionData[this.index].StaffId = '',
        this.Positiontemp[this.index].PositionName = this.positionFormEdit.value.PositionName;
      this.Positiontemp[this.index].PositionStart =
        this.datePipe.transform(new Date((this.positionFormEdit.get('PositionStart').value)), 'MMM d, y');
      this.Positiontemp[this.index].PositionEnd =
        this.datePipe.transform(new Date((this.positionFormEdit.get('PositionEnd').value)), 'MMM d, y');
      this.Positiontemp[this.index].PositionSalary = this.positionFormEdit.value.PositionSalary;
    }
    this.positionevent(this.PositionData);
    this.positionFormEdit.reset();
    this.modalService.dismissAll('');
    this.source.load(this.Positiontemp);
  }

  onDeleteConfirm(event): void {
    this.PositionData.splice(event.index, 1);
    this.Positiontemp.splice(event.index, 1);
    this.positionevent(this.PositionData);
    this.source.load(this.Positiontemp);
  }

  positionevent(value) {
    // tslint:disable-next-line: no-console
    console.log(value);
    this.change.emit(value);
  }

  startDatePick(event) {
    this.min = event;
  }

  toggleenddate() {
    this.enddatecheck = !this.enddatecheck;
  }
}
