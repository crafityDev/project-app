import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StaffService } from '../staff.service';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { LocalDataSource } from 'ng2-smart-table';
import { NbToastrService, NbDateService } from '@nebular/theme';
// import { toJSDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-calendar';

@Component({
  selector: 'ngx-staff-add',
  templateUrl: './staff-add.component.html',
  styleUrls: ['./staff-add.component.scss'],
})
export class StaffAddComponent implements OnInit {
  Done = false;
  closeResult: string;
  staffForm: FormGroup;
  positionForm: FormGroup;
  PositionData = [];
  body: any;
  body1: any;
  resigned = false;

  StaffPicturePath: any;
  StaffSignature: any;
  bodypic: any;
  bodysig: any;
  selectedFile = [];

  setting = {
    mode: 'external',
    actions: {
      add: true,
      edit: false,
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      PositionName: {
        title: 'Position',
        type: 'string',
      },
      PositionSalary: {
        title: 'Salary',
        type: 'string',
      },
      PositionStart: {
        title: 'Start Date',
        type: 'string',
      },
      PositionEnd: {
        title: 'End Date',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  min: Date;
  max: Date;

  constructor(private staffService: StaffService,
    private router: Router,
    private modalService: NgbModal,
    private toastrService: NbToastrService,
    protected dateService: NbDateService<Date>) {

    this.min = this.dateService.addDay(this.dateService.today(), 0);
    this.max = this.dateService.addDay(this.dateService.today(), 0);

    this.staffForm = new FormGroup({
      StaffName: new FormControl('', Validators.required),
      StaffNameEn: new FormControl('', Validators.required),
      StaffEmail: new FormControl('', [Validators.required, Validators.email]),
      StaffTel: new FormControl('', Validators.required),
      StaffAddress: new FormControl('', Validators.required),
      StaffPositionName: new FormControl('', Validators.required),
      StaffPositionSalary: new FormControl('', Validators.required),
      StaffStart: new FormControl('', Validators.required),
      StaffResignation: new FormControl(''),
    });
  }

  ngOnInit() {
    this.staffForm.controls['StaffResignation'].disable();
    this.StaffPicturePath = 'http://206.189.34.171:7777/default/img/dummy.png';
    this.StaffSignature = 'http://206.189.34.171:7777/default/img/gray.png';
    this.getBlob();
    this.getBlob1();
  }

  startDatePick(event) {
    this.min = event;
  }

  onAddStaff() {
    this.Done = true;
    if (this.resigned) {
      this.body = {
        CompanyId: localStorage.getItem('companyId'),
        StaffName: this.staffForm.get('StaffName').value,
        StaffNameEn: this.staffForm.get('StaffNameEn').value,
        StaffEmail: this.staffForm.get('StaffEmail').value,
        StaffTel: this.staffForm.get('StaffTel').value,
        StaffAddress: this.staffForm.get('StaffAddress').value,
        StaffPositionName: this.staffForm.get('StaffPositionName').value,
        StaffPositionSalary: this.staffForm.get('StaffPositionSalary').value,
        StaffStart: Math.round((this.staffForm.get('StaffStart').value).getTime() / 1000),
        StaffResignation: Math.round((this.staffForm.get('StaffResignation').value).getTime() / 1000),
      };
      this.postStaff();
    } else {
      this.body = {
        CompanyId: localStorage.getItem('companyId'),
        StaffName: this.staffForm.get('StaffName').value,
        StaffNameEn: this.staffForm.get('StaffNameEn').value,
        StaffEmail: this.staffForm.get('StaffEmail').value,
        StaffTel: this.staffForm.get('StaffTel').value,
        StaffAddress: this.staffForm.get('StaffAddress').value,
        StaffPositionName: this.staffForm.get('StaffPositionName').value,
        StaffPositionSalary: this.staffForm.get('StaffPositionSalary').value,
        StaffStart: Math.round((this.staffForm.get('StaffStart').value).getTime() / 1000),
        StaffResignation: null,
      };
      this.postStaff();
    }
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  onFileSelectedPic(event) {
    this.selectedFile[0] = event.target.files[0];
    const reader = new FileReader();
    // this.selectedFile = event;
    reader.readAsDataURL(this.selectedFile[0]);
    reader.onload = (_event) => {
      this.StaffPicturePath = reader.result;
    };
  }

  onFileSelectedSig(event) {
    this.selectedFile[1] = event.target.files[0];
    const reader = new FileReader();
    // this.selectedFile = event;
    reader.readAsDataURL(this.selectedFile[1]);
    reader.onload = (_event) => {
      this.StaffSignature = reader.result;
    };
  }

  onUploadPic() {
    this.staffService.putImgWithAuth(this.bodypic, this.selectedFile[0]).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.staffService.putSigWithAuth(this.bodysig, this.selectedFile[1]).subscribe(
          res => {
            // tslint:disable-next-line: no-console
            console.log(res);
            this.Done = false;
            this.goToInfo(this.bodysig.StaffId);
          },
          error => {
            this.Done = false;
            this.toastrService.danger('Cannot upload this signature.', {});
            console.error(error);
          });
      },
      error => {
        this.Done = false;
        this.toastrService.danger('Cannot upload this picture.', {});
        console.error(error);
      });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  toggleResign() {
    this.resigned = !this.resigned;
    if (!this.resigned) {
      this.staffForm.controls['StaffResignation'].disable();
      this.staffForm.controls['StaffResignation'].clearValidators();
      this.staffForm.controls['StaffResignation'].updateValueAndValidity();
    } else {
      this.staffForm.controls['StaffResignation'].enable();
      this.staffForm.controls['StaffResignation'].setValidators([Validators.required]);
      this.staffForm.controls['StaffResignation'].updateValueAndValidity();
    }
  }

  onCancel() {
    this.router.navigateByUrl('/page/company/staff-list/A-Z');
    this.staffForm.reset();
  }

  goToInfo(Byid) {
    this.router.navigateByUrl('/page/company/staff-info/' + Byid);
  }

  getBlob() {
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[0] = this.blobToFile(xhr.response
          , this.StaffPicturePath.split('/')[this.StaffPicturePath.split('/').length - 1]);
      } else {
        this.selectedFile = [];
      }
    };
    xhr.open('GET', this.StaffPicturePath, true);
    xhr.send();
  }

  getBlob1() {
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[1] = this.blobToFile(xhr.response
          , this.StaffSignature.split('/')[this.StaffSignature.split('/').length - 1]);
      } else {
        this.selectedFile = [];
      }
    };
    xhr.open('GET', this.StaffSignature, true);
    xhr.send();
  }

  blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;
    // A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    // Cast to a File() type
    return <File>theBlob;
  }

  postStaff() {
    this.staffService.postStaffWithAuth(this.body).subscribe(
      (response) => {
        this.toastrService.success('', 'A new staff has been added!', {});
        // tslint:disable-next-line: no-console
        console.log(response);
        this.bodypic = {
          type: this.selectedFile[0].name.split('.')[1],
          name: this.selectedFile[0].name.split('.')[0],
          StaffId: response.StaffId,
        };
        this.bodysig = {
          type: this.selectedFile[1].name.split('.')[1],
          name: this.selectedFile[1].name.split('.')[0],
          StaffId: response.StaffId,
        };
        this.onUploadPic();
        for (let i = 0; i < this.PositionData.length; i++) {
          this.PositionData[i].StaffId = response.StaffId;
          this.staffService.postPositionWithAuth(this.PositionData[i]).subscribe(
            res => {
              // tslint:disable-next-line: no-console
              console.log(res);
            },
            error => {
              this.toastrService.danger(error.message, 'Position went wrong!', {});
              console.error(error);
            });
        }
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  getpositiondata(event) {
    this.PositionData = event;
  }
}
