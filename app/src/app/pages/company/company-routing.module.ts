import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyComponent } from './company.component';
import { CompanyInfoComponent } from './company-info/company-info.component';
import { CompanyEditComponent } from './company-edit/company-edit.component';
import { BankAccountComponent } from './bank-account/bank-account.component';
import { StaffListComponent } from './staff/staff-list/staff-list.component';
import { StaffEditComponent } from './staff/staff-edit/staff-edit.component';
import { StaffInfoComponent } from './staff/staff-info/staff-info.component';
import { StaffAddComponent } from './staff/staff-add/staff-add.component';

const routes: Routes = [{
    path: '',
    component: CompanyComponent,
    children: [{
        path: 'company-info',
        component: CompanyInfoComponent,
    }, {
        path: 'bank-account',
        component: BankAccountComponent,
    }, {
        path: 'staff-list/:Bysort',
        component: StaffListComponent,
    }, {
        path: 'staff-info/:Byid',
        component: StaffInfoComponent,
    }, {
        path: 'staff-edit/:Byid',
        component: StaffEditComponent,
    }, {
        path: 'staff-add',
        component: StaffAddComponent,
    }, {
        path: 'edit',
        component: CompanyEditComponent,
    }, {
        path: '',
        redirectTo: 'company-info',
        pathMatch: 'full',
    }],
}];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CompanyRoutingModule { }

export const companyPages = [
    CompanyComponent,
    CompanyInfoComponent,
    CompanyEditComponent,
    BankAccountComponent,
    StaffListComponent,
    StaffInfoComponent,
    StaffEditComponent,
    StaffAddComponent,
];
