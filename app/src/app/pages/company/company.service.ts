import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable()
export class CompanyService {

    constructor(private httpClient: HttpClient) { }

    isCompanyAdd = new Subject<boolean>();

    // Normal API
    getCompany(id) {
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getcompanydata/' + id);
    }

    putCompany(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.httpClient.put('http://206.189.34.171:7777/mt/putonecompany'
            , body, {
            headers,
        });
    }

    postCompany(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.httpClient.post('http://206.189.34.171:7777/mt/postcompanydata', body, {
            headers,
        });
    }

    // Auth API
    getCompanyWithAuth() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getcompanydata', {
            headers: headers,
        });
    }

    postCompanyWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/postcompanydata', body, {
            headers: headers,
        });
    }

    putCompanyWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/putcompanydata', body, {
            headers: headers,
        });
    }

    deleteCompanyWithAuth() {
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletecompanydata', {
            headers: headers,
        });
    }

    putBankWithAuthh(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log(headers);
        return this.httpClient.put('http://206.189.34.171:7777/mt/putbankdata', body, {
            headers: headers,
        });
    }

    postBankWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.post<any>('http://206.189.34.171:7777/mt/postbankdata', body, {
            headers: headers,
        });
    }

    getBankWithAuth() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getallbankdata', {
            headers: headers,
        });
    }

    putBankWithAuth(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.put<any>('http://206.189.34.171:7777/mt/putbankdata', body, {
            headers: headers,
        });
    }

    deleteBankWithAuth(BId) {
        const BankId = JSON.parse(JSON.stringify(BId));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('BankId', BankId);
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('CompanyId', localStorage.getItem('companyId'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deletebankdata', {
            headers: headers,
        });
    }

    putImgWithAuth(data, path) {
        const body = JSON.stringify(data);
        const formData: FormData = new FormData();
        formData.append('data', body);
        formData.append('path', path, path.name);
        formData.append('page', 'company');
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // headers = headers.append('enctype', 'multipart/form-data');
        return this.httpClient.put('http://206.189.34.171:7777/mt/uploadPic', formData, {
            headers: headers,
        });
    }

    updateUserCompany(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.put<any>('http://206.189.34.171:7777/mt/updateusercompany', body, {
            headers: headers,
        });
    }

    updateUserCompanyBypair(data) {
        const body = JSON.parse(JSON.stringify(data));
        // console.log("body", body);
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        // console.log("headers", headers);
        return this.httpClient.put<any>('http://206.189.34.171:7777/mt/updateuserbycompanypair', body, {
            headers: headers,
        });
    }
}
