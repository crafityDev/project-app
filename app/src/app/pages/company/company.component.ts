import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'ngx-company',
  templateUrl: './company.component.html',
})
export class CompanyComponent implements OnInit {

  items: NbMenuItem[] = [
    {
      title: 'Company-info',
      link: '/page/company/company-info',
    },
    {
      title: 'Staffs',
      link: '/page/company/staff-list/A-Z',
    },
    {
      title: 'Bank Account',
      link: '/page/company/bank-account',
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
