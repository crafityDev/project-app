import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.scss'],
})
export class CompanyEditComponent implements OnInit {

  companyForm: FormGroup;
  Done = false;
  id: string;
  body: any;

  CompanyPicturePath: any;
  bodypic: any;
  selectedFile = [];

  constructor(
    private companyService: CompanyService,
    private router: Router,
    private toastrService: NbToastrService) {

    this.companyForm = new FormGroup({
      CompanyName: new FormControl('', Validators.required),
      CompanyNameEn: new FormControl('', Validators.required),
      CompanyEmail: new FormControl('', [Validators.required, Validators.email]),
      CompanyTel: new FormControl('', Validators.required),
      CompanyAddress: new FormControl('', Validators.required),
      CompanyTaxId: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    this.Done = true;
    this.companyService.getCompanyWithAuth().subscribe(
      data => {
        this.Done = false;
        // tslint:disable-next-line: no-console
        console.log(data);
        this.CompanyPicturePath = 'http://206.189.34.171:7777/company/img/' + data.CompanyPicturePath;
        this.getBlob();
        this.companyForm = new FormGroup({
          CompanyName: new FormControl(data.CompanyName, Validators.required),
          CompanyNameEn: new FormControl(data.CompanyNameEn, Validators.required),
          CompanyEmail: new FormControl(data.CompanyEmail, [Validators.required, Validators.email]),
          CompanyTel: new FormControl(data.CompanyTel, Validators.required),
          CompanyAddress: new FormControl(data.CompanyAddress, Validators.required),
          CompanyTaxId: new FormControl(data.CompanyTaxId, Validators.required),
        });
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        this.Done = false;
        console.error(error);
      });
  }

  onSaveCompany() {
    this.Done = true;
    this.body = {
      CompanyId: localStorage.getItem('companyId'),
      CompanyName: this.companyForm.get('CompanyName').value,
      CompanyNameEn: this.companyForm.get('CompanyNameEn').value,
      CompanyEmail: this.companyForm.get('CompanyEmail').value,
      CompanyTel: this.companyForm.get('CompanyTel').value,
      CompanyAddress: this.companyForm.get('CompanyAddress').value,
      CompanyTaxId: this.companyForm.get('CompanyTaxId').value,
    };
    this.companyService.putCompanyWithAuth(this.body).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.onUploadPic();
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  onDeleteCompany() {
    this.Done = true;
    this.companyService.deleteCompanyWithAuth().subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.router.navigateByUrl('/auth/profile');
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  onFileSelected(event) {
    this.selectedFile[0] = event.target.files[0];
    const reader = new FileReader();
    // this.selectedFile = event;
    reader.readAsDataURL(this.selectedFile[0]);
    reader.onload = (_event) => {
      this.CompanyPicturePath = reader.result;
    };
  }

  onUploadPic() {
    this.bodypic = {
      type: this.selectedFile[0].name.split('.')[1],
      name: this.selectedFile[0].name.split('.')[0],
      ClientId: this.id,
      CompanyId: localStorage.getItem('companyId'),
    };
    this.companyService.putImgWithAuth(this.bodypic, this.selectedFile[0]).subscribe(
      data => {
        this.toastrService.success('', 'Edit success!', {});
        this.Done = false;
        // tslint:disable-next-line: no-console
        console.log('upload' + data);
        this.router.navigateByUrl('/page/company/company-info');
      },
      error => {
        this.Done = false;
        this.toastrService.danger('Cannot upload this picture.', {});
        console.error(error);
      });
  }

  getBlob() {
    this.Done = true;
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[0] = this.blobToFile(xhr.response
          , this.CompanyPicturePath.split('/')[this.CompanyPicturePath.split('/').length - 1]);
        this.Done = false;
      } else {
        this.Done = false;
        this.selectedFile = [];
      }
    };
    xhr.open('GET', this.CompanyPicturePath, true);
    xhr.send();
  }

  blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;
    // A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    // Cast to a File() type
    return <File>theBlob;
  }
}

