export class CompanyData {
    id: any;
    CompanyName: string;
    CompanyEmail: string;
    CompanyTel: string;
    CompanyAddress: string;
    CompanyTaxId: string;
}
