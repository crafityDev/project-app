import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { CompanyRoutingModule, companyPages } from './company-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { PositionComponent } from './staff/staff-add/position/position.component';

@NgModule({
    imports: [
        ThemeModule,
        CompanyRoutingModule,
        Ng2SmartTableModule,
        NgxLoadingModule.forRoot({
            animationType: ngxLoadingAnimationTypes.circleSwish,
        }),
    ],
    declarations: [companyPages, PositionComponent],
})
export class CompanyModule { }
