import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CompanyService } from '../company.service';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-company-add',
  templateUrl: './company-add.component.html',
  styleUrls: ['./company-add.component.scss'],
})
export class CompanyAddComponent implements OnInit, OnDestroy {

  userForm: FormGroup;
  companyForm: FormGroup;
  profileForm: FormGroup;
  Done = false;
  body: any;
  CompanyPicturePath: any;
  bodypic: any;
  selectedFile = [];

  constructor(
    private companyService: CompanyService,
    // private accountService: AccountService,
    private router: Router,
    private toastrService: NbToastrService) {

    this.companyForm = new FormGroup({
      CompanyName: new FormControl('', Validators.required),
      CompanyNameEn: new FormControl('', Validators.required),
      CompanyEmail: new FormControl('', [Validators.required, Validators.email]),
      CompanyTel: new FormControl('', Validators.required),
      CompanyAddress: new FormControl('', Validators.required),
      CompanyTaxId: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    this.companyService.isCompanyAdd.next(true);
    this.CompanyPicturePath = 'http://206.189.34.171:7777/default/img/gray.png';
    this.getBlob();
  }

  ngOnDestroy(): void {
    this.companyService.isCompanyAdd.next(false);
  }

  onSaveCompany() {
    this.Done = true;
    this.body = {
      CompanyName: this.companyForm.get('CompanyName').value,
      CompanyNameEn: this.companyForm.get('CompanyNameEn').value,
      CompanyEmail: this.companyForm.get('CompanyEmail').value,
      CompanyTel: this.companyForm.get('CompanyTel').value,
      CompanyAddress: this.companyForm.get('CompanyAddress').value,
      CompanyTaxId: this.companyForm.get('CompanyTaxId').value,
    };
    this.companyService.postCompanyWithAuth(this.body).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        localStorage.setItem('companyId', response.CompanyId);
        this.onUploadPic();
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      },
    );
  }

  onFileSelected(event) {
    this.selectedFile[0] = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(this.selectedFile[0]);
    reader.onload = (_event) => {
      this.CompanyPicturePath = reader.result;
    };
  }

  onUploadPic() {
    this.body = {
      CompanyId: localStorage.getItem('companyId'),
      email: localStorage.getItem('email'),
    };
    this.bodypic = {
      type: this.selectedFile[0].name.split('.')[1],
      name: this.selectedFile[0].name.split('.')[0],
      CompanyId: localStorage.getItem('companyId'),
    };
    this.companyService.updateUserCompany(this.body).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        this.companyService.putImgWithAuth(this.bodypic, this.selectedFile[0]).subscribe(
          data => {
            // tslint:disable-next-line: no-console
            console.log('upload' + data);
            this.router.navigateByUrl('/page/company/company-info');
          },
          error => {
            this.toastrService.danger('Please try again in a few minutes.', 'Cannot upload this picture.', {});
            console.error(error);
          });
      },
      error => {
        this.toastrService.danger(error.message, 'Please select company picture', {});
        console.error(error);
      });
  }

  getBlob() {
    this.Done = true;
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[0] = this.blobToFile(xhr.response
          , this.CompanyPicturePath.split('/')[this.CompanyPicturePath.split('/').length - 1]);
        this.Done = false;
      } else {
        this.Done = false;
        this.selectedFile = [];
      }
    };
    xhr.open('GET', this.CompanyPicturePath, true);
    xhr.send();
  }

  blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;
    // A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    // Cast to a File() type
    return <File>theBlob;
  }
}
