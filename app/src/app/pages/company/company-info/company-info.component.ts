import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';
import { FormGroup } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
// import { CompanyData } from '../company-info.model';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-company-info',
  templateUrl: './company-info.component.html',
  styleUrls: ['./company-info.component.scss'],
})

export class CompanyInfoComponent implements OnInit {

  companyForm: FormGroup;
  check = false;
  Done = false;

  CompanyName = '';
  CompanyNameEn = '';
  CompanyEmail = '';
  CompanyTel = '';
  CompanyAddress = '';
  CompanyTaxId = '';
  CompanyPicturePath = '';

  setting = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      id: {
        title: 'Logo',
        type: 'number',
      },
      firstName: {
        title: 'Bank',
        type: 'string',
      },
      lastName: {
        title: 'Branch',
        type: 'string',
      },
      username: {
        title: 'Account Name',
        type: 'string',
      },
      age: {
        title: 'Account Number',
        type: 'number',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData,
    private router: Router,
    private companyService: CompanyService) {

    const data = this.service.getData();
    this.source.load(data);
  }

  ngOnInit() {
    this.Done = true;
    this.companyService.getCompanyWithAuth().subscribe(
      data => {
        this.Done = false;
        // tslint:disable-next-line: no-console
        console.log(data);
        this.CompanyName = data.CompanyName;
        this.CompanyNameEn = data.CompanyNameEn;
        this.CompanyEmail = data.CompanyEmail;
        this.CompanyTel = data.CompanyTel;
        this.CompanyAddress = data.CompanyAddress;
        this.CompanyTaxId = data.CompanyTaxId;
        this.CompanyPicturePath = 'http://206.189.34.171:7777/company/img/' + data.CompanyPicturePath;
      }, error => {
        this.Done = false;
        console.error(error);
      });

  }

  onEdit() {
    this.router.navigateByUrl('/page/company/edit');
  }
}
