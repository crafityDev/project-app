import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { BankService } from '../../shared/bank.service';
import { CompanyService } from '../company.service';
import { LocalDataSource } from 'ng2-smart-table';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-bank-account',
  templateUrl: './bank-account.component.html',
  styleUrls: ['./bank-account.component.scss'],
})
export class BankAccountComponent implements OnInit {

  banks = this.bankService.getBankData();
  Done = false;
  EbranchName: string;
  EaccName: string;
  EaccNum: string;
  selectedItem: any;

  @ViewChild('bankForm') bankForm: NgForm;
  @ViewChild('bankFormEdit') bankFormEdit: NgForm;
  bankFormValid = false;

  setting = {
    mode: 'external',
    actions: {
      add: true,
      edit: true,
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      logo: {
        title: 'Logo',
        type: 'html',
        valuePrepareFunction: (logo: string) => {
          return '<img src="' + logo + '" class="mr-2 box-16">';
        },
        filter: false,
      },
      Bank: {
        title: 'Bank',
        type: 'string',
      },
      Branch: {
        title: 'Branch',
        type: 'string',
      },
      AccountName: {
        title: 'Account Name',
        type: 'string',
      },
      AccountNumber: {
        title: 'Account Number',
        type: 'string',
      },
    },
  };

  // TypeAhead
  model: any;
  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.banks.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10)),
    )
  formatter = (x: { name: string }) => x.name;

  bankAccounts: any[] = [];
  body: any;
  BankIdtemp: string;

  source: LocalDataSource = new LocalDataSource();

  constructor(private companyService: CompanyService,
    private modalService: NgbModal,
    private bankService: BankService,
    private toastrService: NbToastrService) { }

  ngOnInit() {
    this.Done = true;
    this.getBankDataFunc();
  }

  selectdata(event) {
    this.selectedItem = event;
  }

  addBank() {
    this.Done = true;
    // post data
    this.body = {
      Bank: this.selectedItem,
      Branch: this.bankForm.value.branchName,
      AccountName: this.bankForm.value.accName,
      AccountNumber: this.bankForm.value.accNum,
      CompanyId: localStorage.getItem('companyId'),
    };

    this.companyService.postBankWithAuth(this.body).subscribe(
      response => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.toastrService.success('', 'Bank account has been created!', {});
        this.bankFormValid = true;
        this.bankForm.reset();
        this.modalService.dismissAll('');
        this.getBankDataFunc();
      },
      error => {
        this.Done = false;
        this.getBankDataFunc();
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  editBank() {
    this.Done = true;
    this.body = {
      Bank: this.selectedItem,
      Branch: this.bankFormEdit.value.ebranchName,
      AccountName: this.bankFormEdit.value.eaccName,
      AccountNumber: this.bankFormEdit.value.eaccNum,
      CompanyId: localStorage.getItem('companyId'),
      BankId: this.BankIdtemp,
    };

    this.companyService.putBankWithAuth(this.body).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.toastrService.success('', 'Edit account success!', {});
        this.bankFormValid = true;
        this.bankFormEdit.reset();
        this.modalService.dismissAll('');
        this.getBankDataFunc();
      },
      error => {
        this.Done = false;
        this.getBankDataFunc();
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  onEditConfirm(event, content): void {
    this.BankIdtemp = event.data.BankId;
    this.selectedItem = event.data.Bank;
    // this.model = event.data.Bank;
    this.EbranchName = event.data.Branch;
    this.EaccName = event.data.AccountName;
    this.EaccNum = event.data.AccountNumber;
    this.modalService.open(content, { centered: true });
  }

  onDeleteConfirm(event): void {
    this.Done = true;
    this.companyService.deleteBankWithAuth(event.data.BankId).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.toastrService.success('', 'An account has been removed!', {});
        this.getBankDataFunc();
      },
      error => {
        this.getBankDataFunc();
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  getBankDataFunc() {
    this.companyService.getBankWithAuth().subscribe(
      data => {
        this.bankAccounts = data.CompanyBankAccount;
        for (let i = 0; i < this.bankAccounts.length; i++) {
          this.bankAccounts[i].logo = '../../../../assets/images/banks/'
            + this.bankService.getBankLogo(this.bankAccounts[i].Bank);
          // tslint:disable-next-line: no-console
          console.log(this.bankAccounts[i].logo);
        }
        this.source.load(this.bankAccounts);
        this.Done = false;
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        this.Done = false;
        console.error(error);
      });
  }

  cancelBank() {
    this.bankForm.reset();
    this.bankFormEdit.reset();
    this.modalService.dismissAll('');
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

}
