export interface Bank {
  name: string;
  logo: string;
}

export abstract class BankData {
  abstract getBankData(): Bank[];
}
