import { Injectable } from '@angular/core';
import { Bank, BankData } from './bank';

@Injectable()
export class BankService extends BankData {
  Logo: any;
  temp: any;
  private bankData: Bank[] = [
    { 'name': 'ธนาคารกสิกรไทย', 'logo': 'kbank.png' },
    { 'name': 'ธนาคารกรุงเทพ', 'logo': 'bkk.png' },
    { 'name': 'ธนาคารกรุงไทย', 'logo': 'ktb.png' },
    { 'name': 'ธนาคารทหารไทย', 'logo': 'tmb.png' },
    { 'name': 'ธนาคารไทยพาณิชย์', 'logo': 'scb.png' },
    { 'name': 'ธนาคารกรุงศรีอยุธยา', 'logo': 'krungsri.png' },
    { 'name': 'ธนาคารอิสลามแห่งประเทศไทย', 'logo': 'isbt.png' },
    { 'name': 'ธนาคารซีไอเอ็มบีไทย', 'logo': 'cimb.png' },
    { 'name': 'ธนาคารอาคารสงเคราะห์', 'logo': 'ghb.png' },
    { 'name': 'ธนาคารออมสิน', 'logo': 'gsb.png' },
    { 'name': 'ธนาคารธนชาต', 'logo': 'tbank.png' },
    { 'name': 'ธนาคารยูโอบี', 'logo': 'uob.png' },
  ];

  constructor() {
    super();
  }

  getBankData(): Bank[] {
    return this.bankData;
  }
  getBankLogo(data) {
    this.Logo = this.bankData.filter(bank => bank.name === data);
    return this.Logo[0].logo;
  }
}
