import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { NbToastrService } from '@nebular/theme';
import { StaffService } from '../../company/staff/staff.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  Email: string;
  UserName: string;
  UserPicturePath: any;
  ShowPosition = true;
  Done = false;
  id = 'myId';

  StaffId = '';
  StaffName = '';
  StaffNameEn = '';
  StaffEmail = '';
  StaffTel = '';
  StaffAddress = '';
  StaffStart: any;
  StaffResignation: any;
  PositionHistory = [];
  StaffPicturePath: any;
  StaffSignature: any;
  StaffDate = false;
  StaffPositionName = '';
  StaffPositionSalary = '';
  body: any;

  constructor(private accountService: AccountService,
    private toastrService: NbToastrService,
    private staffService: StaffService,
    activeid: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.Done = true;
    this.body = {

    };
    this.accountService.getUser().subscribe(
      (res) => {
        // tslint:disable-next-line: no-console
        console.log(res);
        this.Email = res.Email;
        this.UserName = res.UserName;
        this.UserPicturePath = 'http://206.189.34.171:7777/user/img/' + res.UserPicturePath;
        this.staffService.getStaffWithAuth(this.id, localStorage.getItem('companyId')).subscribe(
          data => {
            this.Done = false;
            // tslint:disable-next-line: no-console
            console.log(data);
            this.StaffId = data.StaffId;
            this.StaffName = data.StaffName;
            this.StaffNameEn = data.StaffNameEn;
            this.StaffEmail = data.StaffEmail;
            this.StaffTel = data.StaffTel;
            this.StaffAddress = data.StaffAddress;
            this.StaffStart = data.StaffStart;
            this.StaffResignation = data.StaffResignation;
            this.PositionHistory = data.StaffPositionHistory;
            this.StaffPicturePath = 'http://206.189.34.171:7777/staff/img/' + data.StaffPicturePath;
            this.StaffSignature = 'http://206.189.34.171:7777/staff/sig/' + data.StaffSignature;
            this.StaffPositionName = data.StaffPositionName;
            if (this.PositionHistory.length === 0) {
              this.ShowPosition = false;
            }
            this.StaffPositionSalary = data.StaffPositionSalary;
            if (this.StaffResignation === 0) {
              this.StaffDate = true;
            } else {
              this.StaffDate = false;
            }
          },
          error => {
            this.Done = false;
            console.error(error);
          });
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  onEdit(Byid) {
    this.router.navigateByUrl('/page/account/staff-edit/' + Byid);
  }

}
