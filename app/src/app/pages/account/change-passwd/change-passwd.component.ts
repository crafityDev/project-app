import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'ngx-change-passwd',
  templateUrl: './change-passwd.component.html',
  styleUrls: ['./change-passwd.component.scss'],
})
export class ChangePasswdComponent implements OnInit {
  body: any;
  changeForm: FormGroup;
  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastrService: NbToastrService,
  ) {
    this.changeForm = this.formBuilder.group({
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      newpassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl(''),
    }, { validator: this.pwdMatchValidator });
  }

  ngOnInit() {
  }

  changepass() {
    this.body = {
      CurrentPassword: this.changeForm.get('password').value,
      NewPassword: this.changeForm.get('newpassword').value,
    };
    // tslint:disable-next-line: no-console
    console.log(this.body);
    this.authService.changePwd(this.body).subscribe(
      (response) => {
        this.toastrService.success('', 'Edit success!', {});
        // tslint:disable-next-line: no-console
        console.log('Save' + response);
        this.router.navigateByUrl('/page/account/profile');
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  pwdMatchValidator(frm: FormGroup) {
    return frm.get('newpassword').value === frm.get('confirmPassword').value
      ? null : { 'mismatch': true };
  }
}
