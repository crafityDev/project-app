import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalDataSource } from 'ng2-smart-table';
import { NbToastrService, NbDateService } from '@nebular/theme';
import { DatePipe } from '@angular/common';
import { StaffService } from '../../company/staff/staff.service';

@Component({
  selector: 'ngx-ustaff-edit',
  templateUrl: './ustaff-edit.component.html',
  styleUrls: ['./ustaff-edit.component.scss'],
})
export class UstaffEditComponent implements OnInit {
  id: string;
  Done = false;
  resigned: boolean = false;

  staffForm: FormGroup;
  body: any;

  positionForm: FormGroup;
  positionFormEdit: FormGroup;
  PositionData = [];
  position = [];
  Positiontemp: any;
  index: any;

  StaffPicturePath: any;
  StaffSignature: any;
  bodypic: any;
  bodysig: any;
  selectedFile = [];
  enddatecheck = false;

  setting = {
    mode: 'external',
    actions: {
      add: true,
      edit: true,
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      PositionName: {
        title: 'Position',
        type: 'string',
      },
      PositionSalary: {
        title: 'Salary',
        type: 'string',
      },
      PositionStart: {
        title: 'Start Date',
        type: 'Date',
      },
      PositionEnd: {
        title: 'End Date',
        type: 'Date',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  min: any;

  constructor(private staffService: StaffService,
    private router: Router,
    activeid: ActivatedRoute,
    private modalService: NgbModal,
    private toastrService: NbToastrService,
    protected dateService: NbDateService<Date>,
    private datePipe: DatePipe) {
    activeid.params.subscribe((params: Params) => {
      this.id = params['Byid'];
    });

    this.positionFormEdit = new FormGroup({
      PositionName: new FormControl('', Validators.required),
      PositionSalary: new FormControl('', Validators.required),
      PositionStart: new FormControl('', Validators.required),
      PositionEnd: new FormControl(''),
    });

    this.positionForm = new FormGroup({
      PositionName: new FormControl('', Validators.required),
      PositionSalary: new FormControl('', Validators.required),
      PositionStart: new FormControl('', Validators.required),
      PositionEnd: new FormControl(''),
    });
  }

  ngOnInit() {
    this.Done = true;
    this.positionForm.reset();
    this.staffService.getStaffWithAuth(this.id, localStorage.getItem('companyId')).subscribe(
      data => {
        this.Done = false;
        this.StaffPicturePath = 'http://206.189.34.171:7777/staff/img/' + data.StaffPicturePath;
        this.StaffSignature = 'http://206.189.34.171:7777/staff/sig/' + data.StaffSignature;
        this.getBlob();
        this.getBlob1();
        if (data.StaffResignation === 0) {
          this.resigned = false;
          this.staffForm = new FormGroup({
            StaffId: new FormControl(this.id),
            StaffName: new FormControl(data.StaffName, Validators.required),
            StaffNameEn: new FormControl(data.StaffNameEn, Validators.required),
            StaffEmail: new FormControl(data.StaffEmail, [Validators.required, Validators.email]),
            StaffTel: new FormControl(data.StaffTel, Validators.required),
            StaffAddress: new FormControl(data.StaffAddress, Validators.required),
            StaffStart: new FormControl(new Date(data.StaffStart * 1000), Validators.required),
            StaffResignation: new FormControl({ value: '', disable: true }),
            StaffPositionName: new FormControl(data.StaffPositionName, Validators.required),
            StaffPositionSalary: new FormControl(data.StaffPositionSalary, Validators.required),
          });
          this.checkResigned();
        } else {
          this.resigned = true;
          this.staffForm = new FormGroup({
            StaffId: new FormControl(this.id),
            StaffName: new FormControl(data.StaffName, Validators.required),
            StaffNameEn: new FormControl(data.StaffNameEn, Validators.required),
            StaffEmail: new FormControl(data.StaffEmail, [Validators.required, Validators.email]),
            StaffTel: new FormControl(data.StaffTel, Validators.required),
            StaffAddress: new FormControl(data.StaffAddress, Validators.required),
            StaffStart: new FormControl(new Date(data.StaffStart * 1000), Validators.required),
            StaffResignation: new FormControl(new Date(data.StaffResignation * 1000), Validators.required),
            StaffPositionName: new FormControl(data.StaffPositionName, Validators.required),
            StaffPositionSalary: new FormControl(data.StaffPositionSalary, Validators.required),
          });
          this.checkResigned();
        }
        // this.source = data.StaffPositionHistory;
        for (let i = 0; i < data.StaffPositionHistory.length; i++) {
          if (data.StaffPositionHistory[i].PositionEnd === 0) {
            this.Positiontemp = {
              PositionId: data.StaffPositionHistory[i].PositionId,
              PositionName: data.StaffPositionHistory[i].PositionName,
              PositionSalary: data.StaffPositionHistory[i].PositionSalary,
              PositionStart: this.datePipe.transform(new Date
                (data.StaffPositionHistory[i].PositionStart * 1000), 'MMM d, y'),
              PositionEnd: 'now',
            };
            this.PositionData.push({
              PositionId: data.StaffPositionHistory[i].PositionId,
              PositionName: data.StaffPositionHistory[i].PositionName,
              PositionStart: Math.round(new Date(data.StaffPositionHistory[i].PositionStart * 1000).getTime()),
              PositionEnd: null,
              PositionSalary: data.StaffPositionHistory[i].PositionSalary,
              StaffId: '',
            });
          } else {
            this.Positiontemp = {
              PositionId: data.StaffPositionHistory[i].PositionId,
              PositionName: data.StaffPositionHistory[i].PositionName,
              PositionSalary: data.StaffPositionHistory[i].PositionSalary,
              PositionStart: this.datePipe.transform(new Date
                (data.StaffPositionHistory[i].PositionStart * 1000), 'MMM d, y'),
              PositionEnd: this.datePipe.transform(new Date
                (data.StaffPositionHistory[i].PositionEnd * 1000), 'MMM d, y'),
            };
            this.PositionData.push({
              PositionId: data.StaffPositionHistory[i].PositionId,
              PositionName: data.StaffPositionHistory[i].PositionName,
              PositionStart: Math.round(new Date(data.StaffPositionHistory[i].PositionStart * 1000).getTime()),
              PositionEnd: Math.round(new Date(data.StaffPositionHistory[i].PositionEnd * 1000).getTime()),
              PositionSalary: data.StaffPositionHistory[i].PositionSalary,
              StaffId: '',
            });
          }
          this.position.push(this.Positiontemp);
        }
        this.source.load(this.position);
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  startDatePick(event) {
    this.min = event;
  }

  onSaveStaff() {
    this.Done = true;
    if (this.resigned) {
      this.body = {
        companyId: localStorage.getItem('companyId'),
        StaffId: this.id,
        StaffName: this.staffForm.get('StaffName').value,
        StaffNameEn: this.staffForm.get('StaffNameEn').value,
        StaffEmail: this.staffForm.get('StaffEmail').value,
        StaffTel: this.staffForm.get('StaffTel').value,
        StaffAddress: this.staffForm.get('StaffAddress').value,
        StaffStart: Math.round((this.staffForm.get('StaffStart').value).getTime() / 1000),
        StaffResignation: Math.round((this.staffForm.get('StaffResignation').value).getTime() / 1000),
        StaffPositionName: this.staffForm.get('StaffPositionName').value,
        StaffPositionSalary: this.staffForm.get('StaffPositionSalary').value,
      };
    } else {
      this.body = {
        companyId: localStorage.getItem('companyId'),
        StaffId: this.id,
        StaffName: this.staffForm.get('StaffName').value,
        StaffNameEn: this.staffForm.get('StaffNameEn').value,
        StaffEmail: this.staffForm.get('StaffEmail').value,
        StaffTel: this.staffForm.get('StaffTel').value,
        StaffAddress: this.staffForm.get('StaffAddress').value,
        StaffStart: Math.round((this.staffForm.get('StaffStart').value).getTime() / 1000),
        StaffResignation: null,
        StaffPositionName: this.staffForm.get('StaffPositionName').value,
        StaffPositionSalary: this.staffForm.get('StaffPositionSalary').value,
      };
    }
    this.staffService.putStaffWithAuth(this.body).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.onUploadPic();
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  onDeleteStaff() {
    this.Done = true;
    this.staffService.deleteStaffWithAuth(this.id, localStorage.getItem('companyId')).subscribe(
      (response) => {
        this.toastrService.success('', 'A staff has been removed!', {});
        // tslint:disable-next-line: no-console
        console.log(response);
        this.router.navigateByUrl('/auth/profile');
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  addPosition() {
    this.Done = true;
    if (this.positionForm.get('PositionEnd').value == null || this.enddatecheck === false) {
      this.body = {
        CompanyId: localStorage.getItem('companyId'),
        StaffId: this.id,
        PositionName: this.positionForm.value.PositionName,
        PositionStart: Math.round((this.positionForm.get('PositionStart').value).getTime() / 1000),
        PositionEnd: null,
        PositionSalary: this.positionForm.value.PositionSalary,
      };
    } else {
      this.body = {
        CompanyId: localStorage.getItem('companyId'),
        StaffId: this.id,
        PositionName: this.positionForm.value.PositionName,
        PositionStart: Math.round((this.positionForm.get('PositionStart').value).getTime() / 1000),
        PositionEnd: Math.round((this.positionForm.get('PositionEnd').value).getTime() / 1000),
        PositionSalary: this.positionForm.value.PositionSalary,
      };
    }

    this.staffService.postPositionWithAuth(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.toastrService.success('', 'A new position has been added!', {});
        this.getPositionData();
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
    this.positionForm.reset();
    this.modalService.dismissAll('');
  }

  onEditConfirm(event, content): void {
    this.index = event.index;
    this.min = new Date(this.PositionData[event.index].PositionStart);
    if (this.PositionData[event.index].PositionEnd === 0 || this.PositionData[event.index].PositionEnd == null
      || this.enddatecheck === false) {
      this.positionFormEdit = new FormGroup({
        PositionName: new FormControl(this.PositionData[event.index].PositionName, Validators.required),
        PositionSalary: new FormControl(this.PositionData[event.index].PositionSalary, Validators.required),
        PositionStart: new FormControl(new Date(this.PositionData[event.index].PositionStart)
          , Validators.required),
        PositionEnd: new FormControl(new Date(this.dateService.addDay(this.dateService.today(), 0))),
      });
    } else {
      this.enddatecheck = true;
      this.positionFormEdit = new FormGroup({
        PositionName: new FormControl(this.PositionData[event.index].PositionName, Validators.required),
        PositionSalary: new FormControl(this.PositionData[event.index].PositionSalary, Validators.required),
        PositionStart: new FormControl(new Date(this.PositionData[event.index].PositionStart)
          , Validators.required),
        PositionEnd: new FormControl(new Date(this.PositionData[event.index].PositionEnd)),
      });
    }
    this.modalService.open(content, { centered: true });
  }

  editPosition() {
    if (this.positionFormEdit.get('PositionEnd').value === ''
      || this.positionFormEdit.get('PositionEnd').value === null || this.enddatecheck === false) {
      this.body = {
        CompanyId: localStorage.getItem('companyId'),
        StaffId: this.id,
        PositionId: this.PositionData[this.index].PositionId,
        PositionName: this.positionFormEdit.value.PositionName,
        PositionStart: Math.round((this.positionFormEdit.get('PositionStart').value).getTime() / 1000),
        PositionEnd: null,
        PositionSalary: this.positionFormEdit.value.PositionSalary,
      };
    } else {
      this.body = {
        CompanyId: localStorage.getItem('companyId'),
        StaffId: this.id,
        PositionId: this.PositionData[this.index].PositionId,
        PositionName: this.positionFormEdit.value.PositionName,
        PositionStart: Math.round((this.positionFormEdit.get('PositionStart').value).getTime() / 1000),
        PositionEnd: Math.round((this.positionFormEdit.get('PositionEnd').value).getTime() / 1000),
        PositionSalary: this.positionFormEdit.value.PositionSalary,
      };
    }
    this.staffService.putPositionWithAuth(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.toastrService.success('', 'Edit position success!', {});
        this.getPositionData();
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
    this.positionFormEdit.reset();
    this.modalService.dismissAll('');
  }

  onDeleteConfirm(event): void {
    this.Done = true;
    this.staffService.deletePositionWithAuth(this.id, event.data.PositionId, localStorage.getItem('companyId'))
      .subscribe(data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.toastrService.success('', 'A position has been removed!', {});
        this.getPositionData();
      },
        error => {
          this.Done = false;
          this.toastrService.danger(error.message, 'Something went wrong!', {});
          console.error(error);
        });
  }

  onCancel() {
    this.router.navigateByUrl('/page/account/profile');
    this.staffForm.reset();
  }

  toggleResign() {
    this.resigned = !this.resigned;
    this.checkResigned();
  }

  private checkResigned() {
    if (this.resigned) {
      this.staffForm.controls['StaffResignation'].enable();
      this.staffForm.controls['StaffResignation'].setValidators([Validators.required]);
      this.staffForm.controls['StaffResignation'].updateValueAndValidity();
    } else {
      this.staffForm.controls['StaffResignation'].disable();
      this.staffForm.controls['StaffResignation'].reset();
      this.staffForm.controls['StaffResignation'].clearValidators();
      this.staffForm.controls['StaffResignation'].updateValueAndValidity();
    }
  }

  onCancelHistory() {
    this.modalService.dismissAll();
    this.positionForm.reset();
    this.positionFormEdit.reset();
  }

  openVerticallyCentered(content) {
    this.positionForm.reset();
    this.enddatecheck = false;
    this.modalService.open(content, { centered: true });
  }

  onFileSelectedPic(event) {
    this.selectedFile[0] = event.target.files[0];
    const reader = new FileReader();
    // this.selectedFile = event;
    reader.readAsDataURL(this.selectedFile[0]);
    reader.onload = (_event) => {
      this.StaffPicturePath = reader.result;
    };
  }

  onFileSelectedSig(event) {
    this.selectedFile[1] = event.target.files[0];
    const reader = new FileReader();
    // this.selectedFile = event;
    reader.readAsDataURL(this.selectedFile[1]);
    reader.onload = (_event) => {
      this.StaffSignature = reader.result;
    };
  }

  onUploadPic() {
    this.bodypic = {
      type: this.selectedFile[0].name.split('.')[1],
      name: this.selectedFile[0].name.split('.')[0],
      StaffId: this.id,
      companyId: localStorage.getItem('companyId'),
    };
    this.bodysig = {
      type: this.selectedFile[1].name.split('.')[1],
      name: this.selectedFile[1].name.split('.')[0],
      StaffId: this.id,
      companyId: localStorage.getItem('companyId'),
    };
    this.staffService.putImgWithAuth(this.bodypic, this.selectedFile[0]).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.staffService.putSigWithAuth(this.bodysig, this.selectedFile[1]).subscribe(
          res => {
            this.Done = false;
            this.toastrService.success('', 'Edit success!', {});
            // tslint:disable-next-line: no-console
            console.log(res);
            this.router.navigateByUrl('/page/account/profile');
          },
          error => {
            this.Done = false;
            this.toastrService.danger('Cannot upload this signature.', {});
            console.error(error);
          });
      },
      error => {
        this.Done = false;
        this.toastrService.danger('Cannot upload this picture.', {});
        console.error(error);
      });
  }

  getBlob() {
    this.Done = true;
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[0] = this.blobToFile(xhr.response
          , this.StaffPicturePath.split('/')[this.StaffPicturePath.split('/').length - 1]);
        this.Done = false;
      } else {
        this.Done = false;
        this.selectedFile[0] = [];
      }
    };
    xhr.open('GET', this.StaffPicturePath, true);
    xhr.send();
  }

  getBlob1() {
    this.Done = true;
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[1] = this.blobToFile(xhr.response
          , this.StaffSignature.split('/')[this.StaffSignature.split('/').length - 1]);
        this.Done = false;
      } else {
        this.Done = false;
        this.selectedFile[1] = [];
      }
    };
    xhr.open('GET', this.StaffSignature, true);
    xhr.send();
  }

  blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;
    // A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    // Cast to a File() type
    return <File>theBlob;
  }

  transformDate(date) {
    this.datePipe.transform(date, 'MMM d, y');
  }

  getPositionData() {
    this.position = [];
    this.PositionData = [];
    this.staffService.getStaffWithAuth(this.id, localStorage.getItem('companyId')).subscribe(
      data => {
        this.Done = false;
        for (let i = 0; i < data.StaffPositionHistory.length; i++) {
          if (data.StaffPositionHistory[i].PositionEnd === 0) {
            this.Positiontemp = {
              PositionId: data.StaffPositionHistory[i].PositionId,
              PositionName: data.StaffPositionHistory[i].PositionName,
              PositionSalary: data.StaffPositionHistory[i].PositionSalary,
              PositionStart: this.datePipe.transform(new Date
                (data.StaffPositionHistory[i].PositionStart * 1000), 'MMM d, y'),
              PositionEnd: 'now',
            };
            this.PositionData.push({
              PositionId: data.StaffPositionHistory[i].PositionId,
              PositionName: data.StaffPositionHistory[i].PositionName,
              PositionStart: Math.round(new Date(data.StaffPositionHistory[i].PositionStart * 1000).getTime()),
              PositionEnd: null,
              PositionSalary: data.StaffPositionHistory[i].PositionSalary,
              StaffId: '',
            });
          } else {
            this.Positiontemp = {
              PositionId: data.StaffPositionHistory[i].PositionId,
              PositionName: data.StaffPositionHistory[i].PositionName,
              PositionSalary: data.StaffPositionHistory[i].PositionSalary,
              PositionStart: this.datePipe.transform(new Date
                (data.StaffPositionHistory[i].PositionStart * 1000), 'MMM d, y'),
              PositionEnd: this.datePipe.transform(new Date
                (data.StaffPositionHistory[i].PositionEnd * 1000), 'MMM d, y'),
            };
            this.PositionData.push({
              PositionId: data.StaffPositionHistory[i].PositionId,
              PositionName: data.StaffPositionHistory[i].PositionName,
              PositionStart: Math.round(new Date(data.StaffPositionHistory[i].PositionStart * 1000).getTime()),
              PositionEnd: Math.round(new Date(data.StaffPositionHistory[i].PositionEnd * 1000).getTime()),
              PositionSalary: data.StaffPositionHistory[i].PositionSalary,
              StaffId: '',
            });
          }
          this.position.push(this.Positiontemp);
        }
        this.source.load(this.position);
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        // tslint:disable-next-line: no-console
        console.error(error);
      });
  }

  toggleenddate() {
    this.enddatecheck = !this.enddatecheck;
  }
}
