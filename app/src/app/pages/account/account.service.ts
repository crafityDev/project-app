import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable()
export class AccountService {
    constructor(private httpClient: HttpClient) { }

    accountUpdate = new Subject<boolean>();

    getUser() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('email', localStorage.getItem('email'));
        return this.httpClient.get<any>('http://206.189.34.171:7777/mt/getuser', {
            headers: headers,
        });
    }

    putImgWithAuth(data, path) {
        const body = JSON.stringify(data);
        const formData: FormData = new FormData();
        formData.append('data', body);
        formData.append('path', path, path.name);
        formData.append('page', 'user');
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('email', localStorage.getItem('email'));
        // headers = headers.append('enctype', 'multipart/form-data');
        return this.httpClient.put('http://206.189.34.171:7777/mt/uploadPic', formData, {
            headers: headers,
        });
    }

    updataUser(data) {
        const body = JSON.parse(JSON.stringify(data));
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', localStorage.getItem('key'));
        return this.httpClient.put<any>('http://206.189.34.171:7777/mt/addnameuser', body, {
            headers: headers,
        });
    }

    deleteUser() {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', localStorage.getItem('key'));
        headers = headers.append('email', localStorage.getItem('email'));
        return this.httpClient.delete('http://206.189.34.171:7777/mt/deleteuserbyuser', {
            headers: headers,
        });
    }
}
