import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { AccountService } from '../account.service';

@Component({
  selector: 'ngx-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class ProfileEditComponent implements OnInit {

  closeResult: string;
  body: any;
  profileForm: FormGroup;
  Email: any;
  UserPicturePath: any;
  bodypic: any;
  selectedFile = [];

  constructor(private modalService: NgbModal,
    private accountService: AccountService,
    private toastrService: NbToastrService,
    private router: Router) {
    this.profileForm = new FormGroup({
      ProfileName: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    this.accountService.getUser().subscribe(
      (data) => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.Email = data.Email;
        this.UserPicturePath = 'http://206.189.34.171:7777/user/img/' + data.UserPicturePath;
        this.getBlob();
        this.profileForm = new FormGroup({
          ProfileName: new FormControl(data.UserName),
        });
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  onSaveProfile() {
    this.body = {
      email: this.Email,
      UserName: this.profileForm.get('ProfileName').value,
    };
    this.accountService.updataUser(this.body).subscribe(
      (response) => {
        this.toastrService.success('', 'Edit success!', {});
        // tslint:disable-next-line: no-console
        console.log('Save' + response);
        this.onUploadPic();
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  getDismissReason(reason: any) {
    throw new Error('Method not implemented.');
  }

  onFileSelected(event) {
    this.selectedFile[0] = event.target.files[0];
    const reader = new FileReader();
    // this.selectedFile = event;
    reader.readAsDataURL(this.selectedFile[0]);
    reader.onload = (_event) => {
      this.UserPicturePath = reader.result;
    };
  }

  onUploadPic() {
    this.bodypic = {
      type: this.selectedFile[0].name.split('.')[1],
      name: this.selectedFile[0].name.split('.')[0],
    };
    this.accountService.putImgWithAuth(this.bodypic, this.selectedFile[0]).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log('upload' + data);
        this.accountService.accountUpdate.next(true);
        this.router.navigateByUrl('/page/account/profile');
      },
      error => {
        console.error(error);
      });
  }

  getBlob() {
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      if (xhr.status === 200) {
        this.selectedFile[0] = this.blobToFile(xhr.response
          , this.UserPicturePath.split('/')[this.UserPicturePath.split('/').length - 1]);
      } else {
        this.selectedFile = [];
      }
    };
    xhr.open('GET', this.UserPicturePath, true);
    xhr.send();
  }

  blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;
    // A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    // Cast to a File() type
    return <File>theBlob;
  }

  deleteAccount(event) {
    if (window.confirm('Are you sure you want to delete this account?')) {
      // event.confirm.resolve(event.newData);
      this.accountService.deleteUser().subscribe(
        res => {
          // tslint:disable-next-line: no-console
          console.log(res);
          this.toastrService.success('', 'Delete succress!', {});
          localStorage.removeItem('key');
          localStorage.clear();
          this.router.navigateByUrl('/auth/login');
        },
        error => {
          this.toastrService.danger(error.message, 'Something went wrong!', {});
          console.error(error);
        },
      );
    } else {
      event.confirm.reject();
    }

  }
}
