import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { accountPages, AccountRoutingModule } from './account-routing.module';
import { ChangePasswdComponent } from './change-passwd/change-passwd.component';
import { UstaffEditComponent } from './ustaff-edit/ustaff-edit.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';

@NgModule({
  imports: [
    ThemeModule,
    AccountRoutingModule,
    Ng2SmartTableModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.circleSwish,
    }),
  ],
  declarations: [
    accountPages,
    ChangePasswdComponent,
    UstaffEditComponent,
  ],
})
export class AccountModule { }
