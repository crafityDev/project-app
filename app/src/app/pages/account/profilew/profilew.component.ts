import { Component, OnInit, OnDestroy } from '@angular/core';
import { CompanyService } from '../../company/company.service';
import { AccountService } from '../account.service';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-profilew',
  templateUrl: './profilew.component.html',
  styleUrls: ['./profilew.component.scss'],
})
export class ProfilewComponent implements OnInit, OnDestroy {

  Email: string;
  UserName: string;
  UserPicturePath: any;
  constructor(private accountService: AccountService,
    private companyService: CompanyService,
    private router: Router,
    private toastrService: NbToastrService) { }

  ngOnInit() {
    this.companyService.isCompanyAdd.next(true);
    this.accountService.accountUpdate.next(true);
    this.accountService.getUser().subscribe(
      (data) => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.Email = data.Email;
        this.UserName = data.UserName;
        this.UserPicturePath = 'http://206.189.34.171:7777/user/img/' + data.UserPicturePath;
      },
      error => {
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  ngOnDestroy(): void {
    this.companyService.isCompanyAdd.next(false);
    this.accountService.accountUpdate.next(false);
  }

  createCom() {
    this.router.navigateByUrl('/auth/company-add');
  }

}
