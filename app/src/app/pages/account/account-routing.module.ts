import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './account.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileEditComponent } from './edit/edit.component';
import { HelpComponent } from './help/help.component';
import { ChangePasswdComponent } from './change-passwd/change-passwd.component';
import { UstaffEditComponent } from './ustaff-edit/ustaff-edit.component';

const routes: Routes = [{
    path: '',
    component: AccountComponent,
    children: [
        {
            path: 'profile',
            component: ProfileComponent,
        },
        {
            path: 'staff-edit/:Byid',
            component: UstaffEditComponent,
        },
        {
            path: 'edit',
            component: ProfileEditComponent,
        },
        {
            path: 'change-password',
            component: ChangePasswdComponent,
        },
        {
            path: 'help',
            component: HelpComponent,
        },
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AccountRoutingModule { }

export const accountPages = [
    AccountComponent,
    ProfileComponent,
    ProfileEditComponent,
    ChangePasswdComponent,
    HelpComponent,
];
