import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-account',
  template: `
    <div class="container mb-5 mt-sm-5 mt-3">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AccountComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
