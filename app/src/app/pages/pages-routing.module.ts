import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  }, {
    path: 'miscellaneous',
    loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
  }, {
    path: 'project',
    loadChildren: './project/project.module#ProjectModule',
  }, {
    path: 'client',
    loadChildren: './client/client.module#ClientModule',
  }, {
    path: 'company',
    loadChildren: './company/company.module#CompanyModule',
  }, {
    path: 'doc',
    loadChildren: './document/document.module#DocumentModule',
  }, {
    path: 'account',
    loadChildren: './account/account.module#AccountModule',
  }, {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
