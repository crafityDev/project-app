import { NgModule } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { EditableModule } from '../project/project-info/shared/editable/editable.module';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    EditableModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.circleSwish,
    }),
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
