import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbThemeService, NbToastrService, NbDateService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';
import { SolarData } from '../../@core/data/solar';
import { ProjectService } from '../project/project.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { List, Task } from '../project/project-info/shared/Sprint.model';
import { FormArray, FormGroup, Validators, FormControl } from '@angular/forms';
import { moveItemInArray, CdkDragDrop, transferArrayItem } from '@angular/cdk/drag-drop';
import { TaskService } from '../project/project-info/shared/task.service';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit, OnDestroy {
  Done = false;
  OpenSprint = false;
  editTask = false;
  hasSprint = false;
  id: string;
  body: any;
  project: any[] = [];
  staffPics: any[] = [];
  daysLeft: any[] = [];

  lists: List[] = [];
  SprintName: string;
  SprintStart: any;
  SprintEnd: any;
  SprintId: string;

  controls: FormArray;
  sprintForm: FormGroup;
  listForm: FormGroup;
  taskAddForm: FormGroup;
  taskEditForm: FormGroup;
  private alive = true;

  solarValue: number;
  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };

  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
    this.rollerShadesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
  } = {
      default: this.commonStatusCardsSet,
      cosmic: this.commonStatusCardsSet,
      corporate: [
        {
          ...this.lightCard,
          type: 'warning',
        },
        {
          ...this.rollerShadesCard,
          type: 'primary',
        },
        {
          ...this.wirelessAudioCard,
          type: 'danger',
        },
        {
          ...this.coffeeMakerCard,
          type: 'secondary',
        },
      ],
    };

  constructor(private modalService: NgbModal,
    private themeService: NbThemeService,
    private solarService: SolarData,
    private projectService: ProjectService,
    private route: Router,
    private taskService: TaskService,
    private toastrService: NbToastrService,
    protected dateService: NbDateService<Date>) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
      });

    this.solarService.getSolarData()
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.solarValue = data;
      });

    this.sprintForm = new FormGroup({
      SprintName: new FormControl('', Validators.required),
      SprintStart: new FormControl('', Validators.required),
      SprintEnd: new FormControl('', Validators.required),
    });

    this.listForm = new FormGroup({
      ListName: new FormControl('', Validators.required),
    });

    this.taskEditForm = new FormGroup({
      TaskName: new FormControl('', Validators.required),
      TaskPoint: new FormControl(''),
      TaskDesc: new FormControl(''),
    });

    this.taskAddForm = new FormGroup({
      TaskName1: new FormControl('', Validators.required),
      TaskPoint1: new FormControl(''),
      TaskDesc1: new FormControl(''),
    });
  }

  ngOnInit() {
    this.staffPics = [];
    this.daysLeft = [];
    this.Done = true;
    this.getData();
    this.taskService.editTask.subscribe(x => {
      this.editTask = x;
    });
    this.taskService.hasSprint.subscribe(isTrue => {
      this.hasSprint = isTrue;
    });

  }

  getControl(index: number, field: string): FormControl {
    return this.controls.at(index).get(field) as FormControl;
  }

  updateField(index: number, field: string) {
    const control = this.getControl(index, field);

    if (control.valid) {
      this.lists = this.lists.map((e, i) => {
        if (index === i) {
          return {
            ...e,
            [field]: control.value,
          };

        }
        return e;
      });
    }

    this.taskService.updateListName(control.value, index);
    this.UpdateDrop();
  }

  get listIds(): string[] {
    return this.lists.map(list => list.ListId);
  }

  onTaskDrop(event: CdkDragDrop<Task[]>) {
    // In case the destination container is different from the previous container, we
    // need to transfer the given task to the target data array. This happens if
    // a task has been dropped on a different list.
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    this.UpdateDrop();
  }

  onListDrop(event: CdkDragDrop<List[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    const toGroups = this.lists.map(list => {
      // const a = new FormControl(list.isAdmin);
      return new FormGroup({
        ListName: new FormControl(list.ListName, Validators.required),
      });
    });
    this.controls = new FormArray(toGroups);
    this.UpdateDrop();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  goToInfo(Byid): void {
    localStorage.setItem('project', Byid);
    this.route.navigateByUrl('/page/project/info/dashboard/' + Byid);
  }

  onEdit(Byid) {
    localStorage.setItem('project', Byid);
    this.route.navigateByUrl('/page/project/info/edit/' + Byid);
  }

  getData() {
    this.projectService.Sort('pin').subscribe(
      data => {
        this.setData(data);
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  setData(data) {
    this.Done = false;
    this.project = data;
    for (const temp of this.project) {
      this.staffPics.push(temp.ProjectStaffPic);
      this.daysLeft.push(
        Math.round(
          (temp.ProjectEnd * 1000 - Math.round(this.dateService.today().getTime())) / (1000 * 60 * 60 * 24)));
    }
    // console.log(this.staffPics);
    // tslint:disable-next-line: no-console
    console.log(this.project);
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  opensprint(projectId) {
    this.id = projectId;
    this.projectService.getProjectWithAuth(projectId).subscribe(
      data => {
        for (let i = 0; i < data.ProjectSprints.length; i++) {
          this.projectService.getSprint(data.ProjectSprints[i]).subscribe(
            res => {
              if (res.MainSprint === 'Main') {
                // tslint:disable-next-line: no-console
                console.log(res);
                this.lists = res.SprintList;
                this.SprintId = res.SprintId;
                this.SprintStart = res.SprintStart;
                this.SprintEnd = res.SprintEnd;
                this.SprintId = res.SprintId;
                this.OpenSprint = true;
                this.SprintName = res.SprintName;
                // this.modalService.open(content, { centered: true, windowClass: 'xlModal' });
                this.sprintForm = new FormGroup({
                  SprintName: new FormControl(res.SprintName, Validators.required),
                  SprintStart: new FormControl(new Date(res.SprintStart * 1000), Validators.required),
                  SprintEnd: new FormControl(new Date(res.SprintEnd * 1000), Validators.required),
                });
              }
              const toGroups = this.lists.map(list => {
                // const a = new FormControl(list.isAdmin);
                return new FormGroup({
                  ListName: new FormControl(list.ListName, Validators.required),
                });
              });
              this.controls = new FormArray(toGroups);
            });
        }
      }, error => {
        console.error(error);
      });
  }

  onAddList() {
    this.body = {
      SprintId: this.SprintId,
      ListName: this.listForm.get('ListName').value,
    };
    this.projectService.createList(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.opensprint(this.id);
      }, error => {
        console.error(error);
      });
    this.modalService.dismissAll();
    this.listForm.reset();
  }

  onAddTask(listid) {
    if (this.taskAddForm.value.TaskPoint === '') {
      this.body = {
        SprintId: this.SprintId,
        ListId: listid,
        TaskName: this.taskAddForm.get('TaskName1').value,
        TaskPoint: 0,
        TaskDesc: this.taskAddForm.get('TaskDesc1').value,
      };
    } else {
      this.body = {
        SprintId: this.SprintId,
        ListId: listid,
        TaskName: this.taskAddForm.get('TaskName1').value,
        TaskPoint: this.taskAddForm.get('TaskPoint1').value,
        TaskDesc: this.taskAddForm.get('TaskDesc1').value,
      };
    }
    // tslint:disable-next-line: no-console
    console.log(this.body);
    this.projectService.createTask(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log('addtask' + data);
        this.opensprint(this.id);
      }, error => {
        console.error(error);
      });
    this.modalService.dismissAll();
    this.taskAddForm.reset();
  }

  onEditTask(listid, taskid, listindex, taskindex) {
    this.editTask = !this.editTask;
    if (this.taskEditForm.value.TaskPoint === '') {
      this.body = {
        SprintId: this.SprintId,
        ListId: listid,
        TaskId: taskid,
        TaskName: this.taskEditForm.get('TaskName').value,
        TaskPoint: 0,
        TaskDesc: this.taskEditForm.get('TaskDesc').value,
      };
      this.lists[listindex].ListTask[taskindex].TaskName = this.taskEditForm.get('TaskName').value;
      this.lists[listindex].ListTask[taskindex].TaskPoint = 0;
      this.lists[listindex].ListTask[taskindex].TaskDesc = this.taskEditForm.get('TaskDesc').value;
    } else {
      this.body = {
        SprintId: this.SprintId,
        ListId: listid,
        TaskId: taskid,
        TaskName: this.taskEditForm.get('TaskName').value,
        TaskPoint: this.taskEditForm.get('TaskPoint').value,
        TaskDesc: this.taskEditForm.get('TaskDesc').value,
      };
      this.lists[listindex].ListTask[taskindex].TaskName = this.taskEditForm.get('TaskName').value;
      this.lists[listindex].ListTask[taskindex].TaskPoint = this.taskEditForm.get('TaskPoint').value;
      this.lists[listindex].ListTask[taskindex].TaskDesc = this.taskEditForm.get('TaskDesc').value;
    }
    this.projectService.editTaskdata(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.opensprint(this.id);
      }, error => {
        console.error(error);
      });
    this.modalService.dismissAll();
    this.taskEditForm.reset();
  }

  onDeleteTask(taskid) {
    // this.lists[listindex].ListTask.splice(taskindex, 1);
    this.projectService.deleteTask(taskid).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        this.opensprint(this.id);
      }, error => {
        console.error(error);
      });
  }

  onDeleteList(listid, index) {
    this.lists.splice(index, 1);
    this.projectService.deleteList(this.SprintId, listid).subscribe(
      res => {
        // tslint:disable-next-line: no-console
        console.log(res);
        this.opensprint(this.id);
      }, error => {
        console.error(error);
      });
  }

  onDelete(Byid) {
    this.Done = true;
    this.projectService.deleteProjectWithAuth(Byid).subscribe(
      (response) => {
        this.toastrService.success('', 'A project has been removed!', {});
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getData();
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  pinProject(id, pin) {
    this.Done = true;
    this.body = {
      ProjectId: id,
      ProjectPinned: !pin,
    };
    // console.log(this.body)
    this.projectService.pinProject(this.body).subscribe(
      (response) => {
        // tslint:disable-next-line: no-console
        console.log(response);
        this.getData();
      },
      error => {
        this.Done = false;
        this.toastrService.danger(error.message, 'Something went wrong!', {});
        console.error(error);
      });
  }

  updateMainSprint(sprintId) {
    this.body = {
      SprintId: sprintId,
      ProjectId: this.id,
      MainSprint: 'Main',
    };
    this.projectService.updateMain(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.getPage();
        this.modalService.dismissAll();
      },
      error => {
        console.error(error);
      });
  }

  editSprint(sprintId) {
    this.body = {
      SprintId: sprintId,
      ProjectId: this.id,
      SprintList: this.lists,
      SprintName: this.sprintForm.get('SprintName').value,
      SprintStart: Math.round((this.sprintForm.get('SprintStart').value).getTime() / 1000),
      SprintEnd: Math.round((this.sprintForm.get('SprintEnd').value).getTime() / 1000),
    };
    this.projectService.editSprint(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log(data);
        this.modalService.dismissAll();
        this.getPage();
      },
      error => {
        console.error(error);
      });
  }

  getPage() {
    this.projectService.getProjectWithAuth(this.id).subscribe(
      data => {
        for (let i = 0; i < data.ProjectSprints.length; i++) {
          this.projectService.getSprint(data.ProjectSprints[i]).subscribe(
            res => {
              if (res.MainSprint === 'Main') {
                // tslint:disable-next-line: no-console
                console.log(res);
                this.lists = res.SprintList;
                this.SprintId = res.SprintId;
                this.SprintStart = res.SprintStart;
                this.SprintEnd = res.SprintEnd;
                this.SprintId = res.SprintId;

                this.sprintForm = new FormGroup({
                  SprintName: new FormControl(res.SprintName, Validators.required),
                  SprintStart: new FormControl(new Date(res.SprintStart * 1000), Validators.required),
                  SprintEnd: new FormControl(new Date(res.SprintEnd * 1000), Validators.required),
                });
              }
            });
        }
      }, error => {
        console.error(error);
      });
  }

  UpdateDrop() {
    this.body = {
      SprintId: this.SprintId,
      ProjectId: this.id,
      SprintList: this.lists,
      SprintName: this.SprintName,
      SprintStart: this.SprintStart,
      SprintEnd: this.SprintEnd,
    };
    this.projectService.editSprint(this.body).subscribe(
      data => {
        // tslint:disable-next-line: no-console
        console.log('update' + data);
        this.getPage();
      },
      error => {
        console.error(error);
      });
  }

  editTaskClick(TaskPoint, TaskName, TaskDesc) {
    this.editTask = !this.editTask;
    this.taskEditForm = new FormGroup({
      TaskName: new FormControl(TaskName, Validators.required),
      TaskPoint: new FormControl(TaskPoint),
      TaskDesc: new FormControl(TaskDesc),
    });
  }

  cancelTaskAdd() {
    this.modalService.dismissAll();
    this.taskAddForm.reset();
  }

  cancelTaskEdit() {
    this.modalService.dismissAll();
    this.editTask = !this.editTask;
  }

  cancelList() {
    this.modalService.dismissAll();
    this.listForm.reset();
  }

}
