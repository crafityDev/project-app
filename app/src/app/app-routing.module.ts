import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { ForgotPasswordComponent } from './auth/forget-password/forget-password.component';
import { AuthGuard } from './auth/auth.guard';
import { VerifyComponent } from './auth/verify/verify.component';
import { CompanyAddComponent } from './pages/company/company-add/company-add.component';
import { SetPasswordComponent } from './auth/set-password/set-password.component';
import { UserAddComponent } from './auth/user-add/user-add.component';
import { ProfilewComponent } from './pages/account/profilew/profilew.component';
import { WchangepasswdComponent } from './pages/account/wchangepasswd/wchangepasswd.component';
import { WeditComponent } from './pages/account/wedit/wedit.component';
// import { StaffAddComponent } from './pages/company/staff/staff-add/staff-add.component';


const routes: Routes = [
  {
    path: 'page',
    loadChildren: 'app/pages/pages.module#PagesModule',
    canActivateChild: [AuthGuard],
  },
  {
    path: 'auth',
    component: AuthComponent,
    children: [
      {
        path: '',
        component: LoginComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'register',
        component: RegisterComponent,
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
      },
      {
        path: 'verify',
        component: VerifyComponent,
      },
      {
        path: 'user-add',
        component: UserAddComponent,
      },
      {
        path: 'profile',
        component: ProfilewComponent,
      },
      {
        path: 'profile-edit',
        component: WeditComponent,
      },
      {
        path: 'change-password',
        component: WchangepasswdComponent,
      },
      {
        path: 'company-add',
        component: CompanyAddComponent,
        // canActivateChild: [AuthGuard],
      },
      {
        path: 'set-new-password',
        component: SetPasswordComponent,
      },
    ],
  },
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  { path: '**', redirectTo: 'auth' },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
  providers: [
    AuthGuard,
  ],
})
export class AppRoutingModule {
}
