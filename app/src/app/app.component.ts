/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { LayoutService } from './@core/utils';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(private analytics: AnalyticsService,
    protected layoutService: LayoutService) {
    // this.router.events.subscribe(data => {
    //   console.log('data');
    //   console.log(data);
    // })
    // if (localStorage.getItem('key') !== null) {
    //   this.router.navigateByUrl('/page/dashboard');
    // }
  }

  ngOnInit() {
    this.analytics.trackPageViews();
  }
}
